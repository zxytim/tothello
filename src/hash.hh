/*
 * $File: hash.hh
 * $Date: Wed Jan 30 05:41:57 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __HASH_HH__
#define __HASH_HH__

#include "common.hh"

const ull HASH_CODE_EMPTY = 0;

struct HashData
{
	score_t score;
	unsigned char date;
	unsigned char move[2]; // best moves

	unsigned int writeable_level() const;
	void update(HashData *hash_data);
	void upgrade(HashData *hash_data);
};

struct HashItem
{
	ull code; // hash code
	HashData data;

	/* update item of the same date */
	bool update(ull code, HashData *hash_data);
	/* update the item in spite of date difference */
	bool upgrade(ull code, HashData *hash_data);
	/* replace the item */
	void replace(ull code, HashData *hash_data);
	HashItem();
};

/*
 * hash table is ensured to be of the size of power of 2
 */
struct HashTable
{
	HashItem *item;
	ull hash_mask; 
	int size;
	int n_way;

	int date; // time stamp

	HashTable();

	static void setup();
	static HashData HASH_DATA_INIT;


	void set_size(int size);
	void set_n_way(int n_way);

	/*
	 * clear hash table: increase date
	 * IMPORTANT: this is not CLEAN UP hash table,
	 * but may clean up hash table
	 */
	void clear();

	void init();
	/*
	 * clean up the hash table
	 * reset date and date
	 * IMPORTANT: must be invoked before use
	 */
	void cleanup();

	bool inited;


	/*
	 * store a hash data, may fail.
	 * update if exists.
	 */
	void store(ull code, HashData *hash_data);

	/*
	 * store a hash data, replace the most unimportant one 
	 * if no free space remained.
	 * update if exists.
	 */
	void force_store(ull code, HashData *hash_data);

	/*
	 * get HashData by hash code.
	 * return NULL if no such hash data.
	 */
	HashData *get(ull code); // in spite of date
	HashData *get_newest(ull code); // get the newest data

	bool is_newest(const HashData *hash_data) const;

	/*
	 * make a writeable level of HashData according to
	 * couple of attributes such as date.
	 *
	 * used to decide whether to substitute(replace)
	 * a hash data with new one.
	 */
};

#endif // __HASH_HH__


