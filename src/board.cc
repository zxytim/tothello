/*
 * $File: board.cc
 * $Date: Wed Jan 30 19:52:47 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "board.hh"
#include "move.hh"
#include "bit.hh"
#include "flip_kindergarten.hh"
#include "random.hh"
#include "common.hh"

static ull hash_board[16][256];

void Board::setup()
{
	Random random;
	for (int i = 0; i < 16; i ++)
		for (int j = 0; j < 256; j ++)
			do
			{
				hash_board[i][j] = random.get();
			} while (bit_count(hash_board[i][j]) < 8);
}

void Board::init()
{
	player   = 0x0000000810000000ULL; // BLACK
	opponent = 0x0000001008000000ULL; // WHITE
}

ull Board::gen_move(int x, Move *move)
{
	move->flipped = flip[x](player, opponent);
	move->x = x;
	return move->flipped;
}

void Board::update(const Move *move)
{
	player ^= (move->flipped | x_to_bit(move->x));
	opponent ^= move->flipped;
	swap_players();
}

void Board::update(int x)
{
	Move move[1];
	gen_move(x, move);
	update(move);
}

void Board::restore(const Move *move)
{
	swap_players();
	opponent ^= move->flipped;
	player ^= (move->flipped | x_to_bit(move->x));
}


void Board::swap_players()
{
	const ull tmp = player;
	player = opponent;
	opponent = tmp;
}

void Board::pass_update()
{
	swap_players();
}

void Board::pass_restore()
{
	swap_players();
}

static inline unsigned long long get_some_moves(const unsigned long long P, const unsigned long long mask, const int dir)
{
	// kogge-stone algorithm
 	// 6 << + 6 >> + 12 & + 7 |
	// + better instruction independency
	register unsigned long long flip_l, flip_r;
	register unsigned long long mask_l, mask_r;
	register int d;

	flip_l = flip_r = P;
	mask_l = mask_r = mask;
	d = dir;

	flip_l |= mask_l & (flip_l << d);   flip_r |= mask_r & (flip_r >> d);
	mask_l &= (mask_l << d);            mask_r &= (mask_r >> d);
	d <<= 1;
	flip_l |= mask_l & (flip_l << d);   flip_r |= mask_r & (flip_r >> d);
	mask_l &= (mask_l << d);            mask_r &= (mask_r >> d);
	d <<= 1;
	flip_l |= mask_l & (flip_l << d);   flip_r |= mask_r & (flip_r >> d);

	return ((flip_l & mask) << dir) | ((flip_r & mask) >> dir);
}

static ull board_get_moves(ull player, ull opponent)
{
	const unsigned long long mask = opponent & 0x7E7E7E7E7E7E7E7Eull;

	return (get_some_moves(player, mask, 1) // horizontal
		| get_some_moves(player, opponent, 8)   // vertical
		| get_some_moves(player, mask, 7)   // diagonals
		| get_some_moves(player, mask, 9))
		& ~(player|opponent); // mask with empties
}

ull Board::get_moves() const
{
	return board_get_moves(player, opponent);
}

bool board_can_move(ull player, ull opponent)
{
	ull E = ~(player|opponent); // empties

	return (get_some_moves(player, opponent & 0x007E7E7E7E7E7E00ull, 7) & E)  // diagonals
		|| (get_some_moves(player, opponent & 0x007E7E7E7E7E7E00ull, 9) & E)
		|| (get_some_moves(player, opponent & 0x7E7E7E7E7E7E7E7Eull, 1) & E)  // horizontal
		|| (get_some_moves(player, opponent & 0x00FFFFFFFFFFFF00ull, 8) & E); // vertical
}

bool Board::can_move() const
{
	return board_can_move(player, opponent);
}

bool Board::can_opponent_move() const
{
	return board_can_move(opponent, player);
}

int Board::mobility() const
{
	return bit_count(get_moves());
}

int Board::weighted_mobility() const
{
	return bit_weighted_count(get_moves());
}

static inline unsigned long long get_some_potential_moves(const unsigned long long P, const int dir)
{
	return (P << dir | P >> dir);
}

ull Board::get_potential_moves() const
{
	return (get_some_potential_moves(opponent & 0x7E7E7E7E7E7E7E7Eull, 1) // horizontal
		| get_some_potential_moves(opponent & 0x00FFFFFFFFFFFF00ull, 8)   // vertical
		| get_some_potential_moves(opponent & 0x007E7E7E7E7E7E00ull, 7)   // diagonals
		| get_some_potential_moves(opponent & 0x007E7E7E7E7E7E00ull, 9))
		& ~(player|opponent); // mask with empties
}

ull Board::get_hash_code() const
{
	unsigned long long h;
	const unsigned char *p = (const unsigned char*)this;

	h  = hash_board[0][p[0]];
	h ^= hash_board[1][p[1]];
	h ^= hash_board[2][p[2]];
	h ^= hash_board[3][p[3]];
	h ^= hash_board[4][p[4]];
	h ^= hash_board[5][p[5]];
	h ^= hash_board[6][p[6]];
	h ^= hash_board[7][p[7]];
	h ^= hash_board[8][p[8]];
	h ^= hash_board[9][p[9]];
	h ^= hash_board[10][p[10]];
	h ^= hash_board[11][p[11]];
	h ^= hash_board[12][p[12]];
	h ^= hash_board[13][p[13]];
	h ^= hash_board[14][p[14]];
	h ^= hash_board[15][p[15]];

	return h;
}

int Board::potential_mobility() const
{
	return bit_weighted_count(get_potential_moves());
}

int Board::corner_stability() const
{
	const unsigned long long stable = ((((0x0100000000000001ULL & player) << 1) | ((0x8000000000000080ULL & player) >> 1) | ((0x0000000000000081ULL & player) << 8) | ((0x8100000000000000ULL & player) >> 8) | 0x8100000000000081ULL) & player);
	return bit_count(stable);

}

// XXX: TODO
int Board::edge_stability() const
{
	return 0;
}

int Board::count_empties() const
{
	return bit_count(~(player | opponent));
}

void Board::from_string(const std::string &str)
{
	assert(str.length() >= BOARD_SIZE + 2);

	player = opponent = 0;
	for (int i = 0; i < 8; i ++)
		for (int j = 0; j < 8; j ++)
		{
			int x =	i * 8 + j;
			char c = str[x];
			if (c == 'X' || c == 'x')
				player |= x_to_bit(x);
			else if (c == 'O' || c == 'o')
				opponent |= x_to_bit(x);
		}
	char c = str[BOARD_SIZE + 1];
	if (c == 'O' || c == 'o')
		swap_players();
}

void Board::print(int turn) const
{
	do_print(turn, COORD_NUMBER, stderr);
}

void Board::do_print(int turn, CoordType coord_type, FILE *f) const
{
	ull moves;
	if (turn == BLACK)
		moves = board_get_moves(player, opponent);
	else moves = board_get_moves(opponent, player);

	// put char
#define pc(c) fputc((c), f)

	char coord[2][9] = {"ABCDEFGH", "01234567"};
	pc(' ');
	for (int i = 0; i < 8; i ++)
	{
		pc(' ');
		pc(coord[coord_type][i]);
	}
	pc('\n');

	for (int i = 0; i < 8; i ++)
	{
		pc(coord[1][i]);
		for (int j = 0; j < 8; j ++)
		{
			int x = i * 8 + j;
			char c;
			if (player & x_to_bit(x))
				c = 'X';
			else if (opponent & x_to_bit(x))
				c = 'O';
			else if (moves & x_to_bit(x))
				c = '.';
			else c = '-';

			pc(' ');
			pc(c);
		}
		pc('\n');
	}

#undef pc
}

