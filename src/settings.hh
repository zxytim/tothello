/*
 * $File: settings.hh
 * $Date: Fri Feb 01 18:26:42 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SETTINGS_HH__
#define __SETTINGS_HH__

const int HASH_N_WAY_DEFAULT = 4,
	  HASH_SIZE = 1 << 23;

const bool USE_ITERATIVE_DEEPENING = 1;

const bool USE_NWS = 1; // null window search

const bool USE_PROBCUT = 0; // probcut operates only when nws is enabled

const bool USE_MULTICUT = 1; // multi cut
const int MULTICUT_REDUCED_DEPTH = 4;
const int MULTICUT_MOVES_INSPECT = 3; 
const int MULTICUT_CUT_THRESHOLD = 2;  // <= MULTICUT_MOVES_INSPECT

#endif // __SETTINGS_HH__


