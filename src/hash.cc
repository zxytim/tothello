/*
 * $File: hash.cc
 * $Date: Wed Jan 30 05:42:29 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hash.hh"
#include "common.hh"

HashData HashTable::HASH_DATA_INIT;
HashItem::HashItem()
{
	code = HASH_CODE_EMPTY;
}

void HashTable::setup()
{
	HASH_DATA_INIT.score = -SCORE_INF;
	HASH_DATA_INIT.date = 0;
	HASH_DATA_INIT.move[0] = HASH_DATA_INIT.move[1] = NOMOVE;
}

void HashTable::set_size(int size)
{
	this->size = 1;
	while (this->size < size)
		this->size <<= 1;
	if (this->size != size)
		this->size <<= 1;

	if (item)
		delete [] item;
	item = new HashItem[this->size + n_way + 2];
	hash_mask = this->size - 1;
}

void HashTable::set_n_way(int n_way)
{
	this->n_way = n_way;
}

HashTable::HashTable()
{
	item = NULL;
	size = 0;
	n_way = HASH_N_WAY_DEFAULT;

	inited = false;
}

HashData *HashTable::get(ull code)
{
	HashItem *hash = item + (code & hash_mask);
	for (int i = 0; i < n_way; i ++)
	{
		if (hash->code == code)
			return &hash->data;
		hash ++;
	}
	return NULL;
}

HashData *HashTable::get_newest(ull code)
{
	HashItem *hash = item + (code & hash_mask);
	for (int i = 0; i < n_way; i ++)
	{
		if (hash->code == code && hash->data.date == date)
			return &hash->data;
		hash ++;
	}
	return NULL;
}

bool HashTable::is_newest(const HashData *hash_data) const
{
	return hash_data->date == date;
}

void HashTable::store(ull code, HashData *hash_data)
{
	HashItem *hash = item + (code & hash_mask);
	for (int i = 0; i < n_way; i ++)
	{
		if (hash->update(code, hash_data))
			return;
		hash ++;
	}
}

void HashTable::force_store(ull code, HashData *hash_data)
{
	// TODO: find the most unimportant one
	HashItem *hash = item + (code & hash_mask);
	HashItem *worst = hash;
	for (int i = 0; i < n_way; i ++)
	{
		if (hash->upgrade(code, hash_data))
			return;
		if (worst->data.writeable_level() > hash->data.writeable_level())
			worst = hash;
		hash ++;
	}
	// may be a new board configuration
	// substitute the most unimportant one
	worst->replace(code, hash_data);

}

void HashTable::clear()
{
	if (date == 127)
		cleanup();
	date ++;
}

void HashTable::init()
{
	cleanup();
	inited = true;
}

void HashTable::cleanup()
{
	for (register int i = 0; i < size + n_way; i ++)
	{
		item[i].code = HASH_CODE_EMPTY;
		item[i].data = HASH_DATA_INIT;
	}
	date = 0;
}

unsigned int HashData::writeable_level() const
{
	return date;
}

bool HashItem::update(ull code, HashData *hash_data)
{
	if (this->code != 0 && code != this->code)
		return false;
	if (this->code == HASH_CODE_EMPTY)
		this->code = code;
	if (data.date == hash_data->date) // update
		data.update(hash_data);
	else data.upgrade(hash_data);
	return true;
}

bool HashItem::upgrade(ull code, HashData *hash_data)
{
	if (this->code != 0 && code != this->code)
		return false;
	if (this->code == HASH_CODE_EMPTY)
		this->code = code;
	data.upgrade(hash_data);
	return true;
}

void HashItem::replace(ull code, HashData *hash_data)
{
	this->code = code;
	*hash_data = data;
}


void HashData::update(HashData *hash_data)
{
	if (hash_data->score > score && move[0] != hash_data->move[0])
	{
		move[1] = move[0];
		move[0] = (unsigned char)hash_data->move[0];
		score = hash_data->score;
	}
}

void HashData::upgrade(HashData *hash_data)
{
	if (hash_data->score > score && move[0] != hash_data->move[0])
	{
		move[1] = move[0];
		move[0] = (unsigned char)hash_data->move[0];
	}
	date = hash_data->date;
	score = hash_data->score;
}


