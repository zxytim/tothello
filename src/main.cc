/*
 * $File: main.cc
 * $Date: Fri Feb 01 10:44:14 2013 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include <cstdio>

#include "search.hh"

#include "bit.hh"

#include <cstring>

void test()
{
	Search search[1];
	search->setup();

	Board board[1];
	board->from_string("-------------O-------O-X---OOOX---XXXX-----O-------------------- X");

	bitboard_write(board->player, stderr);
	bitboard_write(board->opponent, stderr);
	board->print(BLACK);

	int p = search->get_best_move(*board);
	int x = p / 8,
		y = p % 8;
	printf("%d %d\n", x, y);
}

void wai()
{
	Search search[1];
	search->setup();
	Board board;
	board.init();

	char buf[16];
	scanf("%s", buf);
	int turn = (strcmp(buf, "first") != 0);

	int x, y;
	if (turn)
	{
		scanf("%d%d", &x, &y);
		board.update(x * 8 + y);
	}

	while (true)
	{
		int p = search->get_best_move(board);
		if (p == NOMOVE)
			x = -1, y = -1;
		else 
		{
			x = p / 8, y = p % 8;
			board.update(p);
		}
		board.do_print(turn, COORD_NUMBER, stderr);
		printf("%d %d\n", x, y);
		fflush(stdout);

		scanf("%d%d", &x, &y);
		if (x != -1)
		{
			p = x * 8 + y;
			ull moves = board.get_moves();
			if (!(x_to_bit(p) & moves))
				break;
			board.update(p);
		}
	}
}

int main()
{
	test();
	//wai();
	return 0;
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */

