/*
 * $File: stats.hh
 * $Date: Fri Feb 01 11:26:57 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __STATS_HH__
#define __STATS_HH__

#include "type.hh"

struct Statistics
{
	int n_node;
	int n_node_at_depth[100];
	ull time; // in ms

	int n_nws_node;
	int n_nws_node_at_depth[100];

	int n_solve_0;
	int n_solve_game_over;

	int n_alpha_beta_cutoff;
	int n_ab_cutoff_at_depth[100];

	int n_transpositional_cutoff;

	int n_nws_tried;
	int n_nws_fail;

	int n_probcut_high_cutoff;
	int n_probcut_low_cutoff;

	int n_multicut_tried;
	int n_multicut_succeed;

	void start();
	void stop();
	void print();
};

#define SEARCH_STATS(x) (x)

#endif // __STATS_HH__


