/*
 * $File: stats.cc
 * $Date: Fri Feb 01 11:40:23 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stats.hh"
#include <sys/time.h>
#include <cstdio>
#include <cstring>
#include <cmath>

static ull get_time()
{
	timeval tv;
	gettimeofday(&tv, 0);
	return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

void Statistics::start()
{
	time = get_time();
	memset(n_node_at_depth, 0, sizeof(n_node_at_depth));
	memset(n_nws_node_at_depth, 0, sizeof(n_nws_node_at_depth));

	memset(n_ab_cutoff_at_depth, 0, sizeof(n_ab_cutoff_at_depth));
	n_solve_0 = 0;
	n_solve_game_over = 0;
	n_node = 0;
	n_alpha_beta_cutoff = 0;

	n_transpositional_cutoff = 0;

	n_nws_node = 0;
	n_nws_fail = 0;
	n_nws_tried = 0;

	n_probcut_high_cutoff = 0;
	n_probcut_low_cutoff = 0;

	n_multicut_tried = 0;
	n_multicut_succeed = 0;
}

void Statistics::stop()
{
	time = get_time() - time;
}

void Statistics::print()
{
	int n_node_tot = n_node + n_nws_node;

#define INSPECT(key) \
	fprintf(stderr, #key ": %d\n", key);
	fprintf(stderr, "normal nodes : %d, %.2lf%%\n", n_node, n_node / (double)n_node_tot * 100);
	fprintf(stderr, "nws nodes: %d, %.2lf%%\n", n_nws_node, n_nws_node / (double)n_node_tot * 100);
	fprintf(stderr, "node total: %d\n", n_node_tot);
	fprintf(stderr, "time used: %llums\n", time);
	fprintf(stderr, "node per sec: %.2lf\n", n_node_tot / (time / 1000.0));
	int depth = 0; 
	for (int i = 0; n_node_at_depth[i]; i ++)
	{
		depth ++;
		//fprintf(stderr, "normal node at depth %d: %d, %.2lf%%\n", i, n_node_at_depth[i], n_node_at_depth[i] / (double)n_node * 100);
	}

#if 0
	for (int i = 0; i < depth; i ++)
	{
		fprintf(stderr, "ab-cut at depth %d: %d, %.2lf%%\n", i, n_ab_cutoff_at_depth[i], n_ab_cutoff_at_depth[i] / (double)n_alpha_beta_cutoff * 100);
	}
	
	for (int i = 0; i < depth; i ++)
	{
		fprintf(stderr, "nws node at depth %d: %d, %.2lf%%\n", i, n_nws_node_at_depth[i], n_nws_node_at_depth[i] / (double)n_nws_node * 100);
	}
#endif

	double bfactor = pow(n_node, 1.0 / depth);
	fprintf(stderr, "branching factor (normal node): %.2lf\n", bfactor);
	bfactor = pow(n_node_tot, 1.0 / depth);
	fprintf(stderr, "branching factor (tot): %.2lf\n", bfactor);
	fprintf(stderr, "solve 0: %d\n", n_solve_0);
	fprintf(stderr, "solve game end: %d\n", n_solve_game_over);
	fprintf(stderr, "alpha-beta cutoffs: %d, %.2lf%%\n", n_alpha_beta_cutoff, n_alpha_beta_cutoff / (double)n_node);
	fprintf(stderr, "transpositional cutoffs: %d\n", n_transpositional_cutoff);
	fprintf(stderr, "nws fail/tried: %d/%d, %.2lf%%\n", n_nws_fail, n_nws_tried, n_nws_fail / (double)n_nws_tried);

	fprintf(stderr, "probcut (high + low): %d + %d = %d\n", n_probcut_high_cutoff, n_probcut_low_cutoff, n_probcut_low_cutoff + n_probcut_high_cutoff);
	
	fprintf(stderr, "multicut: %d/%d, %.2lf%%\n", n_multicut_succeed, n_multicut_tried, n_multicut_succeed / (double)n_multicut_tried * 100);
}

