/*
 * $File: flip_kindergarten.hh
 * $Date: Tue Jan 29 00:34:35 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __FLIP_KINDERGARTEN_HH__
#define __FLIP_KINDERGARTEN_HH__

/*
 * Add a header file for flip_kindergarten.c
 */

unsigned long long flip_A1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_pass(const unsigned long long P, const unsigned long long O);

extern unsigned long long (*flip[])(const unsigned long long, const unsigned long long);
#endif // __FLIP_KINDERGARTEN_HH__


