/*
 * $File: move.hh
 * $Date: Wed Jan 30 02:28:24 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MOVE_HH__
#define __MOVE_HH__

#include "common.hh"

struct Search;
struct HashData;
struct Board;

struct Move
{
	ull flipped;	/* bitboard representation of flipped squares */
	int x;			/* square played */
	int score;		/* score for this move (may be a estimation of large number of in range of score_t) */
	Move *next;		/* next move */

	void evaluate(Search *search, HashData *hash_data, const int sort_alpha, const int sort_depth);
	bool wipeout(Board *board) const;
	Move *next_best();

	static void setup();
};


struct MoveList
{
	Move move[MAX_MOVE + 2]; /* the first move is sentinel and is not used*/
	int n_moves;

	bool is_empty() const;
	Move *first_move();
	Move *best_move();

	void evaluate(Search *search, HashData *hash_data, const int alpha, const int depth);
	void sort();

};

#define foreach_move(iter, movelist) \
	for (Move *iter = (movelist)->move->next; (iter); (iter) = (iter)->next)

#define foreach_best_move(iter, movelist) \
	for (Move *iter = movelist->best_move(); (iter); (iter) = move->next_best())
#endif // __MOVE_HH__


