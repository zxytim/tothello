/*
 * $File: search.hh
 * $Date: Fri Feb 01 18:31:16 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SEARCH_HH__
#define __SEARCH_HH__

#include "board.hh"
#include "hash.hh"
#include "eval.hh"
#include "stats.hh"

struct Move;
struct MoveList;

struct Search
{
	Board board[1];
	Eval eval[1];
	int n_empties;

	int height; /* height from root */
	int depth;  /* search depth, in decreasing order */
	HashTable hash_table[1];
	Statistics stats;

	bool enable_probcut;
	bool enable_multicut;

	/*
	 * some intial setups. 
	 * must be called prior to any other method
	 */
	static void setup();

	/*
	 * get the best move to a board configuration.
	 * this is the outer most interface.
	 */
	int get_best_move(Board board);

	/* init the board */
	void init(Board board);


	/* update search status according to a move */
	void update(Move *move); 
	/* restore search status according to a move */
	void restore(Move *move);
	void nws_update(Move *move);
	void nws_restore(Move *move);

	void pass_update();
	void pass_restore();
	void nws_pass_update();
	void nws_pass_restore();


	/* search procedures */
	/* the main search procedure */
	int do_search(int alpha, int beta, int depth, bool force_store = false);
	int do_nws_search(int alpha, int depth); // Null Window Search
	void iterative_deepening(int alpha, int beta, int depth);


	bool probcut(int alpha, int beta, int depth, int &score);
	bool multicut(int alpha, int depth, MoveList * movelist, int &score);

	/* solve at depth 0 */
	int solve_0();
	/* solve at game end */
	int solve_game_over();

	/* evaluation methods */
	int eval_0(); // evaluate at depth 0

	/*
	 * get an unsorted movelist (i.e. from A1 to H8)
	 */
	void gen_movelist(MoveList *movelist);

};

#endif // __SEARCH_HH__


