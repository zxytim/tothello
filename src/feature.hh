/*
 * $File: feature.hh
 * $Date: Fri Feb 01 21:11:05 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __FEATURE_HH__
#define __FEATURE_HH__

struct Move;
struct Board;

/*
 * Abstract feature class
 */
struct Feature
{
	int eval(); 
	void pass();
	void update(Move *move);
	void restore(Move *move);
	void set(Board *board);
};

struct DiscFeature : public Feature
{
	/*
	 * Disc feature consists of a
	 * fixed length ternary number
	 */
	struct Feature2Coord
	{
		int n_square;
		int x[16];
	};

	struct Coord2Feature
	{
		int x;
		int feature_mask;
	};

	int pattern; // disc pattern
	int *val_table;
	Feature2Coord f2x[16];

	DiscFeature();

	int eval(); 
	void pass();
	void update(Move *move);
	void restore(Move *move);
	void set(Board *board);
};


#endif // __FEATURE_HH__


