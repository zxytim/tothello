/*
 * $File: feature.cc
 * $Date: Fri Feb 01 21:10:01 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "feature.hh"

int Feature::eval()
{
	throw "Not implemented!";
}

void Feature::pass()
{
	throw "Not implemented!";
}

void Feature::update(Move *)
{
	throw "Not implemented!";
}

void Feature::restore(Move *)
{
	throw "Not implemented!";
}

void Feature::set(Board *)
{
	throw "Not implemented!";
}


int DiscFeature::eval()
{
	return 0;
}

void DiscFeature::pass()
{
	throw "Not implemented!";
}

void DiscFeature::update(Move * /* move */)
{
	throw "Not implemented!";
}

void DiscFeature::restore(Move * /* move */)
{
	throw "Not implemented!";
}

void DiscFeature::set(Board * /* board */)
{
	throw "Not implemented!";
}




