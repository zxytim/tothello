/*
 * $File: random.cc
 * $Date: Tue Jan 29 00:15:35 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "random.hh"

static const ull MASK48 = 0XFFFFFFFFFFFFull;

void Random::set_seed(ull seed)
{
	this->seed = seed & MASK48;
}

ull Random::get()
{
	const ull A = 0x5DEECE66Dull;
	const ull B = 0xBull;
	register ull r;
	seed = ((A * seed + B) & MASK48);
	r = seed >> 16;
	seed = ((A * seed + B) & MASK48);
	return (r << 32) | (seed >> 16);
}

Random::Random()
{
	seed = 0x5DEECE66Dull;
}

