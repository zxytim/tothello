/*
 * $File: move.cc
 * $Date: Wed Jan 30 06:04:02 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "move.hh"

#include "search.hh"
#include "hash.hh"
#include "board.hh"

const int SQUARE_VALUE[] = {
	// JCW's score:
	18,  4,  16, 12, 12, 16,  4, 18,
	 4,  2,   6,  8,  8,  6,  2,  4,
	16,  6,  14, 10, 10, 14,  6, 16,
	12,  8,  10,  0,  0, 10,  8, 12,
	12,  8,  10,  0,  0, 10,  8, 12,
	16,  6,  14, 10, 10, 14,  6, 16,
	 4,  2,   6,  8,  8,  6,  2,  4,
	18,  4,  16, 12, 12, 16,  4, 18
};

static int eval_by_presorted_x[64];

void Move::setup()
{
	for (int i = 0; i < 64; i ++)
		for (int j = 0; j < 64; j ++)
			if (i == presorted_x[j])
				eval_by_presorted_x[i] = 64 - j;
}

bool MoveList::is_empty() const
{
	return n_moves == 0;
}

Move *MoveList::first_move()
{
	return move->next;
}

Move *MoveList::best_move()
{
	return move->next_best();
}

void MoveList::evaluate(Search *search, HashData *hash_data, const int alpha, const int depth)
{
	int sort_depth,
		sort_alpha;
	int min_depth = 9; // minimum depth to enable search evaluate

	if (search->n_empties <= 27)
		min_depth += (30 - search->n_empties) / 3;

	if (depth >= min_depth)
	{
		sort_depth = (depth - 15) / 3;
		//if (hash_data && hash_data->upper < alpha); // may cut off quickly ?
		if (search->n_empties >= 27) // more precise in end game
			sort_depth ++;
		if (sort_depth < 0)
			sort_depth = 0;
		else if (sort_depth > 6)
			sort_depth = 0;
	}
	else
		sort_depth = -1;

	sort_alpha = MAX(SCORE_MIN, alpha - 8); // slightly decrease alpha
	foreach_move(move, this)
		move->evaluate(search, hash_data, sort_alpha, sort_depth);
}

void MoveList::sort()
{
	foreach_best_move(move, this);
}

void Move::evaluate(Search *search, HashData *hash_data, const int sort_alpha, const int sort_depth)
{
	const int w_wipeout = 1 << 30,
		  w_first_move = 1 << 29,
		  w_second_move = 1 << 28,
		  w_hash = 1 << 15,
		  w_eval = 1 << 15,
		  w_mobility = 1 << 15,
		  w_corner_stability = 1 << 11,
		  w_edge_stability = 1 << 11,
		  w_potential_mobility = 1 << 5,
		  w_presorted_x = 0;

	Board *board = search->board;
	if (wipeout(board))
		score = w_wipeout;
	else if (x == hash_data->move[0])
		score = w_first_move;
	else if (x == hash_data->move[1])
		score = w_second_move;
	else
	{
		score = SQUARE_VALUE[x];
		
		if (sort_depth < 0) // not far from opening
		{
			score += board->corner_stability() * w_corner_stability;
			board->update(this); // after this move
			{
				score += (36 - board->potential_mobility()) * w_potential_mobility; // the lesser the better for opponent
				score += (36 - board->weighted_mobility()) * w_mobility; // the lesser the better  for opponent
			}
			board->restore(this);
		}
		else
		{
			search->update(this); // after this move
			score += board->edge_stability() * w_edge_stability;
			{
				score += (36 - board->potential_mobility()) * w_potential_mobility; // the lesser the better for opponent
				score += (36 - board->weighted_mobility()) * w_mobility; // the lesser the better  for opponent
			}

			switch (sort_depth)
			{
				case 0:
					score += ((SCORE_MAX - search->eval_0()) >> 2) * w_eval;
					break;
				case 1:
				case 2:
				default:
					if (search->hash_table->get(board->get_hash_code()))
						score += w_hash; // hash bonus
					//score += (SCORE_MAX - search->do_search(SCORE_MIN, -sort_alpha, sort_depth)) * w_eval;
			}

			search->restore(this);
		}
	}
	//score += eval_by_presorted_x[x] * w_presorted_x;
}

bool Move::wipeout(Board *board) const
{
	return board->opponent == flipped;
}

Move *Move::next_best()
{
	if (this->next)
	{
		Move *move,
			 *best = this; // the previous one of the best
		for (move = best->next; move->next; move = move->next)
			if (move->next->score > best->next->score)
				best = move;
		if (best != this)
		{
			move = best->next;
			best->next = move->next;
			move->next = this->next;
			this->next = move;
		}
	}
	return this->next;
}
