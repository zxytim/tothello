/*
 * $File: eval.cc
 * $Date: Fri Feb 01 18:37:13 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "eval.hh"
#include "move.hh"
#include "board.hh"
#include "feature.hh"



void Eval::pass()
{
	for (int i = 0; i < n_feature; i ++)
		feature[i]->pass();
}

void Eval::update(Move *move)
{
	for (int i = 0; i < n_feature; i ++)
		feature[i]->update(move);
}

void Eval::restore(Move *move)
{
	for (int i = 0; i < n_feature; i ++)
		feature[i]->restore(move);
}

void Eval::set(Board *board)
{
	for (int i = 0; i < n_feature; i ++)
		feature[i]->set(board);
}

void Eval::setup()
{
}

Eval::Eval()
{
}
