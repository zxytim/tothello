/*
 * $File: search.cc
 * $Date: Fri Feb 01 18:32:14 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "search.hh"
#include "move.hh"
#include "hash.hh"
#include "bit.hh"
#include "board.hh"
#include "common.hh"

#include <cmath>

void Search::setup()
{
	HashTable::setup();
	Board::setup();
	Move::setup();
	Eval::setup();
}

void Search::init(Board board)
{
	*this->board = board;
	enable_probcut = true;
	eval->set(this->board);
}

int Search::get_best_move(Board _board)
{
	init(_board);
	hash_table->set_size(HASH_SIZE);
	hash_table->init();
	n_empties = bit_count(~(board->player | board->opponent));

	stats.start();

	int search_depth = 12;
	if (USE_ITERATIVE_DEEPENING)
		iterative_deepening(SCORE_MIN, SCORE_MAX, search_depth);
	else
		do_search(SCORE_MIN, SCORE_MAX, search_depth, true);

	stats.stop();
	stats.print();

	ull hash_code = board->get_hash_code();
	HashData *hash_data = hash_table->get(hash_code);

	return hash_data->move[0];
}

int Search::do_nws_search(int alpha, int depth)
{
	SEARCH_STATS(stats.n_nws_node ++);
	SEARCH_STATS(stats.n_nws_node_at_depth[depth] ++);

	const int beta = alpha + 1;
	ull hash_code = board->get_hash_code();
	HashData *rst, // result
			 hash_init = HashTable::HASH_DATA_INIT;
	MoveList movelist[1];

	do
	{
		if (n_empties == 0)
			return solve_0();

		if (depth <= 0)
			return eval_0();

		rst = &hash_init;
		rst->score = SCORE_MIN;

		gen_movelist(movelist);

		/* special cases */
		if (movelist->is_empty())
		{
			if (board->can_opponent_move()) // pass
			{
				nws_pass_update();
				{
					rst->score = -do_nws_search(-beta, depth);
					rst->move[0] = PASS;
					rst->move[1] = NOMOVE;
				}
				nws_pass_restore();
			}
			else // game over
			{
				rst->score = solve_game_over();
				rst->move[0] = rst->move[1] = NOMOVE;
			}
			break;
		}

		if (!(rst = hash_table->get_newest(hash_code)))
			rst = &hash_init;
		else break;

		/* normal search */

		int score;
		
		// evaluate and sort moves for better ordering
		movelist->evaluate(this, rst, alpha, depth);
		movelist->sort();

		// multicut after edge sort
		if (multicut(alpha, depth, movelist, score))
			return score;
		// probcut
		if (probcut(alpha, beta, depth, score))
			return score;

		int nmove = 0;
		foreach_move(move, movelist)
		{
			nmove ++;
			nws_update(move);
			{
				move->score = -do_nws_search(-beta, depth - 1);
			}
			nws_restore(move);
			if (move->score > rst->score)
			{
				rst->score = move->score;
				rst->move[0] = move->x;
			}
			alpha = rst->score;
			if (alpha >= beta)
			{
				//SEARCH_STATS(stats.n_alpha_beta_cutoff ++);
				//SEARCH_STATS(stats.n_ab_cutoff_at_depth[depth] ++);
				break;
			}
		}

	} while (0);

	hash_table->store(hash_code, rst);

	return rst->score;
}

int Search::do_search(int alpha, int beta, int depth, bool force_store)
{
	SEARCH_STATS(stats.n_node ++);
	SEARCH_STATS(stats.n_node_at_depth[depth] ++);
	
	assert(n_empties == bit_count(~(board->player | board->opponent)));

	assert(alpha <= beta);
	assert(SCORE_MIN <= alpha && alpha <= SCORE_MAX);
	assert(SCORE_MIN <= beta && beta <= SCORE_MAX);


	ull hash_code = board->get_hash_code();
	HashData *rst, // result
			 hash_init = HashTable::HASH_DATA_INIT;
	MoveList movelist[1];

	do
	{
		if (n_empties == 0)
			return solve_0();

		if (depth <= 0)
			return eval_0();

		rst = &hash_init;
		rst->score = SCORE_MIN;

		gen_movelist(movelist);

		/* special cases */
		if (movelist->is_empty())
		{
			if (board->can_opponent_move()) // pass
			{
				pass_update();
				{
					rst->score = -do_search(-beta, -alpha, depth);
					rst->move[0] = PASS;
					rst->move[1] = NOMOVE;
				}
				pass_restore();
			}
			else // game over
			{
				rst->score = solve_game_over();
				rst->move[0] = rst->move[1] = NOMOVE;
			}
			break;
		}

		/* normal search */
		
#if 0
		int score;
		if (probcut(alpha, beta, depth, score))
			return score;
#endif

		if (!(rst = hash_table->get(hash_code)))
			rst = &hash_init;
		// evaluate and sort moves for better ordering
		movelist->evaluate(this, rst, alpha, depth);
		movelist->sort();

		int nmove = 0;
		foreach_move(move, movelist)
		{
			nmove ++;
			update(move);
			{
				if (nmove > 1 && USE_NWS)
				{
					SEARCH_STATS(stats.n_nws_tried ++);
					move->score = -do_nws_search(-beta, depth - 1);
					if (alpha < move->score && move->score < beta)
					{
						SEARCH_STATS(stats.n_nws_fail ++);
						move->score = -do_search(-beta, -alpha, depth - 1);
					}
				}
				else
					move->score = -do_search(-beta, -alpha, depth - 1);
			}
			restore(move);
			if (move->score > rst->score)
			{
				rst->score = move->score;
				rst->move[0] = move->x;
			}
			alpha = rst->score;
			if (alpha >= beta)
			{
				SEARCH_STATS(stats.n_alpha_beta_cutoff ++);
				SEARCH_STATS(stats.n_ab_cutoff_at_depth[depth] ++);
				break;
			}
		}

	} while (0);

	if (force_store)
	{
		hash_table->force_store(hash_code, rst);
		assert(movelist->first_move()->x == rst->move[0]);
		foreach_move(move, movelist)
		{
			fprintf(stderr, "(%d, %d) -> %d\n", move->x / 8, move->x % 8, move->score);
		}
	}
	else
	   	hash_table->store(hash_code, rst);
	return rst->score;
}


void Search::iterative_deepening(int alpha, int beta, int depth)
{
	for (int i = 1; i < depth; i ++)
	{
		hash_table->clear();
		//do_nws_search(alpha, i);
		do_search(alpha, beta, i);
	}
	hash_table->clear();
	//do_nws_search(alpha, depth);
	do_search(alpha, beta, depth, true);
}

void Search::gen_movelist(MoveList *movelist)
{
	Move *prev = movelist->move;
	Move *move = prev + 1;

	ull moves = board->get_moves();
	register int x;

	foreach_bit(x, moves)
	{
		board->gen_move(x, move);
		prev = prev->next = move;
		move ++;
	}
	prev->next = NULL;
	movelist->n_moves = move - movelist->move - 1;
	assert(movelist->n_moves == board->mobility());
}


void Search::update(Move *move)
{
	board->update(move);
	height ++;
	n_empties --;
}

void Search::restore(Move *move)
{
	board->restore(move);
	height --;
	n_empties ++;
}

void Search::nws_update(Move *move)
{
	board->update(move);
	height ++;
	n_empties --;
}

void Search::nws_restore(Move *move)
{
	board->restore(move);
	height --;
	n_empties ++;
}


void Search::pass_update()
{
	board->pass_update();
}

void Search::pass_restore()
{
	board->pass_restore();
}

void Search::nws_pass_update()
{
	board->pass_update();
}

void Search::nws_pass_restore()
{
	board->pass_restore();
}

int Search::solve_0()
{
	SEARCH_STATS(stats.n_solve_0 ++);
	return 2 * bit_count(board->player); // - SCORE_MAX;
}

int Search::solve_game_over()
{
	SEARCH_STATS(stats.n_solve_game_over ++);

	const int n_discs_p = bit_count(board->player);
	const int n_discs_o = 64 - n_empties - n_discs_p;

	register int score = n_discs_p - n_discs_o;

	if (score < 0)
		score -= n_empties;
	else if (score > 0)
		score += n_empties;

	return score;
}

int Search::eval_0()
{
	// XXX
	return solve_0();
}

bool Search::probcut(int alpha, int beta, int depth, int &score)
{
	if (!USE_PROBCUT)
		return false;
	if (!enable_probcut)
		return false;
	
	int probcut_depth = 2 * floor(depth * 0.25) + (depth & 1);
	if (probcut_depth == 0)
		probcut_depth = depth - 4;
	probcut_depth = depth - 4;
	
	int eval_score = eval_0();
	
	int eval_error = 0;
	int eval_alpha = alpha + eval_error,
		eval_beta = beta - eval_error;

	int probcut_error = 1.5;
	int probcut_alpha, probcut_beta;

	probcut_beta = beta + probcut_error;
	probcut_alpha = probcut_beta - 1;
	if (eval_score >= eval_beta && probcut_beta < SCORE_MAX)
	{
		this->enable_probcut = false;
		score = do_nws_search(probcut_alpha, probcut_depth);
		this->enable_probcut = true;
		if (score >= probcut_beta)
		{
			score = beta;
			SEARCH_STATS(stats.n_probcut_high_cutoff ++);
			return true;
		}
	}

	probcut_alpha = alpha - probcut_error;
	if (eval_score < eval_alpha && probcut_alpha > SCORE_MIN)
	{
		this->enable_probcut = false;
		score = do_nws_search(probcut_alpha, probcut_depth);
		this->enable_probcut = true;

		if (score <= probcut_alpha)
		{
			score = alpha;
			SEARCH_STATS(stats.n_probcut_low_cutoff ++);
			return true;
		}
	}
	return false;
}

bool Search::multicut(int alpha, int depth, MoveList *movelist, int &score)
{
	if (!USE_MULTICUT)
		return false;
	if (depth - 1 < MULTICUT_REDUCED_DEPTH)
		return false;

	// cut after edge sorted
	if (USE_MULTICUT && depth >= MULTICUT_REDUCED_DEPTH + 1)
	{
		SEARCH_STATS(stats.n_multicut_tried ++);
		int beta = alpha + 1;
		int nmove_remain = MULTICUT_MOVES_INSPECT;
		int ncut = MULTICUT_CUT_THRESHOLD;
		score = SCORE_MIN;
		int sc;
		foreach_move(move, movelist)
		{
			if ((nmove_remain --) == 0)
				break;
			nws_update(move);
			sc = -do_nws_search(-beta, depth - 1 - MULTICUT_REDUCED_DEPTH);
			nws_restore(move);
			if (sc > score)
			{
				score = sc;
				if (score >= beta)
				{
					ncut --; 
					if (ncut == 0)
					{
						SEARCH_STATS(stats.n_multicut_succeed ++);
						return true;
					}
				}
			}
		}
	}
	return false;
}

