/*
 * $File: board.hh
 * $Date: Wed Jan 30 19:41:52 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __BOARD_HH__
#define __BOARD_HH__

#include "common.hh"

#include <string>

struct Move;

enum CoordType {
	COORD_STANDARD,
	COORD_NUMBER
};

struct Board
{
	ull player, opponent;

	/* board setup, must be called first */
	static void setup();

	/* set the board to starting position */
	void init();

	/*
	 * compute and fill a move.
	 * return discs flipped
	 */
	ull gen_move(int x, Move *move);

	/* give available moves in bitboard format */
	ull get_moves() const;
	ull get_potential_moves() const;
	ull get_hash_code() const;

	/* check if a player can move */
	bool can_move() const;
	bool can_opponent_move() const;

	/* update the board according to move */
	void update(const Move *move);
	void update(int x);
	/* restore the board according to the move applied */
	void restore(const Move *move);

	void swap_players();


	/* pass a turn */
	void pass_update();
	void pass_restore();


	/* attributes */
	int mobility() const;
	int weighted_mobility() const; // count corners twice
	int potential_mobility() const;
	int corner_stability() const; // estimate #stable discs around the corner
	int edge_stability() const;
	int count_empties() const;


	/* peripheral methods */
	void from_string(const std::string &str);

	void print(int turn) const;
	void do_print(int turn, CoordType coord_type, FILE *f) const;
};

/* some isolated functions */
bool board_can_move(ull player, ull opponent);

#endif // __BOARD_HH__


