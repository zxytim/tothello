/*
 * $File: common.hh
 * $Date: Wed Jan 30 19:55:59 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __COMMON_HH__
#define __COMMON_HH__

#include <cstdio>

#ifdef ENABLE_ASSERT
#include <cassert>
#else
#define assert(x)
#endif

#include "type.hh"
#include "settings.hh"

const int BOARD_SIZE = 64,
	  MAX_MOVE = 32,
	  GAME_SIZE = 80; // size of the game including passing moves. this is an arbitrary big enough value

/** constants for square coordinates */
enum {
	A1, B1, C1, D1, E1, F1, G1, H1,
	A2, B2, C2, D2, E2, F2, G2, H2,
	A3, B3, C3, D3, E3, F3, G3, H3,
	A4, B4, C4, D4, E4, F4, G4, H4,
	A5, B5, C5, D5, E5, F5, G5, H5,
	A6, B6, C6, D6, E6, F6, G6, H6,
	A7, B7, C7, D7, E7, F7, G7, H7,
	A8, B8, C8, D8, E8, F8, G8, H8,
	PASS, NOMOVE
};

/** constants for colors */
enum {
	BLACK = 0,
	WHITE,
	EMPTY,
	OFF_SIDE
};

const score_t SCORE_INF = 127,
	  SCORE_MIN = -64,
	  SCORE_MAX = 64;

const int presorted_x[] = {
	A1, A8, H1, H8,                    /* Corner */
	C4, C5, D3, D6, E3, E6, F4, F5,    /* E */
	C3, C6, F3, F6,                    /* D */
	A3, A6, C1, C8, F1, F8, H3, H6,    /* A */
	A4, A5, D1, D8, E1, E8, H4, H5,    /* B */
	B4, B5, D2, D7, E2, E7, G4, G5,    /* G */
	B3, B6, C2, C7, F2, F7, G3, G6,    /* F */
	A2, A7, B1, B8, G1, G8, H2, H7,    /* C */
	B2, B7, G2, G7,                    /* X */
	D4, E4, D5, E5,                    /* center */
};


#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

#endif // __COMMON_HH__
