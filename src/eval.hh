/*
 * $File: eval.hh
 * $Date: Fri Feb 01 18:36:33 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __EVAL_HH__
#define __EVAL_HH__


struct Move;
struct Board;
struct Feature;

/*
 * the eval structure that hold
 * features
 */
struct Eval
{
	Feature **feature; // kinds of features
	int n_feature;

	Eval();

	static void setup();

	void pass();
	void update(Move *move);
	void restore(Move *move);
	void set(Board *board);
};

#endif // __EVAL_HH__


