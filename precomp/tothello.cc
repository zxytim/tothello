/*
 * $File: type.hh
 * $Date: Tue Jan 29 01:39:14 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __TYPE_HH__
#define __TYPE_HH__

typedef unsigned long long ull;
typedef char score_t;

#endif // __TYPE_HH__


/*
 * $File: stats.hh
 * $Date: Fri Feb 01 11:26:57 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __STATS_HH__
#define __STATS_HH__



struct Statistics
{
	int n_node;
	int n_node_at_depth[100];
	ull time; // in ms

	int n_nws_node;
	int n_nws_node_at_depth[100];

	int n_solve_0;
	int n_solve_game_over;

	int n_alpha_beta_cutoff;
	int n_ab_cutoff_at_depth[100];

	int n_transpositional_cutoff;

	int n_nws_tried;
	int n_nws_fail;

	int n_probcut_high_cutoff;
	int n_probcut_low_cutoff;

	int n_multicut_tried;
	int n_multicut_succeed;

	void start();
	void stop();
	void print();
};

#define SEARCH_STATS(x) (x)

#endif // __STATS_HH__


/*
 * $File: common.hh
 * $Date: Wed Jan 30 19:55:59 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __COMMON_HH__
#define __COMMON_HH__

#include <cstdio>

#ifdef ENABLE_ASSERT
#include <cassert>
#else
#define assert(x)
#endif




const int BOARD_SIZE = 64,
	  MAX_MOVE = 32,
	  GAME_SIZE = 80; // size of the game including passing moves. this is an arbitrary big enough value

/** constants for square coordinates */
enum {
	A1, B1, C1, D1, E1, F1, G1, H1,
	A2, B2, C2, D2, E2, F2, G2, H2,
	A3, B3, C3, D3, E3, F3, G3, H3,
	A4, B4, C4, D4, E4, F4, G4, H4,
	A5, B5, C5, D5, E5, F5, G5, H5,
	A6, B6, C6, D6, E6, F6, G6, H6,
	A7, B7, C7, D7, E7, F7, G7, H7,
	A8, B8, C8, D8, E8, F8, G8, H8,
	PASS, NOMOVE
};

/** constants for colors */
enum {
	BLACK = 0,
	WHITE,
	EMPTY,
	OFF_SIDE
};

const score_t SCORE_INF = 127,
	  SCORE_MIN = -64,
	  SCORE_MAX = 64;

const int presorted_x[] = {
	A1, A8, H1, H8,                    /* Corner */
	C4, C5, D3, D6, E3, E6, F4, F5,    /* E */
	C3, C6, F3, F6,                    /* D */
	A3, A6, C1, C8, F1, F8, H3, H6,    /* A */
	A4, A5, D1, D8, E1, E8, H4, H5,    /* B */
	B4, B5, D2, D7, E2, E7, G4, G5,    /* G */
	B3, B6, C2, C7, F2, F7, G3, G6,    /* F */
	A2, A7, B1, B8, G1, G8, H2, H7,    /* C */
	B2, B7, G2, G7,                    /* X */
	D4, E4, D5, E5,                    /* center */
};


#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

#endif // __COMMON_HH__
/**
 * @file bit.h
 *
 * Bitwise operations header file.
 *
 * @date 1998 - 2012
 * @author Richard Delorme
 * @version 4.3
 */

#ifndef EDAX_BIT_H
#define EDAX_BIT_H

#include <stdio.h>

struct Random;

/* declaration */
int bit_count(unsigned long long);
int bit_weighted_count(const unsigned long long);
int first_bit(unsigned long long);
int next_bit(unsigned long long*);
int last_bit(unsigned long long);
void bitboard_write(const unsigned long long, FILE*);
unsigned long long transpose(unsigned long long);
unsigned long long vertical_mirror(unsigned long long);
unsigned long long horizontal_mirror(unsigned long long);
unsigned int bswap_int(unsigned int);
unsigned short bswap_short(unsigned short);
int get_rand_bit(unsigned long long, struct Random*);

/** Loop over each bit set. */
#define foreach_bit(i, b) for (i = first_bit(b); b; i = next_bit(&b))

extern const unsigned long long X_TO_BIT[];
/** Return a bitboard with bit x set. */
#define x_to_bit(x) X_TO_BIT[x]

//#define x_to_bit(x) (1ULL << (x)) // 1% slower on Sandy Bridge
#endif

/*
 * $File: board.hh
 * $Date: Wed Jan 30 19:41:52 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __BOARD_HH__
#define __BOARD_HH__



#include <string>

struct Move;

enum CoordType {
	COORD_STANDARD,
	COORD_NUMBER
};

struct Board
{
	ull player, opponent;

	/* board setup, must be called first */
	static void setup();

	/* set the board to starting position */
	void init();

	/*
	 * compute and fill a move.
	 * return discs flipped
	 */
	ull gen_move(int x, Move *move);

	/* give available moves in bitboard format */
	ull get_moves() const;
	ull get_potential_moves() const;
	ull get_hash_code() const;

	/* check if a player can move */
	bool can_move() const;
	bool can_opponent_move() const;

	/* update the board according to move */
	void update(const Move *move);
	void update(int x);
	/* restore the board according to the move applied */
	void restore(const Move *move);

	void swap_players();


	/* pass a turn */
	void pass_update();
	void pass_restore();


	/* attributes */
	int mobility() const;
	int weighted_mobility() const; // count corners twice
	int potential_mobility() const;
	int corner_stability() const; // estimate #stable discs around the corner
	int edge_stability() const;
	int count_empties() const;


	/* peripheral methods */
	void from_string(const std::string &str);

	void print(int turn) const;
	void do_print(int turn, CoordType coord_type, FILE *f) const;
};

/* some isolated functions */
bool board_can_move(ull player, ull opponent);

#endif // __BOARD_HH__


/*
 * $File: feature.hh
 * $Date: Fri Feb 01 18:35:09 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __FEATURE_HH__
#define __FEATURE_HH__

struct Move;
struct Board;

/*
 * Abstract feature class
 */
struct Feature
{
	int eval(); 
	void pass();
	void update(Move *move);
	void restore(Move *move);
	void set(Board *board);
};

struct DiscFeature : public Feature
{
	int mask;
	DiscFeature();

	int eval(); 
	void pass();
	void update(Move *move);
	void restore(Move *move);
	void set(Board *board);
};


#endif // __FEATURE_HH__


/*
 * $File: eval.hh
 * $Date: Fri Feb 01 18:36:33 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __EVAL_HH__
#define __EVAL_HH__


struct Move;
struct Board;
struct Feature;

/*
 * the eval structure that hold
 * features
 */
struct Eval
{
	Feature **feature; // kinds of features
	int n_feature;

	Eval();

	static void setup();

	void pass();
	void update(Move *move);
	void restore(Move *move);
	void set(Board *board);
};

#endif // __EVAL_HH__


/*
 * $File: flip_kindergarten.hh
 * $Date: Tue Jan 29 00:34:35 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __FLIP_KINDERGARTEN_HH__
#define __FLIP_KINDERGARTEN_HH__

/*
 * Add a header file for flip_kindergarten.c
 */

unsigned long long flip_A1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H1(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H2(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H3(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H4(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H5(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H6(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H7(const unsigned long long P, const unsigned long long O);
unsigned long long flip_A8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_B8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_C8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_D8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_E8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_F8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_G8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_H8(const unsigned long long P, const unsigned long long O);
unsigned long long flip_pass(const unsigned long long P, const unsigned long long O);

extern unsigned long long (*flip[])(const unsigned long long, const unsigned long long);
#endif // __FLIP_KINDERGARTEN_HH__


/*
 * $File: hash.hh
 * $Date: Wed Jan 30 05:41:57 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __HASH_HH__
#define __HASH_HH__



const ull HASH_CODE_EMPTY = 0;

struct HashData
{
	score_t score;
	unsigned char date;
	unsigned char move[2]; // best moves

	unsigned int writeable_level() const;
	void update(HashData *hash_data);
	void upgrade(HashData *hash_data);
};

struct HashItem
{
	ull code; // hash code
	HashData data;

	/* update item of the same date */
	bool update(ull code, HashData *hash_data);
	/* update the item in spite of date difference */
	bool upgrade(ull code, HashData *hash_data);
	/* replace the item */
	void replace(ull code, HashData *hash_data);
	HashItem();
};

/*
 * hash table is ensured to be of the size of power of 2
 */
struct HashTable
{
	HashItem *item;
	ull hash_mask; 
	int size;
	int n_way;

	int date; // time stamp

	HashTable();

	static void setup();
	static HashData HASH_DATA_INIT;


	void set_size(int size);
	void set_n_way(int n_way);

	/*
	 * clear hash table: increase date
	 * IMPORTANT: this is not CLEAN UP hash table,
	 * but may clean up hash table
	 */
	void clear();

	void init();
	/*
	 * clean up the hash table
	 * reset date and date
	 * IMPORTANT: must be invoked before use
	 */
	void cleanup();

	bool inited;


	/*
	 * store a hash data, may fail.
	 * update if exists.
	 */
	void store(ull code, HashData *hash_data);

	/*
	 * store a hash data, replace the most unimportant one 
	 * if no free space remained.
	 * update if exists.
	 */
	void force_store(ull code, HashData *hash_data);

	/*
	 * get HashData by hash code.
	 * return NULL if no such hash data.
	 */
	HashData *get(ull code); // in spite of date
	HashData *get_newest(ull code); // get the newest data

	bool is_newest(const HashData *hash_data) const;

	/*
	 * make a writeable level of HashData according to
	 * couple of attributes such as date.
	 *
	 * used to decide whether to substitute(replace)
	 * a hash data with new one.
	 */
};

#endif // __HASH_HH__


/*
 * $File: move.hh
 * $Date: Wed Jan 30 02:28:24 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MOVE_HH__
#define __MOVE_HH__



struct Search;
struct HashData;
struct Board;

struct Move
{
	ull flipped;	/* bitboard representation of flipped squares */
	int x;			/* square played */
	int score;		/* score for this move (may be a estimation of large number of in range of score_t) */
	Move *next;		/* next move */

	void evaluate(Search *search, HashData *hash_data, const int sort_alpha, const int sort_depth);
	bool wipeout(Board *board) const;
	Move *next_best();

	static void setup();
};


struct MoveList
{
	Move move[MAX_MOVE + 2]; /* the first move is sentinel and is not used*/
	int n_moves;

	bool is_empty() const;
	Move *first_move();
	Move *best_move();

	void evaluate(Search *search, HashData *hash_data, const int alpha, const int depth);
	void sort();

};

#define foreach_move(iter, movelist) \
	for (Move *iter = (movelist)->move->next; (iter); (iter) = (iter)->next)

#define foreach_best_move(iter, movelist) \
	for (Move *iter = movelist->best_move(); (iter); (iter) = move->next_best())
#endif // __MOVE_HH__


/*
 * $File: random.hh
 * $Date: Tue Jan 29 00:15:34 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __RANDOM_HH__
#define __RANDOM_HH__



struct Random
{
	ull seed;

	Random();
	void set_seed(ull seed);
	ull get();
};

#endif // __RANDOM_HH__


/*
 * $File: search.hh
 * $Date: Fri Feb 01 18:31:16 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SEARCH_HH__
#define __SEARCH_HH__






struct Move;
struct MoveList;

struct Search
{
	Board board[1];
	Eval eval[1];
	int n_empties;

	int height; /* height from root */
	int depth;  /* search depth, in decreasing order */
	HashTable hash_table[1];
	Statistics stats;

	bool enable_probcut;
	bool enable_multicut;

	/*
	 * some intial setups. 
	 * must be called prior to any other method
	 */
	static void setup();

	/*
	 * get the best move to a board configuration.
	 * this is the outer most interface.
	 */
	int get_best_move(Board board);

	/* init the board */
	void init(Board board);


	/* update search status according to a move */
	void update(Move *move); 
	/* restore search status according to a move */
	void restore(Move *move);
	void nws_update(Move *move);
	void nws_restore(Move *move);

	void pass_update();
	void pass_restore();
	void nws_pass_update();
	void nws_pass_restore();


	/* search procedures */
	/* the main search procedure */
	int do_search(int alpha, int beta, int depth, bool force_store = false);
	int do_nws_search(int alpha, int depth); // Null Window Search
	void iterative_deepening(int alpha, int beta, int depth);


	bool probcut(int alpha, int beta, int depth, int &score);
	bool multicut(int alpha, int depth, MoveList * movelist, int &score);

	/* solve at depth 0 */
	int solve_0();
	/* solve at game end */
	int solve_game_over();

	/* evaluation methods */
	int eval_0(); // evaluate at depth 0

	/*
	 * get an unsorted movelist (i.e. from A1 to H8)
	 */
	void gen_movelist(MoveList *movelist);

};

#endif // __SEARCH_HH__


/*
 * $File: settings.hh
 * $Date: Fri Feb 01 18:26:42 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2012>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello.

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SETTINGS_HH__
#define __SETTINGS_HH__

const int HASH_N_WAY_DEFAULT = 4,
	  HASH_SIZE = 1 << 23;

const bool USE_ITERATIVE_DEEPENING = 1;

const bool USE_NWS = 1; // null window search

const bool USE_PROBCUT = 0; // probcut operates only when nws is enabled

const bool USE_MULTICUT = 1; // multi cut
const int MULTICUT_REDUCED_DEPTH = 4;
const int MULTICUT_MOVES_INSPECT = 3; 
const int MULTICUT_CUT_THRESHOLD = 2;  // <= MULTICUT_MOVES_INSPECT

#endif // __SETTINGS_HH__


/**
 * @file bit.c
 *
 * Bitwise operations.
 * Several algorithms manipulating bits are presented here. Quite often,
 * a macro needs to be defined to chose between different flavors of the
 * algorithm.
 *
 * Adapted by Xinyu Zhou.
 * Thanks to Richard Delorme's great job!
 *
 * @date 1998 - 2012
 * @author Richard Delorme
 * @version 4.3
 */



/** coordinate to bit table converter */
const unsigned long long X_TO_BIT[] = {
	0x0000000000000001ULL, 0x0000000000000002ULL, 0x0000000000000004ULL, 0x0000000000000008ULL,
	0x0000000000000010ULL, 0x0000000000000020ULL, 0x0000000000000040ULL, 0x0000000000000080ULL,
	0x0000000000000100ULL, 0x0000000000000200ULL, 0x0000000000000400ULL, 0x0000000000000800ULL,
	0x0000000000001000ULL, 0x0000000000002000ULL, 0x0000000000004000ULL, 0x0000000000008000ULL,
	0x0000000000010000ULL, 0x0000000000020000ULL, 0x0000000000040000ULL, 0x0000000000080000ULL,
	0x0000000000100000ULL, 0x0000000000200000ULL, 0x0000000000400000ULL, 0x0000000000800000ULL,
	0x0000000001000000ULL, 0x0000000002000000ULL, 0x0000000004000000ULL, 0x0000000008000000ULL,
	0x0000000010000000ULL, 0x0000000020000000ULL, 0x0000000040000000ULL, 0x0000000080000000ULL,
	0x0000000100000000ULL, 0x0000000200000000ULL, 0x0000000400000000ULL, 0x0000000800000000ULL,
	0x0000001000000000ULL, 0x0000002000000000ULL, 0x0000004000000000ULL, 0x0000008000000000ULL,
	0x0000010000000000ULL, 0x0000020000000000ULL, 0x0000040000000000ULL, 0x0000080000000000ULL,
	0x0000100000000000ULL, 0x0000200000000000ULL, 0x0000400000000000ULL, 0x0000800000000000ULL,
	0x0001000000000000ULL, 0x0002000000000000ULL, 0x0004000000000000ULL, 0x0008000000000000ULL,
	0x0010000000000000ULL, 0x0020000000000000ULL, 0x0040000000000000ULL, 0x0080000000000000ULL,
	0x0100000000000000ULL, 0x0200000000000000ULL, 0x0400000000000000ULL, 0x0800000000000000ULL,
	0x1000000000000000ULL, 0x2000000000000000ULL, 0x4000000000000000ULL, 0x8000000000000000ULL,
	0, 0 // <- hack for passing move & nomove
};

/** Conversion array: neighbour bits */
const unsigned long long NEIGHBOUR[] = {
	0x0000000000000302ULL, 0x0000000000000705ULL, 0x0000000000000e0aULL, 0x0000000000001c14ULL,
	0x0000000000003828ULL, 0x0000000000007050ULL, 0x000000000000e0a0ULL, 0x000000000000c040ULL,
	0x0000000000030203ULL, 0x0000000000070507ULL, 0x00000000000e0a0eULL, 0x00000000001c141cULL,
	0x0000000000382838ULL, 0x0000000000705070ULL, 0x0000000000e0a0e0ULL, 0x0000000000c040c0ULL,
	0x0000000003020300ULL, 0x0000000007050700ULL, 0x000000000e0a0e00ULL, 0x000000001c141c00ULL,
	0x0000000038283800ULL, 0x0000000070507000ULL, 0x00000000e0a0e000ULL, 0x00000000c040c000ULL,
	0x0000000302030000ULL, 0x0000000705070000ULL, 0x0000000e0a0e0000ULL, 0x0000001c141c0000ULL,
	0x0000003828380000ULL, 0x0000007050700000ULL, 0x000000e0a0e00000ULL, 0x000000c040c00000ULL,
	0x0000030203000000ULL, 0x0000070507000000ULL, 0x00000e0a0e000000ULL, 0x00001c141c000000ULL,
	0x0000382838000000ULL, 0x0000705070000000ULL, 0x0000e0a0e0000000ULL, 0x0000c040c0000000ULL,
	0x0003020300000000ULL, 0x0007050700000000ULL, 0x000e0a0e00000000ULL, 0x001c141c00000000ULL,
	0x0038283800000000ULL, 0x0070507000000000ULL, 0x00e0a0e000000000ULL, 0x00c040c000000000ULL,
	0x0302030000000000ULL, 0x0705070000000000ULL, 0x0e0a0e0000000000ULL, 0x1c141c0000000000ULL,
	0x3828380000000000ULL, 0x7050700000000000ULL, 0xe0a0e00000000000ULL, 0xc040c00000000000ULL,
	0x0203000000000000ULL, 0x0507000000000000ULL, 0x0a0e000000000000ULL, 0x141c000000000000ULL,
	0x2838000000000000ULL, 0x5070000000000000ULL, 0xa0e0000000000000ULL, 0x40c0000000000000ULL,
	0, 0 // <- hack for passing move & nomove
};

/**
 * @brief Count the number of bits set to one in an unsigned long long.
 *
 * This is the classical popcount function.
 * Since 2007, it is part of the instruction set of some modern CPU,
 * (>= barcelona for AMD or >= nelhacem for Intel). Alternatively,
 * a fast SWAR algorithm, adding bits in parallel is provided here.
 * This function is massively used to count discs on the board,
 * the mobility, or the stability.
 *
 * @param b 64-bit integer to count bits of.
 * @return the number of bits set.
 */

int bit_count(unsigned long long b)
{
#if defined(POPCOUNT)

	#if defined(USE_GAS_X64)
		__asm__("popcntq %1,%0" :"=r" (b) :"rm" (b));
		return (int) b;
	#elif defined(USE_MSVC_X64)
		return __popcnt64(b);
	#elif defined(USE_GCC_X64)
		return __builtin_popcountll(b);
	#endif

// MMX does not help much here :-(
#elif defined (USE_GAS_MMX)
 
	const unsigned long long M55 = 0x5555555555555555ULL;
	const unsigned long long M33 = 0x3333333333333333ULL;
	const unsigned long long M0F = 0x0F0F0F0F0F0F0F0FULL;
	int count;

	__asm__ volatile(
 		"movq  %1, %%mm1\n\t"
		"pxor  %%mm2, %%mm2\n\t"

		"movq  %%mm1, %%mm0\n\t"
		"psrlq $1, %%mm1\n\t"
		"pand  %2, %%mm1\n\t"
		"psubd %%mm1, %%mm0\n\t"

		"movq  %%mm0, %%mm1\n\t"
		"psrlq $2, %%mm0\n\t"
		"pand  %3, %%mm1\n\t"
		"pand  %3, %%mm0\n\t"
		"paddd %%mm1, %%mm0\n\t"

		"movq  %%mm0, %%mm1\n\t"
		"psrlq $4, %%mm0\n\t"
		"paddd %%mm1, %%mm0\n\t"
		"pand  %4, %%mm0\n\t"

		"psadbw %%mm2, %%mm0\n\t"
		"movd	%%mm0, %0\n\t"
		"emms\n\t"
		: "=a" (count) : "m" (b), "m" (M55), "m" (M33), "m" (M0F));

	return count;

#else

	register unsigned long long c = b
		- ((b >> 1) & 0x7777777777777777ULL)
		- ((b >> 2) & 0x3333333333333333ULL)
		- ((b >> 3) & 0x1111111111111111ULL);
	c = ((c + (c >> 4)) & 0x0F0F0F0F0F0F0F0FULL) * 0x0101010101010101ULL;

	return  (int)(c >> 56);

#endif
}

/**
 * @brief count the number of discs, counting the corners twice.
 *
 * This is a variation of the above algorithm to count the mobility and favour
 * the corners. This function is usefull for move sorting.
 *
 * @param v 64-bit integer to count bits of.
 * @return the number of bit set, counting the corners twice.
 */
int bit_weighted_count(const unsigned long long v)
{
#if defined(POPCOUNT)

	return bit_count(v) + bit_count(v & 0x8100000000000081ULL);

#else

	register unsigned long long b;
	b  = v - ((v >> 1) & 0x1555555555555515ULL) + (v & 0x0100000000000001ULL);
	b  = ((b >> 2) & 0x3333333333333333ULL) + (b & 0x3333333333333333ULL);
	b  = ((b >> 4) + b) & 0x0f0f0f0f0f0f0f0fULL;
	b *= 0x0101010101010101ULL;

	return  (int)(b >> 56);

#endif
}

/**
 *
 * @brief Search the first bit set.
 *
 * On CPU with AMD64 or EMT64 instructions, a fast asm
 * code is provided. Alternatively, a fast algorithm based on
 * magic numbers is provided.
 *
 * @param b 64-bit integer.
 * @return the index of the first bit set.
 */
int first_bit(unsigned long long b)
{
#if defined(USE_GAS_X64)

	__asm__("bsfq %1,%0" : "=r" (b) : "rm" (b));
	return (int) b;

#elif defined(USE_GAS_X86)

  int x1, x2;
	__asm__ ("bsf %0,%0\n"
	         "jnz 1f\n"
	         "bsf %1,%0\n"
	         "jz 1f\n"
	         "addl $32,%0\n"
		     "1:": "=&q" (x1), "=&q" (x2):"1" ((int) (b >> 32)), "0" ((int) b));
	return x1;

#elif defined(USE_MSVC_X64)

	unsigned long index;
	_BitScanForward64(&index, b);
	return (int) index;

#elif defined(USE_GCC_X64)

	return __builtin_ctzll(b);

#elif defined(USE_MASM_X86)
	__asm {
		xor eax, eax
		bsf edx, dword ptr b
		jnz l1
		bsf edx, dword ptr b+4
		mov eax, 32
		jnz l1
		mov edx, -32
	l1:	add eax, edx
	}

#elif defined(USE_GCC_ARM)
	const unsigned int lb = (unsigned int)b;
	if (lb) {
		return  __builtin_clz(lb & -lb) ^ 31;
	} else {
		const unsigned int hb = b >> 32;
		return 32 + (__builtin_clz(hb & -hb) ^ 31);
	}

#else

	const int magic[64] = {
		63, 0, 58, 1, 59, 47, 53, 2,
		60, 39, 48, 27, 54, 33, 42, 3,
		61, 51, 37, 40, 49, 18, 28, 20,
		55, 30, 34, 11, 43, 14, 22, 4,
		62, 57, 46, 52, 38, 26, 32, 41,
		50, 36, 17, 19, 29, 10, 13, 21,
		56, 45, 25, 31, 35, 16, 9, 12,
		44, 24, 15, 8, 23, 7, 6, 5
	};

	return magic[((b & (-b)) * 0x07EDD5E59A4E28C2ULL) >> 58];

#endif
}

/**
 * @brief Search the next bit set.
 *
 * In practice, clear the first bit set and search the next one.
 *
 * @param b 64-bit integer.
  * @return the index of the next bit set.
 */
int next_bit(unsigned long long *b)
{
	*b &= *b - 1;
	return first_bit(*b);
}

/**
 * @brief Search the last bit set (same as log2()).
 *
 * On CPU with AMD64 or EMT64 instructions, a fast asm
 * code is provided. Alternatively, a fast algorithm based on
 * magic numbers is provided.
 *
 * @param b 64-bit integer.
 * @return the index of the last bit set.
 */
int last_bit(unsigned long long b)
{
#if defined(USE_GAS_X64)

	__asm__("bsrq %1,%0" :"=r" (b) :"rm" (b));
	return b;

#elif defined(USE_MSVC_X64)

	unsigned long long index;
	_BitScanReverse64(&index, b);
	return (int) index;

#elif defined(USE_GCC_X64)

	return 63 - __builtin_clzll(b);
	
#elif defined(USE_GAS_X86)

  int x1, x2;
	__asm__ ("bsr %1,%0\n"
	         "jnz 1f\n"
	         "bsr %0,%0\n"
	         "subl $32,%0\n"
             "1: addl $32,%0\n" : "=&q" (x1), "=&q" (x2) : "1" ((int) (b >> 32)), "0" ((int) b));
  return x1;

	
#elif defined(USE_GCC_ARM)
	const unsigned int hb = b >> 32;
	if (hb) {
		return 63 - __builtin_clz(hb);
	} else {
		return 31 - __builtin_clz((int) b);
	}


#elif defined(USE_MASM_X86)
	__asm {
		xor eax, eax
		bsr edx, dword ptr b+4
		jnz l1
		bsr edx, dword ptr b
		mov eax, 32
		jnz l1
		mov edx, -32
	l1:	add eax, edx
	}

	
#else

	const int magic[64] = {
		63, 0, 58, 1, 59, 47, 53, 2,
		60, 39, 48, 27, 54, 33, 42, 3,
		61, 51, 37, 40, 49, 18, 28, 20,
		55, 30, 34, 11, 43, 14, 22, 4,
		62, 57, 46, 52, 38, 26, 32, 41,
		50, 36, 17, 19, 29, 10, 13, 21,
		56, 45, 25, 31, 35, 16, 9, 12,
		44, 24, 15, 8, 23, 7, 6, 5
	};

	b |= b >> 1;
	b |= b >> 2;
	b |= b >> 4;
	b |= b >> 8;
	b |= b >> 16;
	b |= b >> 32;
	b = (b >> 1) + 1;

	return magic[(b * 0x07EDD5E59A4E28C2ULL) >> 58];

#endif
}

/**
 * @brief Transpose the unsigned long long (symetry % A1-H8 diagonal).
 * @param b An unsigned long long
 * @return The transposed unsigned long long.
 */
unsigned long long transpose(unsigned long long b)
{
	unsigned long long t;

	t = (b ^ (b >> 7)) & 0x00aa00aa00aa00aaULL;
	b = b ^ t ^ (t << 7);
	t = (b ^ (b >> 14)) & 0x0000cccc0000ccccULL;
	b = b ^ t ^ (t << 14);
	t = (b ^ (b >> 28)) & 0x00000000f0f0f0f0ULL;
	b = b ^ t ^ (t << 28);

	return b;
}

/**
 * @brief Mirror the unsigned long long (exchange the lines A - H, B - G, C - F & D - E.).
 * @param b An unsigned long long
 * @return The mirrored unsigned long long.
 */
unsigned long long vertical_mirror(unsigned long long b)
{
	b = ((b >>  8) & 0x00FF00FF00FF00FFULL) | ((b <<  8) & 0xFF00FF00FF00FF00ULL);
	b = ((b >> 16) & 0x0000FFFF0000FFFFULL) | ((b << 16) & 0xFFFF0000FFFF0000ULL);
	b = ((b >> 32) & 0x00000000FFFFFFFFULL) | ((b << 32) & 0xFFFFFFFF00000000ULL);
	return b;
}

/**
 * @brief Mirror the unsigned long long (exchange the line 1 - 8, 2 - 7, 3 - 6 & 4 - 5).
 * @param b An unsigned long long.
 * @return The mirrored unsigned long long.
 */
unsigned long long horizontal_mirror(unsigned long long b)
{
  b = ((b >> 1) & 0x5555555555555555ULL) | ((b << 1) & 0xAAAAAAAAAAAAAAAAULL);
  b = ((b >> 2) & 0x3333333333333333ULL) | ((b << 2) & 0xCCCCCCCCCCCCCCCCULL);
  b = ((b >> 4) & 0x0F0F0F0F0F0F0F0FULL) | ((b << 4) & 0xF0F0F0F0F0F0F0F0ULL);

  return b;
}

/**
 * @brief Swap bytes of a short (little <-> big endian).
 * @param s An unsigned short.
 * @return The mirrored short.
 */
unsigned short bswap_short(unsigned short s)
{
	return (unsigned short) ((s >> 8) & 0x00FF) | ((s <<  8) & 0xFF00);
}

/**
 * @brief Mirror the unsigned int (little <-> big endian).
 * @param i An unsigned int.
 * @return The mirrored int.
 */
unsigned int bswap_int(unsigned int i)
{
	i = ((i >>  8) & 0x00FF00FFU) | ((i <<  8) & 0xFF00FF00U);
	i = ((i >> 16) & 0x0000FFFFU) | ((i << 16) & 0xFFFF0000U);
	return i;
}

#if 0
/**
 * @brief Get a random set bit index.
 *
 * @param b The unsigned long long.
 * @param r The pseudo-number generator.
 * @return a random bit index, or -1 if b value is zero.
 */
int get_rand_bit(unsigned long long b, Random *r)
{
	int n = bit_count(b), x;

	if (n == 0) return -1;

	n = random_get(r) % n;
	foreach_bit(x, b) if (n-- == 0) return x;

	return -2; // impossible.
}
#endif

/**
 * @brief Print an unsigned long long as a board.
 *
 * Write a 64-bit long number as an Othello board.
 *
 * @param b The unsigned long long.
 * @param f Output stream.
 */
void bitboard_write(const unsigned long long b, FILE *f)
{
	int i, j, x;
	const char *color = ".X";

	fputs("  A B C D E F G H\n", f);
	for (i = 0; i < 8; ++i) {
		fputc(i + '1', f);
		fputc(' ', f);
		for (j = 0; j < 8; ++j) {
			x = i * 8 + j;
			fputc(color[((b >> (unsigned)x) & 1)], f);
			fputc(' ', f);
		}
		fputc(i + '1', f);
		fputc('\n', f);
	}
	fputs("  A B C D E F G H\n", f);
}





/*
 * $File: board.cc
 * $Date: Wed Jan 30 19:52:47 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/








static ull hash_board[16][256];

void Board::setup()
{
	Random random;
	for (int i = 0; i < 16; i ++)
		for (int j = 0; j < 256; j ++)
			do
			{
				hash_board[i][j] = random.get();
			} while (bit_count(hash_board[i][j]) < 8);
}

void Board::init()
{
	player   = 0x0000000810000000ULL; // BLACK
	opponent = 0x0000001008000000ULL; // WHITE
}

ull Board::gen_move(int x, Move *move)
{
	move->flipped = flip[x](player, opponent);
	move->x = x;
	return move->flipped;
}

void Board::update(const Move *move)
{
	player ^= (move->flipped | x_to_bit(move->x));
	opponent ^= move->flipped;
	swap_players();
}

void Board::update(int x)
{
	Move move[1];
	gen_move(x, move);
	update(move);
}

void Board::restore(const Move *move)
{
	swap_players();
	opponent ^= move->flipped;
	player ^= (move->flipped | x_to_bit(move->x));
}


void Board::swap_players()
{
	const ull tmp = player;
	player = opponent;
	opponent = tmp;
}

void Board::pass_update()
{
	swap_players();
}

void Board::pass_restore()
{
	swap_players();
}

static inline unsigned long long get_some_moves(const unsigned long long P, const unsigned long long mask, const int dir)
{
	// kogge-stone algorithm
 	// 6 << + 6 >> + 12 & + 7 |
	// + better instruction independency
	register unsigned long long flip_l, flip_r;
	register unsigned long long mask_l, mask_r;
	register int d;

	flip_l = flip_r = P;
	mask_l = mask_r = mask;
	d = dir;

	flip_l |= mask_l & (flip_l << d);   flip_r |= mask_r & (flip_r >> d);
	mask_l &= (mask_l << d);            mask_r &= (mask_r >> d);
	d <<= 1;
	flip_l |= mask_l & (flip_l << d);   flip_r |= mask_r & (flip_r >> d);
	mask_l &= (mask_l << d);            mask_r &= (mask_r >> d);
	d <<= 1;
	flip_l |= mask_l & (flip_l << d);   flip_r |= mask_r & (flip_r >> d);

	return ((flip_l & mask) << dir) | ((flip_r & mask) >> dir);
}

static ull board_get_moves(ull player, ull opponent)
{
	const unsigned long long mask = opponent & 0x7E7E7E7E7E7E7E7Eull;

	return (get_some_moves(player, mask, 1) // horizontal
		| get_some_moves(player, opponent, 8)   // vertical
		| get_some_moves(player, mask, 7)   // diagonals
		| get_some_moves(player, mask, 9))
		& ~(player|opponent); // mask with empties
}

ull Board::get_moves() const
{
	return board_get_moves(player, opponent);
}

bool board_can_move(ull player, ull opponent)
{
	ull E = ~(player|opponent); // empties

	return (get_some_moves(player, opponent & 0x007E7E7E7E7E7E00ull, 7) & E)  // diagonals
		|| (get_some_moves(player, opponent & 0x007E7E7E7E7E7E00ull, 9) & E)
		|| (get_some_moves(player, opponent & 0x7E7E7E7E7E7E7E7Eull, 1) & E)  // horizontal
		|| (get_some_moves(player, opponent & 0x00FFFFFFFFFFFF00ull, 8) & E); // vertical
}

bool Board::can_move() const
{
	return board_can_move(player, opponent);
}

bool Board::can_opponent_move() const
{
	return board_can_move(opponent, player);
}

int Board::mobility() const
{
	return bit_count(get_moves());
}

int Board::weighted_mobility() const
{
	return bit_weighted_count(get_moves());
}

static inline unsigned long long get_some_potential_moves(const unsigned long long P, const int dir)
{
	return (P << dir | P >> dir);
}

ull Board::get_potential_moves() const
{
	return (get_some_potential_moves(opponent & 0x7E7E7E7E7E7E7E7Eull, 1) // horizontal
		| get_some_potential_moves(opponent & 0x00FFFFFFFFFFFF00ull, 8)   // vertical
		| get_some_potential_moves(opponent & 0x007E7E7E7E7E7E00ull, 7)   // diagonals
		| get_some_potential_moves(opponent & 0x007E7E7E7E7E7E00ull, 9))
		& ~(player|opponent); // mask with empties
}

ull Board::get_hash_code() const
{
	unsigned long long h;
	const unsigned char *p = (const unsigned char*)this;

	h  = hash_board[0][p[0]];
	h ^= hash_board[1][p[1]];
	h ^= hash_board[2][p[2]];
	h ^= hash_board[3][p[3]];
	h ^= hash_board[4][p[4]];
	h ^= hash_board[5][p[5]];
	h ^= hash_board[6][p[6]];
	h ^= hash_board[7][p[7]];
	h ^= hash_board[8][p[8]];
	h ^= hash_board[9][p[9]];
	h ^= hash_board[10][p[10]];
	h ^= hash_board[11][p[11]];
	h ^= hash_board[12][p[12]];
	h ^= hash_board[13][p[13]];
	h ^= hash_board[14][p[14]];
	h ^= hash_board[15][p[15]];

	return h;
}

int Board::potential_mobility() const
{
	return bit_weighted_count(get_potential_moves());
}

int Board::corner_stability() const
{
	const unsigned long long stable = ((((0x0100000000000001ULL & player) << 1) | ((0x8000000000000080ULL & player) >> 1) | ((0x0000000000000081ULL & player) << 8) | ((0x8100000000000000ULL & player) >> 8) | 0x8100000000000081ULL) & player);
	return bit_count(stable);

}

// XXX: TODO
int Board::edge_stability() const
{
	return 0;
}

int Board::count_empties() const
{
	return bit_count(~(player | opponent));
}

void Board::from_string(const std::string &str)
{
	assert(str.length() >= BOARD_SIZE + 2);

	player = opponent = 0;
	for (int i = 0; i < 8; i ++)
		for (int j = 0; j < 8; j ++)
		{
			int x =	i * 8 + j;
			char c = str[x];
			if (c == 'X' || c == 'x')
				player |= x_to_bit(x);
			else if (c == 'O' || c == 'o')
				opponent |= x_to_bit(x);
		}
	char c = str[BOARD_SIZE + 1];
	if (c == 'O' || c == 'o')
		swap_players();
}

void Board::print(int turn) const
{
	do_print(turn, COORD_NUMBER, stderr);
}

void Board::do_print(int turn, CoordType coord_type, FILE *f) const
{
	ull moves;
	if (turn == BLACK)
		moves = board_get_moves(player, opponent);
	else moves = board_get_moves(opponent, player);

	// put char
#define pc(c) fputc((c), f)

	char coord[2][9] = {"ABCDEFGH", "01234567"};
	pc(' ');
	for (int i = 0; i < 8; i ++)
	{
		pc(' ');
		pc(coord[coord_type][i]);
	}
	pc('\n');

	for (int i = 0; i < 8; i ++)
	{
		pc(coord[1][i]);
		for (int j = 0; j < 8; j ++)
		{
			int x = i * 8 + j;
			char c;
			if (player & x_to_bit(x))
				c = 'X';
			else if (opponent & x_to_bit(x))
				c = 'O';
			else if (moves & x_to_bit(x))
				c = '.';
			else c = '-';

			pc(' ');
			pc(c);
		}
		pc('\n');
	}

#undef pc
}

/*
 * $File: eval.cc
 * $Date: Fri Feb 01 18:36:11 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/








void Eval::pass()
{
	for (int i = 0; i < n_feature; i ++)
		feature[i]->pass();
}

void Eval::update(Move *move)
{
	for (int i = 0; i < n_feature; i ++)
		feature[i]->update(move);
}

void Eval::restore(Move *move)
{
	for (int i = 0; i < n_feature; i ++)
		feature[i]->restore(move);
}

void Eval::set(Board *board)
{
	for (int i = 0; i < n_feature; i ++)
		feature[i]->set(board);
}

void Eval::setup()
{
}

Eval::Eval()
{
}
/*
 * $File: feature.cc
 * $Date: Fri Feb 01 18:35:46 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/



int Feature::eval()
{
	throw "Not implemented!";
}

void Feature::pass()
{
	throw "Not implemented!";
}

void Feature::update(Move *)
{
	throw "Not implemented!";
}

void Feature::restore(Move *)
{
	throw "Not implemented!";
}

void Feature::set(Board *)
{
	throw "Not implemented!";
}


int DiscFeature::eval()
{
	throw "Not implemented!";
}

void DiscFeature::pass()
{
	throw "Not implemented!";
}

void DiscFeature::update(Move *move)
{
	throw "Not implemented!";
}

void DiscFeature::restore(Move *move)
{
	throw "Not implemented!";
}

void DiscFeature::set(Board *board)
{
	throw "Not implemented!";
}




/**
 * @file flip_kindergarten.c
 *
 * This module deals with flipping discs.
 *
 * A function is provided for each square of the board. These functions are
 * gathered into an array of functions, so that a fast access to each function
 * is allowed. The generic form of the function take as input the player and
 * the opponent bitboards and return the flipped squares into a bitboard.
 *
 * Given the following notation:
 *  - x = square where we play,
 *  - P = player's disc pattern,
 *  - O = opponent's disc pattern,
 * the basic principle is to read into an array the result of a move. Doing
 * this is easier for a single line ; so we can use arrays of the form:
 *  - ARRAY[x][8-bits disc pattern].
 * The problem is thus to convert any line of a 64-bits disc pattern into an
 * 8-bits disc pattern. A fast way to do this is to select the right line,
 * with a bit-mask, to gather the masked-bits into a continuous set by a simple
 * multiplication and to right-shift the result to scale it into a number
 * between 0 and 255.
 * Once we get our 8-bits disc patterns,a first array (OUTFLANK) is used to
 * get the player's discs that surround the opponent discs:
 *  - outflank = OUTFLANK[x][O] & P
 * The result is then used as an index to access a second array giving the
 * flipped discs according to the surrounding player's discs:
 *  - flipped = FLIPPED[x][outflank].
 * Finally, a precomputed array transform the 8-bits disc pattern back into a
 * 64-bits disc pattern, and the flipped squares for each line are gathered and
 * returned to generate moves.
 *
 * Adapted by Xinyu Zhou.
 * Thanks Richard Delorme's great job!
 *
 * File automatically generated
 * @date 1998 - 2012
 * @author Richard Delorme
 * @version 4.3
 */



/** outflank array */
const unsigned char OUTFLANK[8][64] = {
	{
		0x00, 0x04, 0x00, 0x08, 0x00, 0x04, 0x00, 0x10, 0x00, 0x04, 0x00, 0x08, 0x00, 0x04, 0x00, 0x20,
		0x00, 0x04, 0x00, 0x08, 0x00, 0x04, 0x00, 0x10, 0x00, 0x04, 0x00, 0x08, 0x00, 0x04, 0x00, 0x40,
		0x00, 0x04, 0x00, 0x08, 0x00, 0x04, 0x00, 0x10, 0x00, 0x04, 0x00, 0x08, 0x00, 0x04, 0x00, 0x20,
		0x00, 0x04, 0x00, 0x08, 0x00, 0x04, 0x00, 0x10, 0x00, 0x04, 0x00, 0x08, 0x00, 0x04, 0x00, 0x80,
	},
	{
		0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x20, 0x00,
		0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x40, 0x00,
		0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x20, 0x00,
		0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x80, 0x00,
	},
	{
		0x00, 0x01, 0x00, 0x00, 0x10, 0x11, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x20, 0x21, 0x00, 0x00,
		0x00, 0x01, 0x00, 0x00, 0x10, 0x11, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x40, 0x41, 0x00, 0x00,
		0x00, 0x01, 0x00, 0x00, 0x10, 0x11, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x20, 0x21, 0x00, 0x00,
		0x00, 0x01, 0x00, 0x00, 0x10, 0x11, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x80, 0x81, 0x00, 0x00,
	},
	{
		0x00, 0x00, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x20, 0x20, 0x22, 0x21, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x40, 0x40, 0x42, 0x41, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x20, 0x20, 0x22, 0x21, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x80, 0x80, 0x82, 0x81, 0x00, 0x00, 0x00, 0x00,
	},
	{
		0x00, 0x00, 0x00, 0x00, 0x04, 0x04, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x40, 0x40, 0x40, 0x40, 0x44, 0x44, 0x42, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x04, 0x04, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x80, 0x80, 0x80, 0x80, 0x84, 0x84, 0x82, 0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	},
	{
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x08, 0x08, 0x08, 0x04, 0x04, 0x02, 0x01,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x88, 0x88, 0x88, 0x88, 0x84, 0x84, 0x82, 0x81,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	},
	{
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x08, 0x08, 0x08, 0x08, 0x04, 0x04, 0x02, 0x01,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	},
	{
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
		0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x08, 0x08, 0x08, 0x08, 0x04, 0x04, 0x02, 0x01,
	},
};

/** flip array */
const unsigned char FLIPPED[8][144] = {
	{
		0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x0f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x1f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	},
	{
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x1e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x3e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	},
	{
		0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x04, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x0c, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x1c, 0x1d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x3c, 0x3d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	},
	{
		0x00, 0x03, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x08, 0x0b, 0x0a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x18, 0x1b, 0x1a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x38, 0x3b, 0x3a, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	},
	{
		0x00, 0x07, 0x06, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x10, 0x17, 0x16, 0x00, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x30, 0x37, 0x36, 0x00, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	},
	{
		0x00, 0x0f, 0x0e, 0x00, 0x0c, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x20, 0x2f, 0x2e, 0x00, 0x2c, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	},
	{
		0x00, 0x1f, 0x1e, 0x00, 0x1c, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	},
	{
		0x00, 0x3f, 0x3e, 0x00, 0x3c, 0x00, 0x00, 0x00, 0x38, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	},
};

/** conversion from an 8-bit line to the B1-G1- line */
unsigned long long B1G1[64] = {
		0x0000000000000000ULL, 0x0000000000000002ULL, 0x0000000000000004ULL, 0x0000000000000006ULL, 0x0000000000000008ULL, 0x000000000000000aULL, 0x000000000000000cULL, 0x000000000000000eULL,
		0x0000000000000010ULL, 0x0000000000000012ULL, 0x0000000000000014ULL, 0x0000000000000016ULL, 0x0000000000000018ULL, 0x000000000000001aULL, 0x000000000000001cULL, 0x000000000000001eULL,
		0x0000000000000020ULL, 0x0000000000000022ULL, 0x0000000000000024ULL, 0x0000000000000026ULL, 0x0000000000000028ULL, 0x000000000000002aULL, 0x000000000000002cULL, 0x000000000000002eULL,
		0x0000000000000030ULL, 0x0000000000000032ULL, 0x0000000000000034ULL, 0x0000000000000036ULL, 0x0000000000000038ULL, 0x000000000000003aULL, 0x000000000000003cULL, 0x000000000000003eULL,
		0x0000000000000040ULL, 0x0000000000000042ULL, 0x0000000000000044ULL, 0x0000000000000046ULL, 0x0000000000000048ULL, 0x000000000000004aULL, 0x000000000000004cULL, 0x000000000000004eULL,
		0x0000000000000050ULL, 0x0000000000000052ULL, 0x0000000000000054ULL, 0x0000000000000056ULL, 0x0000000000000058ULL, 0x000000000000005aULL, 0x000000000000005cULL, 0x000000000000005eULL,
		0x0000000000000060ULL, 0x0000000000000062ULL, 0x0000000000000064ULL, 0x0000000000000066ULL, 0x0000000000000068ULL, 0x000000000000006aULL, 0x000000000000006cULL, 0x000000000000006eULL,
		0x0000000000000070ULL, 0x0000000000000072ULL, 0x0000000000000074ULL, 0x0000000000000076ULL, 0x0000000000000078ULL, 0x000000000000007aULL, 0x000000000000007cULL, 0x000000000000007eULL,
};

/** conversion from an 8-bit line to the B2-G2- line */
unsigned long long B2G2[64] = {
		0x0000000000000000ULL, 0x0000000000000200ULL, 0x0000000000000400ULL, 0x0000000000000600ULL, 0x0000000000000800ULL, 0x0000000000000a00ULL, 0x0000000000000c00ULL, 0x0000000000000e00ULL,
		0x0000000000001000ULL, 0x0000000000001200ULL, 0x0000000000001400ULL, 0x0000000000001600ULL, 0x0000000000001800ULL, 0x0000000000001a00ULL, 0x0000000000001c00ULL, 0x0000000000001e00ULL,
		0x0000000000002000ULL, 0x0000000000002200ULL, 0x0000000000002400ULL, 0x0000000000002600ULL, 0x0000000000002800ULL, 0x0000000000002a00ULL, 0x0000000000002c00ULL, 0x0000000000002e00ULL,
		0x0000000000003000ULL, 0x0000000000003200ULL, 0x0000000000003400ULL, 0x0000000000003600ULL, 0x0000000000003800ULL, 0x0000000000003a00ULL, 0x0000000000003c00ULL, 0x0000000000003e00ULL,
		0x0000000000004000ULL, 0x0000000000004200ULL, 0x0000000000004400ULL, 0x0000000000004600ULL, 0x0000000000004800ULL, 0x0000000000004a00ULL, 0x0000000000004c00ULL, 0x0000000000004e00ULL,
		0x0000000000005000ULL, 0x0000000000005200ULL, 0x0000000000005400ULL, 0x0000000000005600ULL, 0x0000000000005800ULL, 0x0000000000005a00ULL, 0x0000000000005c00ULL, 0x0000000000005e00ULL,
		0x0000000000006000ULL, 0x0000000000006200ULL, 0x0000000000006400ULL, 0x0000000000006600ULL, 0x0000000000006800ULL, 0x0000000000006a00ULL, 0x0000000000006c00ULL, 0x0000000000006e00ULL,
		0x0000000000007000ULL, 0x0000000000007200ULL, 0x0000000000007400ULL, 0x0000000000007600ULL, 0x0000000000007800ULL, 0x0000000000007a00ULL, 0x0000000000007c00ULL, 0x0000000000007e00ULL,
};

/** conversion from an 8-bit line to the B3-G3- line */
unsigned long long B3G3[64] = {
		0x0000000000000000ULL, 0x0000000000020000ULL, 0x0000000000040000ULL, 0x0000000000060000ULL, 0x0000000000080000ULL, 0x00000000000a0000ULL, 0x00000000000c0000ULL, 0x00000000000e0000ULL,
		0x0000000000100000ULL, 0x0000000000120000ULL, 0x0000000000140000ULL, 0x0000000000160000ULL, 0x0000000000180000ULL, 0x00000000001a0000ULL, 0x00000000001c0000ULL, 0x00000000001e0000ULL,
		0x0000000000200000ULL, 0x0000000000220000ULL, 0x0000000000240000ULL, 0x0000000000260000ULL, 0x0000000000280000ULL, 0x00000000002a0000ULL, 0x00000000002c0000ULL, 0x00000000002e0000ULL,
		0x0000000000300000ULL, 0x0000000000320000ULL, 0x0000000000340000ULL, 0x0000000000360000ULL, 0x0000000000380000ULL, 0x00000000003a0000ULL, 0x00000000003c0000ULL, 0x00000000003e0000ULL,
		0x0000000000400000ULL, 0x0000000000420000ULL, 0x0000000000440000ULL, 0x0000000000460000ULL, 0x0000000000480000ULL, 0x00000000004a0000ULL, 0x00000000004c0000ULL, 0x00000000004e0000ULL,
		0x0000000000500000ULL, 0x0000000000520000ULL, 0x0000000000540000ULL, 0x0000000000560000ULL, 0x0000000000580000ULL, 0x00000000005a0000ULL, 0x00000000005c0000ULL, 0x00000000005e0000ULL,
		0x0000000000600000ULL, 0x0000000000620000ULL, 0x0000000000640000ULL, 0x0000000000660000ULL, 0x0000000000680000ULL, 0x00000000006a0000ULL, 0x00000000006c0000ULL, 0x00000000006e0000ULL,
		0x0000000000700000ULL, 0x0000000000720000ULL, 0x0000000000740000ULL, 0x0000000000760000ULL, 0x0000000000780000ULL, 0x00000000007a0000ULL, 0x00000000007c0000ULL, 0x00000000007e0000ULL,
};

/** conversion from an 8-bit line to the B4-G4- line */
unsigned long long B4G4[64] = {
		0x0000000000000000ULL, 0x0000000002000000ULL, 0x0000000004000000ULL, 0x0000000006000000ULL, 0x0000000008000000ULL, 0x000000000a000000ULL, 0x000000000c000000ULL, 0x000000000e000000ULL,
		0x0000000010000000ULL, 0x0000000012000000ULL, 0x0000000014000000ULL, 0x0000000016000000ULL, 0x0000000018000000ULL, 0x000000001a000000ULL, 0x000000001c000000ULL, 0x000000001e000000ULL,
		0x0000000020000000ULL, 0x0000000022000000ULL, 0x0000000024000000ULL, 0x0000000026000000ULL, 0x0000000028000000ULL, 0x000000002a000000ULL, 0x000000002c000000ULL, 0x000000002e000000ULL,
		0x0000000030000000ULL, 0x0000000032000000ULL, 0x0000000034000000ULL, 0x0000000036000000ULL, 0x0000000038000000ULL, 0x000000003a000000ULL, 0x000000003c000000ULL, 0x000000003e000000ULL,
		0x0000000040000000ULL, 0x0000000042000000ULL, 0x0000000044000000ULL, 0x0000000046000000ULL, 0x0000000048000000ULL, 0x000000004a000000ULL, 0x000000004c000000ULL, 0x000000004e000000ULL,
		0x0000000050000000ULL, 0x0000000052000000ULL, 0x0000000054000000ULL, 0x0000000056000000ULL, 0x0000000058000000ULL, 0x000000005a000000ULL, 0x000000005c000000ULL, 0x000000005e000000ULL,
		0x0000000060000000ULL, 0x0000000062000000ULL, 0x0000000064000000ULL, 0x0000000066000000ULL, 0x0000000068000000ULL, 0x000000006a000000ULL, 0x000000006c000000ULL, 0x000000006e000000ULL,
		0x0000000070000000ULL, 0x0000000072000000ULL, 0x0000000074000000ULL, 0x0000000076000000ULL, 0x0000000078000000ULL, 0x000000007a000000ULL, 0x000000007c000000ULL, 0x000000007e000000ULL,
};

/** conversion from an 8-bit line to the B5-G5- line */
unsigned long long B5G5[64] = {
		0x0000000000000000ULL, 0x0000000200000000ULL, 0x0000000400000000ULL, 0x0000000600000000ULL, 0x0000000800000000ULL, 0x0000000a00000000ULL, 0x0000000c00000000ULL, 0x0000000e00000000ULL,
		0x0000001000000000ULL, 0x0000001200000000ULL, 0x0000001400000000ULL, 0x0000001600000000ULL, 0x0000001800000000ULL, 0x0000001a00000000ULL, 0x0000001c00000000ULL, 0x0000001e00000000ULL,
		0x0000002000000000ULL, 0x0000002200000000ULL, 0x0000002400000000ULL, 0x0000002600000000ULL, 0x0000002800000000ULL, 0x0000002a00000000ULL, 0x0000002c00000000ULL, 0x0000002e00000000ULL,
		0x0000003000000000ULL, 0x0000003200000000ULL, 0x0000003400000000ULL, 0x0000003600000000ULL, 0x0000003800000000ULL, 0x0000003a00000000ULL, 0x0000003c00000000ULL, 0x0000003e00000000ULL,
		0x0000004000000000ULL, 0x0000004200000000ULL, 0x0000004400000000ULL, 0x0000004600000000ULL, 0x0000004800000000ULL, 0x0000004a00000000ULL, 0x0000004c00000000ULL, 0x0000004e00000000ULL,
		0x0000005000000000ULL, 0x0000005200000000ULL, 0x0000005400000000ULL, 0x0000005600000000ULL, 0x0000005800000000ULL, 0x0000005a00000000ULL, 0x0000005c00000000ULL, 0x0000005e00000000ULL,
		0x0000006000000000ULL, 0x0000006200000000ULL, 0x0000006400000000ULL, 0x0000006600000000ULL, 0x0000006800000000ULL, 0x0000006a00000000ULL, 0x0000006c00000000ULL, 0x0000006e00000000ULL,
		0x0000007000000000ULL, 0x0000007200000000ULL, 0x0000007400000000ULL, 0x0000007600000000ULL, 0x0000007800000000ULL, 0x0000007a00000000ULL, 0x0000007c00000000ULL, 0x0000007e00000000ULL,
};

/** conversion from an 8-bit line to the B6-G6- line */
unsigned long long B6G6[64] = {
		0x0000000000000000ULL, 0x0000020000000000ULL, 0x0000040000000000ULL, 0x0000060000000000ULL, 0x0000080000000000ULL, 0x00000a0000000000ULL, 0x00000c0000000000ULL, 0x00000e0000000000ULL,
		0x0000100000000000ULL, 0x0000120000000000ULL, 0x0000140000000000ULL, 0x0000160000000000ULL, 0x0000180000000000ULL, 0x00001a0000000000ULL, 0x00001c0000000000ULL, 0x00001e0000000000ULL,
		0x0000200000000000ULL, 0x0000220000000000ULL, 0x0000240000000000ULL, 0x0000260000000000ULL, 0x0000280000000000ULL, 0x00002a0000000000ULL, 0x00002c0000000000ULL, 0x00002e0000000000ULL,
		0x0000300000000000ULL, 0x0000320000000000ULL, 0x0000340000000000ULL, 0x0000360000000000ULL, 0x0000380000000000ULL, 0x00003a0000000000ULL, 0x00003c0000000000ULL, 0x00003e0000000000ULL,
		0x0000400000000000ULL, 0x0000420000000000ULL, 0x0000440000000000ULL, 0x0000460000000000ULL, 0x0000480000000000ULL, 0x00004a0000000000ULL, 0x00004c0000000000ULL, 0x00004e0000000000ULL,
		0x0000500000000000ULL, 0x0000520000000000ULL, 0x0000540000000000ULL, 0x0000560000000000ULL, 0x0000580000000000ULL, 0x00005a0000000000ULL, 0x00005c0000000000ULL, 0x00005e0000000000ULL,
		0x0000600000000000ULL, 0x0000620000000000ULL, 0x0000640000000000ULL, 0x0000660000000000ULL, 0x0000680000000000ULL, 0x00006a0000000000ULL, 0x00006c0000000000ULL, 0x00006e0000000000ULL,
		0x0000700000000000ULL, 0x0000720000000000ULL, 0x0000740000000000ULL, 0x0000760000000000ULL, 0x0000780000000000ULL, 0x00007a0000000000ULL, 0x00007c0000000000ULL, 0x00007e0000000000ULL,
};

/** conversion from an 8-bit line to the B7-G7- line */
unsigned long long B7G7[64] = {
		0x0000000000000000ULL, 0x0002000000000000ULL, 0x0004000000000000ULL, 0x0006000000000000ULL, 0x0008000000000000ULL, 0x000a000000000000ULL, 0x000c000000000000ULL, 0x000e000000000000ULL,
		0x0010000000000000ULL, 0x0012000000000000ULL, 0x0014000000000000ULL, 0x0016000000000000ULL, 0x0018000000000000ULL, 0x001a000000000000ULL, 0x001c000000000000ULL, 0x001e000000000000ULL,
		0x0020000000000000ULL, 0x0022000000000000ULL, 0x0024000000000000ULL, 0x0026000000000000ULL, 0x0028000000000000ULL, 0x002a000000000000ULL, 0x002c000000000000ULL, 0x002e000000000000ULL,
		0x0030000000000000ULL, 0x0032000000000000ULL, 0x0034000000000000ULL, 0x0036000000000000ULL, 0x0038000000000000ULL, 0x003a000000000000ULL, 0x003c000000000000ULL, 0x003e000000000000ULL,
		0x0040000000000000ULL, 0x0042000000000000ULL, 0x0044000000000000ULL, 0x0046000000000000ULL, 0x0048000000000000ULL, 0x004a000000000000ULL, 0x004c000000000000ULL, 0x004e000000000000ULL,
		0x0050000000000000ULL, 0x0052000000000000ULL, 0x0054000000000000ULL, 0x0056000000000000ULL, 0x0058000000000000ULL, 0x005a000000000000ULL, 0x005c000000000000ULL, 0x005e000000000000ULL,
		0x0060000000000000ULL, 0x0062000000000000ULL, 0x0064000000000000ULL, 0x0066000000000000ULL, 0x0068000000000000ULL, 0x006a000000000000ULL, 0x006c000000000000ULL, 0x006e000000000000ULL,
		0x0070000000000000ULL, 0x0072000000000000ULL, 0x0074000000000000ULL, 0x0076000000000000ULL, 0x0078000000000000ULL, 0x007a000000000000ULL, 0x007c000000000000ULL, 0x007e000000000000ULL,
};

/** conversion from an 8-bit line to the B8-G8- line */
unsigned long long B8G8[64] = {
		0x0000000000000000ULL, 0x0200000000000000ULL, 0x0400000000000000ULL, 0x0600000000000000ULL, 0x0800000000000000ULL, 0x0a00000000000000ULL, 0x0c00000000000000ULL, 0x0e00000000000000ULL,
		0x1000000000000000ULL, 0x1200000000000000ULL, 0x1400000000000000ULL, 0x1600000000000000ULL, 0x1800000000000000ULL, 0x1a00000000000000ULL, 0x1c00000000000000ULL, 0x1e00000000000000ULL,
		0x2000000000000000ULL, 0x2200000000000000ULL, 0x2400000000000000ULL, 0x2600000000000000ULL, 0x2800000000000000ULL, 0x2a00000000000000ULL, 0x2c00000000000000ULL, 0x2e00000000000000ULL,
		0x3000000000000000ULL, 0x3200000000000000ULL, 0x3400000000000000ULL, 0x3600000000000000ULL, 0x3800000000000000ULL, 0x3a00000000000000ULL, 0x3c00000000000000ULL, 0x3e00000000000000ULL,
		0x4000000000000000ULL, 0x4200000000000000ULL, 0x4400000000000000ULL, 0x4600000000000000ULL, 0x4800000000000000ULL, 0x4a00000000000000ULL, 0x4c00000000000000ULL, 0x4e00000000000000ULL,
		0x5000000000000000ULL, 0x5200000000000000ULL, 0x5400000000000000ULL, 0x5600000000000000ULL, 0x5800000000000000ULL, 0x5a00000000000000ULL, 0x5c00000000000000ULL, 0x5e00000000000000ULL,
		0x6000000000000000ULL, 0x6200000000000000ULL, 0x6400000000000000ULL, 0x6600000000000000ULL, 0x6800000000000000ULL, 0x6a00000000000000ULL, 0x6c00000000000000ULL, 0x6e00000000000000ULL,
		0x7000000000000000ULL, 0x7200000000000000ULL, 0x7400000000000000ULL, 0x7600000000000000ULL, 0x7800000000000000ULL, 0x7a00000000000000ULL, 0x7c00000000000000ULL, 0x7e00000000000000ULL,
};

/** conversion from an 8-bit line to the A2-A7- line */
unsigned long long A2A7[64] = {
		0x0000000000000000ULL, 0x0000000000000100ULL, 0x0000000000010000ULL, 0x0000000000010100ULL, 0x0000000001000000ULL, 0x0000000001000100ULL, 0x0000000001010000ULL, 0x0000000001010100ULL,
		0x0000000100000000ULL, 0x0000000100000100ULL, 0x0000000100010000ULL, 0x0000000100010100ULL, 0x0000000101000000ULL, 0x0000000101000100ULL, 0x0000000101010000ULL, 0x0000000101010100ULL,
		0x0000010000000000ULL, 0x0000010000000100ULL, 0x0000010000010000ULL, 0x0000010000010100ULL, 0x0000010001000000ULL, 0x0000010001000100ULL, 0x0000010001010000ULL, 0x0000010001010100ULL,
		0x0000010100000000ULL, 0x0000010100000100ULL, 0x0000010100010000ULL, 0x0000010100010100ULL, 0x0000010101000000ULL, 0x0000010101000100ULL, 0x0000010101010000ULL, 0x0000010101010100ULL,
		0x0001000000000000ULL, 0x0001000000000100ULL, 0x0001000000010000ULL, 0x0001000000010100ULL, 0x0001000001000000ULL, 0x0001000001000100ULL, 0x0001000001010000ULL, 0x0001000001010100ULL,
		0x0001000100000000ULL, 0x0001000100000100ULL, 0x0001000100010000ULL, 0x0001000100010100ULL, 0x0001000101000000ULL, 0x0001000101000100ULL, 0x0001000101010000ULL, 0x0001000101010100ULL,
		0x0001010000000000ULL, 0x0001010000000100ULL, 0x0001010000010000ULL, 0x0001010000010100ULL, 0x0001010001000000ULL, 0x0001010001000100ULL, 0x0001010001010000ULL, 0x0001010001010100ULL,
		0x0001010100000000ULL, 0x0001010100000100ULL, 0x0001010100010000ULL, 0x0001010100010100ULL, 0x0001010101000000ULL, 0x0001010101000100ULL, 0x0001010101010000ULL, 0x0001010101010100ULL,
};

/** conversion from an 8-bit line to the B2-B7- line */
unsigned long long B2B7[64] = {
		0x0000000000000000ULL, 0x0000000000000200ULL, 0x0000000000020000ULL, 0x0000000000020200ULL, 0x0000000002000000ULL, 0x0000000002000200ULL, 0x0000000002020000ULL, 0x0000000002020200ULL,
		0x0000000200000000ULL, 0x0000000200000200ULL, 0x0000000200020000ULL, 0x0000000200020200ULL, 0x0000000202000000ULL, 0x0000000202000200ULL, 0x0000000202020000ULL, 0x0000000202020200ULL,
		0x0000020000000000ULL, 0x0000020000000200ULL, 0x0000020000020000ULL, 0x0000020000020200ULL, 0x0000020002000000ULL, 0x0000020002000200ULL, 0x0000020002020000ULL, 0x0000020002020200ULL,
		0x0000020200000000ULL, 0x0000020200000200ULL, 0x0000020200020000ULL, 0x0000020200020200ULL, 0x0000020202000000ULL, 0x0000020202000200ULL, 0x0000020202020000ULL, 0x0000020202020200ULL,
		0x0002000000000000ULL, 0x0002000000000200ULL, 0x0002000000020000ULL, 0x0002000000020200ULL, 0x0002000002000000ULL, 0x0002000002000200ULL, 0x0002000002020000ULL, 0x0002000002020200ULL,
		0x0002000200000000ULL, 0x0002000200000200ULL, 0x0002000200020000ULL, 0x0002000200020200ULL, 0x0002000202000000ULL, 0x0002000202000200ULL, 0x0002000202020000ULL, 0x0002000202020200ULL,
		0x0002020000000000ULL, 0x0002020000000200ULL, 0x0002020000020000ULL, 0x0002020000020200ULL, 0x0002020002000000ULL, 0x0002020002000200ULL, 0x0002020002020000ULL, 0x0002020002020200ULL,
		0x0002020200000000ULL, 0x0002020200000200ULL, 0x0002020200020000ULL, 0x0002020200020200ULL, 0x0002020202000000ULL, 0x0002020202000200ULL, 0x0002020202020000ULL, 0x0002020202020200ULL,
};

/** conversion from an 8-bit line to the C2-C7- line */
unsigned long long C2C7[64] = {
		0x0000000000000000ULL, 0x0000000000000400ULL, 0x0000000000040000ULL, 0x0000000000040400ULL, 0x0000000004000000ULL, 0x0000000004000400ULL, 0x0000000004040000ULL, 0x0000000004040400ULL,
		0x0000000400000000ULL, 0x0000000400000400ULL, 0x0000000400040000ULL, 0x0000000400040400ULL, 0x0000000404000000ULL, 0x0000000404000400ULL, 0x0000000404040000ULL, 0x0000000404040400ULL,
		0x0000040000000000ULL, 0x0000040000000400ULL, 0x0000040000040000ULL, 0x0000040000040400ULL, 0x0000040004000000ULL, 0x0000040004000400ULL, 0x0000040004040000ULL, 0x0000040004040400ULL,
		0x0000040400000000ULL, 0x0000040400000400ULL, 0x0000040400040000ULL, 0x0000040400040400ULL, 0x0000040404000000ULL, 0x0000040404000400ULL, 0x0000040404040000ULL, 0x0000040404040400ULL,
		0x0004000000000000ULL, 0x0004000000000400ULL, 0x0004000000040000ULL, 0x0004000000040400ULL, 0x0004000004000000ULL, 0x0004000004000400ULL, 0x0004000004040000ULL, 0x0004000004040400ULL,
		0x0004000400000000ULL, 0x0004000400000400ULL, 0x0004000400040000ULL, 0x0004000400040400ULL, 0x0004000404000000ULL, 0x0004000404000400ULL, 0x0004000404040000ULL, 0x0004000404040400ULL,
		0x0004040000000000ULL, 0x0004040000000400ULL, 0x0004040000040000ULL, 0x0004040000040400ULL, 0x0004040004000000ULL, 0x0004040004000400ULL, 0x0004040004040000ULL, 0x0004040004040400ULL,
		0x0004040400000000ULL, 0x0004040400000400ULL, 0x0004040400040000ULL, 0x0004040400040400ULL, 0x0004040404000000ULL, 0x0004040404000400ULL, 0x0004040404040000ULL, 0x0004040404040400ULL,
};

/** conversion from an 8-bit line to the D2-D7- line */
unsigned long long D2D7[64] = {
		0x0000000000000000ULL, 0x0000000000000800ULL, 0x0000000000080000ULL, 0x0000000000080800ULL, 0x0000000008000000ULL, 0x0000000008000800ULL, 0x0000000008080000ULL, 0x0000000008080800ULL,
		0x0000000800000000ULL, 0x0000000800000800ULL, 0x0000000800080000ULL, 0x0000000800080800ULL, 0x0000000808000000ULL, 0x0000000808000800ULL, 0x0000000808080000ULL, 0x0000000808080800ULL,
		0x0000080000000000ULL, 0x0000080000000800ULL, 0x0000080000080000ULL, 0x0000080000080800ULL, 0x0000080008000000ULL, 0x0000080008000800ULL, 0x0000080008080000ULL, 0x0000080008080800ULL,
		0x0000080800000000ULL, 0x0000080800000800ULL, 0x0000080800080000ULL, 0x0000080800080800ULL, 0x0000080808000000ULL, 0x0000080808000800ULL, 0x0000080808080000ULL, 0x0000080808080800ULL,
		0x0008000000000000ULL, 0x0008000000000800ULL, 0x0008000000080000ULL, 0x0008000000080800ULL, 0x0008000008000000ULL, 0x0008000008000800ULL, 0x0008000008080000ULL, 0x0008000008080800ULL,
		0x0008000800000000ULL, 0x0008000800000800ULL, 0x0008000800080000ULL, 0x0008000800080800ULL, 0x0008000808000000ULL, 0x0008000808000800ULL, 0x0008000808080000ULL, 0x0008000808080800ULL,
		0x0008080000000000ULL, 0x0008080000000800ULL, 0x0008080000080000ULL, 0x0008080000080800ULL, 0x0008080008000000ULL, 0x0008080008000800ULL, 0x0008080008080000ULL, 0x0008080008080800ULL,
		0x0008080800000000ULL, 0x0008080800000800ULL, 0x0008080800080000ULL, 0x0008080800080800ULL, 0x0008080808000000ULL, 0x0008080808000800ULL, 0x0008080808080000ULL, 0x0008080808080800ULL,
};

/** conversion from an 8-bit line to the E2-E7- line */
unsigned long long E2E7[64] = {
		0x0000000000000000ULL, 0x0000000000001000ULL, 0x0000000000100000ULL, 0x0000000000101000ULL, 0x0000000010000000ULL, 0x0000000010001000ULL, 0x0000000010100000ULL, 0x0000000010101000ULL,
		0x0000001000000000ULL, 0x0000001000001000ULL, 0x0000001000100000ULL, 0x0000001000101000ULL, 0x0000001010000000ULL, 0x0000001010001000ULL, 0x0000001010100000ULL, 0x0000001010101000ULL,
		0x0000100000000000ULL, 0x0000100000001000ULL, 0x0000100000100000ULL, 0x0000100000101000ULL, 0x0000100010000000ULL, 0x0000100010001000ULL, 0x0000100010100000ULL, 0x0000100010101000ULL,
		0x0000101000000000ULL, 0x0000101000001000ULL, 0x0000101000100000ULL, 0x0000101000101000ULL, 0x0000101010000000ULL, 0x0000101010001000ULL, 0x0000101010100000ULL, 0x0000101010101000ULL,
		0x0010000000000000ULL, 0x0010000000001000ULL, 0x0010000000100000ULL, 0x0010000000101000ULL, 0x0010000010000000ULL, 0x0010000010001000ULL, 0x0010000010100000ULL, 0x0010000010101000ULL,
		0x0010001000000000ULL, 0x0010001000001000ULL, 0x0010001000100000ULL, 0x0010001000101000ULL, 0x0010001010000000ULL, 0x0010001010001000ULL, 0x0010001010100000ULL, 0x0010001010101000ULL,
		0x0010100000000000ULL, 0x0010100000001000ULL, 0x0010100000100000ULL, 0x0010100000101000ULL, 0x0010100010000000ULL, 0x0010100010001000ULL, 0x0010100010100000ULL, 0x0010100010101000ULL,
		0x0010101000000000ULL, 0x0010101000001000ULL, 0x0010101000100000ULL, 0x0010101000101000ULL, 0x0010101010000000ULL, 0x0010101010001000ULL, 0x0010101010100000ULL, 0x0010101010101000ULL,
};

/** conversion from an 8-bit line to the F2-F7- line */
unsigned long long F2F7[64] = {
		0x0000000000000000ULL, 0x0000000000002000ULL, 0x0000000000200000ULL, 0x0000000000202000ULL, 0x0000000020000000ULL, 0x0000000020002000ULL, 0x0000000020200000ULL, 0x0000000020202000ULL,
		0x0000002000000000ULL, 0x0000002000002000ULL, 0x0000002000200000ULL, 0x0000002000202000ULL, 0x0000002020000000ULL, 0x0000002020002000ULL, 0x0000002020200000ULL, 0x0000002020202000ULL,
		0x0000200000000000ULL, 0x0000200000002000ULL, 0x0000200000200000ULL, 0x0000200000202000ULL, 0x0000200020000000ULL, 0x0000200020002000ULL, 0x0000200020200000ULL, 0x0000200020202000ULL,
		0x0000202000000000ULL, 0x0000202000002000ULL, 0x0000202000200000ULL, 0x0000202000202000ULL, 0x0000202020000000ULL, 0x0000202020002000ULL, 0x0000202020200000ULL, 0x0000202020202000ULL,
		0x0020000000000000ULL, 0x0020000000002000ULL, 0x0020000000200000ULL, 0x0020000000202000ULL, 0x0020000020000000ULL, 0x0020000020002000ULL, 0x0020000020200000ULL, 0x0020000020202000ULL,
		0x0020002000000000ULL, 0x0020002000002000ULL, 0x0020002000200000ULL, 0x0020002000202000ULL, 0x0020002020000000ULL, 0x0020002020002000ULL, 0x0020002020200000ULL, 0x0020002020202000ULL,
		0x0020200000000000ULL, 0x0020200000002000ULL, 0x0020200000200000ULL, 0x0020200000202000ULL, 0x0020200020000000ULL, 0x0020200020002000ULL, 0x0020200020200000ULL, 0x0020200020202000ULL,
		0x0020202000000000ULL, 0x0020202000002000ULL, 0x0020202000200000ULL, 0x0020202000202000ULL, 0x0020202020000000ULL, 0x0020202020002000ULL, 0x0020202020200000ULL, 0x0020202020202000ULL,
};

/** conversion from an 8-bit line to the G2-G7- line */
unsigned long long G2G7[64] = {
		0x0000000000000000ULL, 0x0000000000004000ULL, 0x0000000000400000ULL, 0x0000000000404000ULL, 0x0000000040000000ULL, 0x0000000040004000ULL, 0x0000000040400000ULL, 0x0000000040404000ULL,
		0x0000004000000000ULL, 0x0000004000004000ULL, 0x0000004000400000ULL, 0x0000004000404000ULL, 0x0000004040000000ULL, 0x0000004040004000ULL, 0x0000004040400000ULL, 0x0000004040404000ULL,
		0x0000400000000000ULL, 0x0000400000004000ULL, 0x0000400000400000ULL, 0x0000400000404000ULL, 0x0000400040000000ULL, 0x0000400040004000ULL, 0x0000400040400000ULL, 0x0000400040404000ULL,
		0x0000404000000000ULL, 0x0000404000004000ULL, 0x0000404000400000ULL, 0x0000404000404000ULL, 0x0000404040000000ULL, 0x0000404040004000ULL, 0x0000404040400000ULL, 0x0000404040404000ULL,
		0x0040000000000000ULL, 0x0040000000004000ULL, 0x0040000000400000ULL, 0x0040000000404000ULL, 0x0040000040000000ULL, 0x0040000040004000ULL, 0x0040000040400000ULL, 0x0040000040404000ULL,
		0x0040004000000000ULL, 0x0040004000004000ULL, 0x0040004000400000ULL, 0x0040004000404000ULL, 0x0040004040000000ULL, 0x0040004040004000ULL, 0x0040004040400000ULL, 0x0040004040404000ULL,
		0x0040400000000000ULL, 0x0040400000004000ULL, 0x0040400000400000ULL, 0x0040400000404000ULL, 0x0040400040000000ULL, 0x0040400040004000ULL, 0x0040400040400000ULL, 0x0040400040404000ULL,
		0x0040404000000000ULL, 0x0040404000004000ULL, 0x0040404000400000ULL, 0x0040404000404000ULL, 0x0040404040000000ULL, 0x0040404040004000ULL, 0x0040404040400000ULL, 0x0040404040404000ULL,
};

/** conversion from an 8-bit line to the H2-H7- line */
unsigned long long H2H7[64] = {
		0x0000000000000000ULL, 0x0000000000008000ULL, 0x0000000000800000ULL, 0x0000000000808000ULL, 0x0000000080000000ULL, 0x0000000080008000ULL, 0x0000000080800000ULL, 0x0000000080808000ULL,
		0x0000008000000000ULL, 0x0000008000008000ULL, 0x0000008000800000ULL, 0x0000008000808000ULL, 0x0000008080000000ULL, 0x0000008080008000ULL, 0x0000008080800000ULL, 0x0000008080808000ULL,
		0x0000800000000000ULL, 0x0000800000008000ULL, 0x0000800000800000ULL, 0x0000800000808000ULL, 0x0000800080000000ULL, 0x0000800080008000ULL, 0x0000800080800000ULL, 0x0000800080808000ULL,
		0x0000808000000000ULL, 0x0000808000008000ULL, 0x0000808000800000ULL, 0x0000808000808000ULL, 0x0000808080000000ULL, 0x0000808080008000ULL, 0x0000808080800000ULL, 0x0000808080808000ULL,
		0x0080000000000000ULL, 0x0080000000008000ULL, 0x0080000000800000ULL, 0x0080000000808000ULL, 0x0080000080000000ULL, 0x0080000080008000ULL, 0x0080000080800000ULL, 0x0080000080808000ULL,
		0x0080008000000000ULL, 0x0080008000008000ULL, 0x0080008000800000ULL, 0x0080008000808000ULL, 0x0080008080000000ULL, 0x0080008080008000ULL, 0x0080008080800000ULL, 0x0080008080808000ULL,
		0x0080800000000000ULL, 0x0080800000008000ULL, 0x0080800000800000ULL, 0x0080800000808000ULL, 0x0080800080000000ULL, 0x0080800080008000ULL, 0x0080800080800000ULL, 0x0080800080808000ULL,
		0x0080808000000000ULL, 0x0080808000008000ULL, 0x0080808000800000ULL, 0x0080808000808000ULL, 0x0080808080000000ULL, 0x0080808080008000ULL, 0x0080808080800000ULL, 0x0080808080808000ULL,
};

/** conversion from an 8-bit line to the E2-B5- line */
unsigned long long E2B5[16] = {
		0x0000000000000000ULL, 0x0000000200000000ULL, 0x0000000004000000ULL, 0x0000000204000000ULL, 0x0000000000080000ULL, 0x0000000200080000ULL, 0x0000000004080000ULL, 0x0000000204080000ULL,
		0x0000000000001000ULL, 0x0000000200001000ULL, 0x0000000004001000ULL, 0x0000000204001000ULL, 0x0000000000081000ULL, 0x0000000200081000ULL, 0x0000000004081000ULL, 0x0000000204081000ULL,
};

/** conversion from an 8-bit line to the F2-B6- line */
unsigned long long F2B6[32] = {
		0x0000000000000000ULL, 0x0000020000000000ULL, 0x0000000400000000ULL, 0x0000020400000000ULL, 0x0000000008000000ULL, 0x0000020008000000ULL, 0x0000000408000000ULL, 0x0000020408000000ULL,
		0x0000000000100000ULL, 0x0000020000100000ULL, 0x0000000400100000ULL, 0x0000020400100000ULL, 0x0000000008100000ULL, 0x0000020008100000ULL, 0x0000000408100000ULL, 0x0000020408100000ULL,
		0x0000000000002000ULL, 0x0000020000002000ULL, 0x0000000400002000ULL, 0x0000020400002000ULL, 0x0000000008002000ULL, 0x0000020008002000ULL, 0x0000000408002000ULL, 0x0000020408002000ULL,
		0x0000000000102000ULL, 0x0000020000102000ULL, 0x0000000400102000ULL, 0x0000020400102000ULL, 0x0000000008102000ULL, 0x0000020008102000ULL, 0x0000000408102000ULL, 0x0000020408102000ULL,
};

/** conversion from an 8-bit line to the G2-B7- line */
unsigned long long G2B7[64] = {
		0x0000000000000000ULL, 0x0002000000000000ULL, 0x0000040000000000ULL, 0x0002040000000000ULL, 0x0000000800000000ULL, 0x0002000800000000ULL, 0x0000040800000000ULL, 0x0002040800000000ULL,
		0x0000000010000000ULL, 0x0002000010000000ULL, 0x0000040010000000ULL, 0x0002040010000000ULL, 0x0000000810000000ULL, 0x0002000810000000ULL, 0x0000040810000000ULL, 0x0002040810000000ULL,
		0x0000000000200000ULL, 0x0002000000200000ULL, 0x0000040000200000ULL, 0x0002040000200000ULL, 0x0000000800200000ULL, 0x0002000800200000ULL, 0x0000040800200000ULL, 0x0002040800200000ULL,
		0x0000000010200000ULL, 0x0002000010200000ULL, 0x0000040010200000ULL, 0x0002040010200000ULL, 0x0000000810200000ULL, 0x0002000810200000ULL, 0x0000040810200000ULL, 0x0002040810200000ULL,
		0x0000000000004000ULL, 0x0002000000004000ULL, 0x0000040000004000ULL, 0x0002040000004000ULL, 0x0000000800004000ULL, 0x0002000800004000ULL, 0x0000040800004000ULL, 0x0002040800004000ULL,
		0x0000000010004000ULL, 0x0002000010004000ULL, 0x0000040010004000ULL, 0x0002040010004000ULL, 0x0000000810004000ULL, 0x0002000810004000ULL, 0x0000040810004000ULL, 0x0002040810004000ULL,
		0x0000000000204000ULL, 0x0002000000204000ULL, 0x0000040000204000ULL, 0x0002040000204000ULL, 0x0000000800204000ULL, 0x0002000800204000ULL, 0x0000040800204000ULL, 0x0002040800204000ULL,
		0x0000000010204000ULL, 0x0002000010204000ULL, 0x0000040010204000ULL, 0x0002040010204000ULL, 0x0000000810204000ULL, 0x0002000810204000ULL, 0x0000040810204000ULL, 0x0002040810204000ULL,
};

/** conversion from an 8-bit line to the G3-C7- line */
unsigned long long G3C7[32] = {
		0x0000000000000000ULL, 0x0004000000000000ULL, 0x0000080000000000ULL, 0x0004080000000000ULL, 0x0000001000000000ULL, 0x0004001000000000ULL, 0x0000081000000000ULL, 0x0004081000000000ULL,
		0x0000000020000000ULL, 0x0004000020000000ULL, 0x0000080020000000ULL, 0x0004080020000000ULL, 0x0000001020000000ULL, 0x0004001020000000ULL, 0x0000081020000000ULL, 0x0004081020000000ULL,
		0x0000000000400000ULL, 0x0004000000400000ULL, 0x0000080000400000ULL, 0x0004080000400000ULL, 0x0000001000400000ULL, 0x0004001000400000ULL, 0x0000081000400000ULL, 0x0004081000400000ULL,
		0x0000000020400000ULL, 0x0004000020400000ULL, 0x0000080020400000ULL, 0x0004080020400000ULL, 0x0000001020400000ULL, 0x0004001020400000ULL, 0x0000081020400000ULL, 0x0004081020400000ULL,
};

/** conversion from an 8-bit line to the G4-D7- line */
unsigned long long G4D7[16] = {
		0x0000000000000000ULL, 0x0008000000000000ULL, 0x0000100000000000ULL, 0x0008100000000000ULL, 0x0000002000000000ULL, 0x0008002000000000ULL, 0x0000102000000000ULL, 0x0008102000000000ULL,
		0x0000000040000000ULL, 0x0008000040000000ULL, 0x0000100040000000ULL, 0x0008100040000000ULL, 0x0000002040000000ULL, 0x0008002040000000ULL, 0x0000102040000000ULL, 0x0008102040000000ULL,
};

/** conversion from an 8-bit line to the B4-E7- line */
unsigned long long B4E7[16] = {
		0x0000000000000000ULL, 0x0000000002000000ULL, 0x0000000400000000ULL, 0x0000000402000000ULL, 0x0000080000000000ULL, 0x0000080002000000ULL, 0x0000080400000000ULL, 0x0000080402000000ULL,
		0x0010000000000000ULL, 0x0010000002000000ULL, 0x0010000400000000ULL, 0x0010000402000000ULL, 0x0010080000000000ULL, 0x0010080002000000ULL, 0x0010080400000000ULL, 0x0010080402000000ULL,
};

/** conversion from an 8-bit line to the B3-F7- line */
unsigned long long B3F7[32] = {
		0x0000000000000000ULL, 0x0000000000020000ULL, 0x0000000004000000ULL, 0x0000000004020000ULL, 0x0000000800000000ULL, 0x0000000800020000ULL, 0x0000000804000000ULL, 0x0000000804020000ULL,
		0x0000100000000000ULL, 0x0000100000020000ULL, 0x0000100004000000ULL, 0x0000100004020000ULL, 0x0000100800000000ULL, 0x0000100800020000ULL, 0x0000100804000000ULL, 0x0000100804020000ULL,
		0x0020000000000000ULL, 0x0020000000020000ULL, 0x0020000004000000ULL, 0x0020000004020000ULL, 0x0020000800000000ULL, 0x0020000800020000ULL, 0x0020000804000000ULL, 0x0020000804020000ULL,
		0x0020100000000000ULL, 0x0020100000020000ULL, 0x0020100004000000ULL, 0x0020100004020000ULL, 0x0020100800000000ULL, 0x0020100800020000ULL, 0x0020100804000000ULL, 0x0020100804020000ULL,
};

/** conversion from an 8-bit line to the B2-G7- line */
unsigned long long B2G7[64] = {
		0x0000000000000000ULL, 0x0000000000000200ULL, 0x0000000000040000ULL, 0x0000000000040200ULL, 0x0000000008000000ULL, 0x0000000008000200ULL, 0x0000000008040000ULL, 0x0000000008040200ULL,
		0x0000001000000000ULL, 0x0000001000000200ULL, 0x0000001000040000ULL, 0x0000001000040200ULL, 0x0000001008000000ULL, 0x0000001008000200ULL, 0x0000001008040000ULL, 0x0000001008040200ULL,
		0x0000200000000000ULL, 0x0000200000000200ULL, 0x0000200000040000ULL, 0x0000200000040200ULL, 0x0000200008000000ULL, 0x0000200008000200ULL, 0x0000200008040000ULL, 0x0000200008040200ULL,
		0x0000201000000000ULL, 0x0000201000000200ULL, 0x0000201000040000ULL, 0x0000201000040200ULL, 0x0000201008000000ULL, 0x0000201008000200ULL, 0x0000201008040000ULL, 0x0000201008040200ULL,
		0x0040000000000000ULL, 0x0040000000000200ULL, 0x0040000000040000ULL, 0x0040000000040200ULL, 0x0040000008000000ULL, 0x0040000008000200ULL, 0x0040000008040000ULL, 0x0040000008040200ULL,
		0x0040001000000000ULL, 0x0040001000000200ULL, 0x0040001000040000ULL, 0x0040001000040200ULL, 0x0040001008000000ULL, 0x0040001008000200ULL, 0x0040001008040000ULL, 0x0040001008040200ULL,
		0x0040200000000000ULL, 0x0040200000000200ULL, 0x0040200000040000ULL, 0x0040200000040200ULL, 0x0040200008000000ULL, 0x0040200008000200ULL, 0x0040200008040000ULL, 0x0040200008040200ULL,
		0x0040201000000000ULL, 0x0040201000000200ULL, 0x0040201000040000ULL, 0x0040201000040200ULL, 0x0040201008000000ULL, 0x0040201008000200ULL, 0x0040201008040000ULL, 0x0040201008040200ULL,
};

/** conversion from an 8-bit line to the C2-G6- line */
unsigned long long C2G6[32] = {
		0x0000000000000000ULL, 0x0000000000000400ULL, 0x0000000000080000ULL, 0x0000000000080400ULL, 0x0000000010000000ULL, 0x0000000010000400ULL, 0x0000000010080000ULL, 0x0000000010080400ULL,
		0x0000002000000000ULL, 0x0000002000000400ULL, 0x0000002000080000ULL, 0x0000002000080400ULL, 0x0000002010000000ULL, 0x0000002010000400ULL, 0x0000002010080000ULL, 0x0000002010080400ULL,
		0x0000400000000000ULL, 0x0000400000000400ULL, 0x0000400000080000ULL, 0x0000400000080400ULL, 0x0000400010000000ULL, 0x0000400010000400ULL, 0x0000400010080000ULL, 0x0000400010080400ULL,
		0x0000402000000000ULL, 0x0000402000000400ULL, 0x0000402000080000ULL, 0x0000402000080400ULL, 0x0000402010000000ULL, 0x0000402010000400ULL, 0x0000402010080000ULL, 0x0000402010080400ULL,
};

/** conversion from an 8-bit line to the D2-G5- line */
unsigned long long D2G5[16] = {
		0x0000000000000000ULL, 0x0000000000000800ULL, 0x0000000000100000ULL, 0x0000000000100800ULL, 0x0000000020000000ULL, 0x0000000020000800ULL, 0x0000000020100000ULL, 0x0000000020100800ULL,
		0x0000004000000000ULL, 0x0000004000000800ULL, 0x0000004000100000ULL, 0x0000004000100800ULL, 0x0000004020000000ULL, 0x0000004020000800ULL, 0x0000004020100000ULL, 0x0000004020100800ULL,
};


/** conversion from an 8-bit line to the B2-C1-G5 line */
unsigned long long B2C1G5[64] = {
		0x0000000000000000ULL, 0x0000000000000200ULL, 0x0000000000000004ULL, 0x0000000000000204ULL, 0x0000000000000800ULL, 0x0000000000000a00ULL, 0x0000000000000804ULL, 0x0000000000000a04ULL,
		0x0000000000100000ULL, 0x0000000000100200ULL, 0x0000000000100004ULL, 0x0000000000100204ULL, 0x0000000000100800ULL, 0x0000000000100a00ULL, 0x0000000000100804ULL, 0x0000000000100a04ULL,
		0x0000000020000000ULL, 0x0000000020000200ULL, 0x0000000020000004ULL, 0x0000000020000204ULL, 0x0000000020000800ULL, 0x0000000020000a00ULL, 0x0000000020000804ULL, 0x0000000020000a04ULL,
		0x0000000020100000ULL, 0x0000000020100200ULL, 0x0000000020100004ULL, 0x0000000020100204ULL, 0x0000000020100800ULL, 0x0000000020100a00ULL, 0x0000000020100804ULL, 0x0000000020100a04ULL,
		0x0000004000000000ULL, 0x0000004000000200ULL, 0x0000004000000004ULL, 0x0000004000000204ULL, 0x0000004000000800ULL, 0x0000004000000a00ULL, 0x0000004000000804ULL, 0x0000004000000a04ULL,
		0x0000004000100000ULL, 0x0000004000100200ULL, 0x0000004000100004ULL, 0x0000004000100204ULL, 0x0000004000100800ULL, 0x0000004000100a00ULL, 0x0000004000100804ULL, 0x0000004000100a04ULL,
		0x0000004020000000ULL, 0x0000004020000200ULL, 0x0000004020000004ULL, 0x0000004020000204ULL, 0x0000004020000800ULL, 0x0000004020000a00ULL, 0x0000004020000804ULL, 0x0000004020000a04ULL,
		0x0000004020100000ULL, 0x0000004020100200ULL, 0x0000004020100004ULL, 0x0000004020100204ULL, 0x0000004020100800ULL, 0x0000004020100a00ULL, 0x0000004020100804ULL, 0x0000004020100a04ULL,
};

/** conversion from an 8-bit line to the B3-D1-G4 line */
unsigned long long B3D1G4[64] = {
		0x0000000000000000ULL, 0x0000000000020000ULL, 0x0000000000000400ULL, 0x0000000000020400ULL, 0x0000000000000008ULL, 0x0000000000020008ULL, 0x0000000000000408ULL, 0x0000000000020408ULL,
		0x0000000000001000ULL, 0x0000000000021000ULL, 0x0000000000001400ULL, 0x0000000000021400ULL, 0x0000000000001008ULL, 0x0000000000021008ULL, 0x0000000000001408ULL, 0x0000000000021408ULL,
		0x0000000000200000ULL, 0x0000000000220000ULL, 0x0000000000200400ULL, 0x0000000000220400ULL, 0x0000000000200008ULL, 0x0000000000220008ULL, 0x0000000000200408ULL, 0x0000000000220408ULL,
		0x0000000000201000ULL, 0x0000000000221000ULL, 0x0000000000201400ULL, 0x0000000000221400ULL, 0x0000000000201008ULL, 0x0000000000221008ULL, 0x0000000000201408ULL, 0x0000000000221408ULL,
		0x0000000040000000ULL, 0x0000000040020000ULL, 0x0000000040000400ULL, 0x0000000040020400ULL, 0x0000000040000008ULL, 0x0000000040020008ULL, 0x0000000040000408ULL, 0x0000000040020408ULL,
		0x0000000040001000ULL, 0x0000000040021000ULL, 0x0000000040001400ULL, 0x0000000040021400ULL, 0x0000000040001008ULL, 0x0000000040021008ULL, 0x0000000040001408ULL, 0x0000000040021408ULL,
		0x0000000040200000ULL, 0x0000000040220000ULL, 0x0000000040200400ULL, 0x0000000040220400ULL, 0x0000000040200008ULL, 0x0000000040220008ULL, 0x0000000040200408ULL, 0x0000000040220408ULL,
		0x0000000040201000ULL, 0x0000000040221000ULL, 0x0000000040201400ULL, 0x0000000040221400ULL, 0x0000000040201008ULL, 0x0000000040221008ULL, 0x0000000040201408ULL, 0x0000000040221408ULL,
};

/** conversion from an 8-bit line to the B4-E1-G3 line */
unsigned long long B4E1G3[64] = {
		0x0000000000000000ULL, 0x0000000002000000ULL, 0x0000000000040000ULL, 0x0000000002040000ULL, 0x0000000000000800ULL, 0x0000000002000800ULL, 0x0000000000040800ULL, 0x0000000002040800ULL,
		0x0000000000000010ULL, 0x0000000002000010ULL, 0x0000000000040010ULL, 0x0000000002040010ULL, 0x0000000000000810ULL, 0x0000000002000810ULL, 0x0000000000040810ULL, 0x0000000002040810ULL,
		0x0000000000002000ULL, 0x0000000002002000ULL, 0x0000000000042000ULL, 0x0000000002042000ULL, 0x0000000000002800ULL, 0x0000000002002800ULL, 0x0000000000042800ULL, 0x0000000002042800ULL,
		0x0000000000002010ULL, 0x0000000002002010ULL, 0x0000000000042010ULL, 0x0000000002042010ULL, 0x0000000000002810ULL, 0x0000000002002810ULL, 0x0000000000042810ULL, 0x0000000002042810ULL,
		0x0000000000400000ULL, 0x0000000002400000ULL, 0x0000000000440000ULL, 0x0000000002440000ULL, 0x0000000000400800ULL, 0x0000000002400800ULL, 0x0000000000440800ULL, 0x0000000002440800ULL,
		0x0000000000400010ULL, 0x0000000002400010ULL, 0x0000000000440010ULL, 0x0000000002440010ULL, 0x0000000000400810ULL, 0x0000000002400810ULL, 0x0000000000440810ULL, 0x0000000002440810ULL,
		0x0000000000402000ULL, 0x0000000002402000ULL, 0x0000000000442000ULL, 0x0000000002442000ULL, 0x0000000000402800ULL, 0x0000000002402800ULL, 0x0000000000442800ULL, 0x0000000002442800ULL,
		0x0000000000402010ULL, 0x0000000002402010ULL, 0x0000000000442010ULL, 0x0000000002442010ULL, 0x0000000000402810ULL, 0x0000000002402810ULL, 0x0000000000442810ULL, 0x0000000002442810ULL,
};

/** conversion from an 8-bit line to the B5-F1-G2 line */
unsigned long long B5F1G2[64] = {
		0x0000000000000000ULL, 0x0000000200000000ULL, 0x0000000004000000ULL, 0x0000000204000000ULL, 0x0000000000080000ULL, 0x0000000200080000ULL, 0x0000000004080000ULL, 0x0000000204080000ULL,
		0x0000000000001000ULL, 0x0000000200001000ULL, 0x0000000004001000ULL, 0x0000000204001000ULL, 0x0000000000081000ULL, 0x0000000200081000ULL, 0x0000000004081000ULL, 0x0000000204081000ULL,
		0x0000000000000020ULL, 0x0000000200000020ULL, 0x0000000004000020ULL, 0x0000000204000020ULL, 0x0000000000080020ULL, 0x0000000200080020ULL, 0x0000000004080020ULL, 0x0000000204080020ULL,
		0x0000000000001020ULL, 0x0000000200001020ULL, 0x0000000004001020ULL, 0x0000000204001020ULL, 0x0000000000081020ULL, 0x0000000200081020ULL, 0x0000000004081020ULL, 0x0000000204081020ULL,
		0x0000000000004000ULL, 0x0000000200004000ULL, 0x0000000004004000ULL, 0x0000000204004000ULL, 0x0000000000084000ULL, 0x0000000200084000ULL, 0x0000000004084000ULL, 0x0000000204084000ULL,
		0x0000000000005000ULL, 0x0000000200005000ULL, 0x0000000004005000ULL, 0x0000000204005000ULL, 0x0000000000085000ULL, 0x0000000200085000ULL, 0x0000000004085000ULL, 0x0000000204085000ULL,
		0x0000000000004020ULL, 0x0000000200004020ULL, 0x0000000004004020ULL, 0x0000000204004020ULL, 0x0000000000084020ULL, 0x0000000200084020ULL, 0x0000000004084020ULL, 0x0000000204084020ULL,
		0x0000000000005020ULL, 0x0000000200005020ULL, 0x0000000004005020ULL, 0x0000000204005020ULL, 0x0000000000085020ULL, 0x0000000200085020ULL, 0x0000000004085020ULL, 0x0000000204085020ULL,
};

/** conversion from an 8-bit line to the B3-C2-G6 line */
unsigned long long B3C2G6[64] = {
		0x0000000000000000ULL, 0x0000000000020000ULL, 0x0000000000000400ULL, 0x0000000000020400ULL, 0x0000000000080000ULL, 0x00000000000a0000ULL, 0x0000000000080400ULL, 0x00000000000a0400ULL,
		0x0000000010000000ULL, 0x0000000010020000ULL, 0x0000000010000400ULL, 0x0000000010020400ULL, 0x0000000010080000ULL, 0x00000000100a0000ULL, 0x0000000010080400ULL, 0x00000000100a0400ULL,
		0x0000002000000000ULL, 0x0000002000020000ULL, 0x0000002000000400ULL, 0x0000002000020400ULL, 0x0000002000080000ULL, 0x00000020000a0000ULL, 0x0000002000080400ULL, 0x00000020000a0400ULL,
		0x0000002010000000ULL, 0x0000002010020000ULL, 0x0000002010000400ULL, 0x0000002010020400ULL, 0x0000002010080000ULL, 0x00000020100a0000ULL, 0x0000002010080400ULL, 0x00000020100a0400ULL,
		0x0000400000000000ULL, 0x0000400000020000ULL, 0x0000400000000400ULL, 0x0000400000020400ULL, 0x0000400000080000ULL, 0x00004000000a0000ULL, 0x0000400000080400ULL, 0x00004000000a0400ULL,
		0x0000400010000000ULL, 0x0000400010020000ULL, 0x0000400010000400ULL, 0x0000400010020400ULL, 0x0000400010080000ULL, 0x00004000100a0000ULL, 0x0000400010080400ULL, 0x00004000100a0400ULL,
		0x0000402000000000ULL, 0x0000402000020000ULL, 0x0000402000000400ULL, 0x0000402000020400ULL, 0x0000402000080000ULL, 0x00004020000a0000ULL, 0x0000402000080400ULL, 0x00004020000a0400ULL,
		0x0000402010000000ULL, 0x0000402010020000ULL, 0x0000402010000400ULL, 0x0000402010020400ULL, 0x0000402010080000ULL, 0x00004020100a0000ULL, 0x0000402010080400ULL, 0x00004020100a0400ULL,
};

/** conversion from an 8-bit line to the B4-D2-G5 line */
unsigned long long B4D2G5[64] = {
		0x0000000000000000ULL, 0x0000000002000000ULL, 0x0000000000040000ULL, 0x0000000002040000ULL, 0x0000000000000800ULL, 0x0000000002000800ULL, 0x0000000000040800ULL, 0x0000000002040800ULL,
		0x0000000000100000ULL, 0x0000000002100000ULL, 0x0000000000140000ULL, 0x0000000002140000ULL, 0x0000000000100800ULL, 0x0000000002100800ULL, 0x0000000000140800ULL, 0x0000000002140800ULL,
		0x0000000020000000ULL, 0x0000000022000000ULL, 0x0000000020040000ULL, 0x0000000022040000ULL, 0x0000000020000800ULL, 0x0000000022000800ULL, 0x0000000020040800ULL, 0x0000000022040800ULL,
		0x0000000020100000ULL, 0x0000000022100000ULL, 0x0000000020140000ULL, 0x0000000022140000ULL, 0x0000000020100800ULL, 0x0000000022100800ULL, 0x0000000020140800ULL, 0x0000000022140800ULL,
		0x0000004000000000ULL, 0x0000004002000000ULL, 0x0000004000040000ULL, 0x0000004002040000ULL, 0x0000004000000800ULL, 0x0000004002000800ULL, 0x0000004000040800ULL, 0x0000004002040800ULL,
		0x0000004000100000ULL, 0x0000004002100000ULL, 0x0000004000140000ULL, 0x0000004002140000ULL, 0x0000004000100800ULL, 0x0000004002100800ULL, 0x0000004000140800ULL, 0x0000004002140800ULL,
		0x0000004020000000ULL, 0x0000004022000000ULL, 0x0000004020040000ULL, 0x0000004022040000ULL, 0x0000004020000800ULL, 0x0000004022000800ULL, 0x0000004020040800ULL, 0x0000004022040800ULL,
		0x0000004020100000ULL, 0x0000004022100000ULL, 0x0000004020140000ULL, 0x0000004022140000ULL, 0x0000004020100800ULL, 0x0000004022100800ULL, 0x0000004020140800ULL, 0x0000004022140800ULL,
};

/** conversion from an 8-bit line to the B5-E2-G4 line */
unsigned long long B5E2G4[64] = {
		0x0000000000000000ULL, 0x0000000200000000ULL, 0x0000000004000000ULL, 0x0000000204000000ULL, 0x0000000000080000ULL, 0x0000000200080000ULL, 0x0000000004080000ULL, 0x0000000204080000ULL,
		0x0000000000001000ULL, 0x0000000200001000ULL, 0x0000000004001000ULL, 0x0000000204001000ULL, 0x0000000000081000ULL, 0x0000000200081000ULL, 0x0000000004081000ULL, 0x0000000204081000ULL,
		0x0000000000200000ULL, 0x0000000200200000ULL, 0x0000000004200000ULL, 0x0000000204200000ULL, 0x0000000000280000ULL, 0x0000000200280000ULL, 0x0000000004280000ULL, 0x0000000204280000ULL,
		0x0000000000201000ULL, 0x0000000200201000ULL, 0x0000000004201000ULL, 0x0000000204201000ULL, 0x0000000000281000ULL, 0x0000000200281000ULL, 0x0000000004281000ULL, 0x0000000204281000ULL,
		0x0000000040000000ULL, 0x0000000240000000ULL, 0x0000000044000000ULL, 0x0000000244000000ULL, 0x0000000040080000ULL, 0x0000000240080000ULL, 0x0000000044080000ULL, 0x0000000244080000ULL,
		0x0000000040001000ULL, 0x0000000240001000ULL, 0x0000000044001000ULL, 0x0000000244001000ULL, 0x0000000040081000ULL, 0x0000000240081000ULL, 0x0000000044081000ULL, 0x0000000244081000ULL,
		0x0000000040200000ULL, 0x0000000240200000ULL, 0x0000000044200000ULL, 0x0000000244200000ULL, 0x0000000040280000ULL, 0x0000000240280000ULL, 0x0000000044280000ULL, 0x0000000244280000ULL,
		0x0000000040201000ULL, 0x0000000240201000ULL, 0x0000000044201000ULL, 0x0000000244201000ULL, 0x0000000040281000ULL, 0x0000000240281000ULL, 0x0000000044281000ULL, 0x0000000244281000ULL,
};

/** conversion from an 8-bit line to the B6-F2-G3 line */
unsigned long long B6F2G3[64] = {
		0x0000000000000000ULL, 0x0000020000000000ULL, 0x0000000400000000ULL, 0x0000020400000000ULL, 0x0000000008000000ULL, 0x0000020008000000ULL, 0x0000000408000000ULL, 0x0000020408000000ULL,
		0x0000000000100000ULL, 0x0000020000100000ULL, 0x0000000400100000ULL, 0x0000020400100000ULL, 0x0000000008100000ULL, 0x0000020008100000ULL, 0x0000000408100000ULL, 0x0000020408100000ULL,
		0x0000000000002000ULL, 0x0000020000002000ULL, 0x0000000400002000ULL, 0x0000020400002000ULL, 0x0000000008002000ULL, 0x0000020008002000ULL, 0x0000000408002000ULL, 0x0000020408002000ULL,
		0x0000000000102000ULL, 0x0000020000102000ULL, 0x0000000400102000ULL, 0x0000020400102000ULL, 0x0000000008102000ULL, 0x0000020008102000ULL, 0x0000000408102000ULL, 0x0000020408102000ULL,
		0x0000000000400000ULL, 0x0000020000400000ULL, 0x0000000400400000ULL, 0x0000020400400000ULL, 0x0000000008400000ULL, 0x0000020008400000ULL, 0x0000000408400000ULL, 0x0000020408400000ULL,
		0x0000000000500000ULL, 0x0000020000500000ULL, 0x0000000400500000ULL, 0x0000020400500000ULL, 0x0000000008500000ULL, 0x0000020008500000ULL, 0x0000000408500000ULL, 0x0000020408500000ULL,
		0x0000000000402000ULL, 0x0000020000402000ULL, 0x0000000400402000ULL, 0x0000020400402000ULL, 0x0000000008402000ULL, 0x0000020008402000ULL, 0x0000000408402000ULL, 0x0000020408402000ULL,
		0x0000000000502000ULL, 0x0000020000502000ULL, 0x0000000400502000ULL, 0x0000020400502000ULL, 0x0000000008502000ULL, 0x0000020008502000ULL, 0x0000000408502000ULL, 0x0000020408502000ULL,
};

/** conversion from an 8-bit line to the B6-C7-G3 line */
unsigned long long B6C7G3[64] = {
		0x0000000000000000ULL, 0x0000020000000000ULL, 0x0004000000000000ULL, 0x0004020000000000ULL, 0x0000080000000000ULL, 0x00000a0000000000ULL, 0x0004080000000000ULL, 0x00040a0000000000ULL,
		0x0000001000000000ULL, 0x0000021000000000ULL, 0x0004001000000000ULL, 0x0004021000000000ULL, 0x0000081000000000ULL, 0x00000a1000000000ULL, 0x0004081000000000ULL, 0x00040a1000000000ULL,
		0x0000000020000000ULL, 0x0000020020000000ULL, 0x0004000020000000ULL, 0x0004020020000000ULL, 0x0000080020000000ULL, 0x00000a0020000000ULL, 0x0004080020000000ULL, 0x00040a0020000000ULL,
		0x0000001020000000ULL, 0x0000021020000000ULL, 0x0004001020000000ULL, 0x0004021020000000ULL, 0x0000081020000000ULL, 0x00000a1020000000ULL, 0x0004081020000000ULL, 0x00040a1020000000ULL,
		0x0000000000400000ULL, 0x0000020000400000ULL, 0x0004000000400000ULL, 0x0004020000400000ULL, 0x0000080000400000ULL, 0x00000a0000400000ULL, 0x0004080000400000ULL, 0x00040a0000400000ULL,
		0x0000001000400000ULL, 0x0000021000400000ULL, 0x0004001000400000ULL, 0x0004021000400000ULL, 0x0000081000400000ULL, 0x00000a1000400000ULL, 0x0004081000400000ULL, 0x00040a1000400000ULL,
		0x0000000020400000ULL, 0x0000020020400000ULL, 0x0004000020400000ULL, 0x0004020020400000ULL, 0x0000080020400000ULL, 0x00000a0020400000ULL, 0x0004080020400000ULL, 0x00040a0020400000ULL,
		0x0000001020400000ULL, 0x0000021020400000ULL, 0x0004001020400000ULL, 0x0004021020400000ULL, 0x0000081020400000ULL, 0x00000a1020400000ULL, 0x0004081020400000ULL, 0x00040a1020400000ULL,
};

/** conversion from an 8-bit line to the B5-D7-G4 line */
unsigned long long B5D7G4[64] = {
		0x0000000000000000ULL, 0x0000000200000000ULL, 0x0000040000000000ULL, 0x0000040200000000ULL, 0x0008000000000000ULL, 0x0008000200000000ULL, 0x0008040000000000ULL, 0x0008040200000000ULL,
		0x0000100000000000ULL, 0x0000100200000000ULL, 0x0000140000000000ULL, 0x0000140200000000ULL, 0x0008100000000000ULL, 0x0008100200000000ULL, 0x0008140000000000ULL, 0x0008140200000000ULL,
		0x0000002000000000ULL, 0x0000002200000000ULL, 0x0000042000000000ULL, 0x0000042200000000ULL, 0x0008002000000000ULL, 0x0008002200000000ULL, 0x0008042000000000ULL, 0x0008042200000000ULL,
		0x0000102000000000ULL, 0x0000102200000000ULL, 0x0000142000000000ULL, 0x0000142200000000ULL, 0x0008102000000000ULL, 0x0008102200000000ULL, 0x0008142000000000ULL, 0x0008142200000000ULL,
		0x0000000040000000ULL, 0x0000000240000000ULL, 0x0000040040000000ULL, 0x0000040240000000ULL, 0x0008000040000000ULL, 0x0008000240000000ULL, 0x0008040040000000ULL, 0x0008040240000000ULL,
		0x0000100040000000ULL, 0x0000100240000000ULL, 0x0000140040000000ULL, 0x0000140240000000ULL, 0x0008100040000000ULL, 0x0008100240000000ULL, 0x0008140040000000ULL, 0x0008140240000000ULL,
		0x0000002040000000ULL, 0x0000002240000000ULL, 0x0000042040000000ULL, 0x0000042240000000ULL, 0x0008002040000000ULL, 0x0008002240000000ULL, 0x0008042040000000ULL, 0x0008042240000000ULL,
		0x0000102040000000ULL, 0x0000102240000000ULL, 0x0000142040000000ULL, 0x0000142240000000ULL, 0x0008102040000000ULL, 0x0008102240000000ULL, 0x0008142040000000ULL, 0x0008142240000000ULL,
};

/** conversion from an 8-bit line to the B4-E7-G5 line */
unsigned long long B4E7G5[64] = {
		0x0000000000000000ULL, 0x0000000002000000ULL, 0x0000000400000000ULL, 0x0000000402000000ULL, 0x0000080000000000ULL, 0x0000080002000000ULL, 0x0000080400000000ULL, 0x0000080402000000ULL,
		0x0010000000000000ULL, 0x0010000002000000ULL, 0x0010000400000000ULL, 0x0010000402000000ULL, 0x0010080000000000ULL, 0x0010080002000000ULL, 0x0010080400000000ULL, 0x0010080402000000ULL,
		0x0000200000000000ULL, 0x0000200002000000ULL, 0x0000200400000000ULL, 0x0000200402000000ULL, 0x0000280000000000ULL, 0x0000280002000000ULL, 0x0000280400000000ULL, 0x0000280402000000ULL,
		0x0010200000000000ULL, 0x0010200002000000ULL, 0x0010200400000000ULL, 0x0010200402000000ULL, 0x0010280000000000ULL, 0x0010280002000000ULL, 0x0010280400000000ULL, 0x0010280402000000ULL,
		0x0000004000000000ULL, 0x0000004002000000ULL, 0x0000004400000000ULL, 0x0000004402000000ULL, 0x0000084000000000ULL, 0x0000084002000000ULL, 0x0000084400000000ULL, 0x0000084402000000ULL,
		0x0010004000000000ULL, 0x0010004002000000ULL, 0x0010004400000000ULL, 0x0010004402000000ULL, 0x0010084000000000ULL, 0x0010084002000000ULL, 0x0010084400000000ULL, 0x0010084402000000ULL,
		0x0000204000000000ULL, 0x0000204002000000ULL, 0x0000204400000000ULL, 0x0000204402000000ULL, 0x0000284000000000ULL, 0x0000284002000000ULL, 0x0000284400000000ULL, 0x0000284402000000ULL,
		0x0010204000000000ULL, 0x0010204002000000ULL, 0x0010204400000000ULL, 0x0010204402000000ULL, 0x0010284000000000ULL, 0x0010284002000000ULL, 0x0010284400000000ULL, 0x0010284402000000ULL,
};

/** conversion from an 8-bit line to the B3-F7-G6 line */
unsigned long long B3F7G6[64] = {
		0x0000000000000000ULL, 0x0000000000020000ULL, 0x0000000004000000ULL, 0x0000000004020000ULL, 0x0000000800000000ULL, 0x0000000800020000ULL, 0x0000000804000000ULL, 0x0000000804020000ULL,
		0x0000100000000000ULL, 0x0000100000020000ULL, 0x0000100004000000ULL, 0x0000100004020000ULL, 0x0000100800000000ULL, 0x0000100800020000ULL, 0x0000100804000000ULL, 0x0000100804020000ULL,
		0x0020000000000000ULL, 0x0020000000020000ULL, 0x0020000004000000ULL, 0x0020000004020000ULL, 0x0020000800000000ULL, 0x0020000800020000ULL, 0x0020000804000000ULL, 0x0020000804020000ULL,
		0x0020100000000000ULL, 0x0020100000020000ULL, 0x0020100004000000ULL, 0x0020100004020000ULL, 0x0020100800000000ULL, 0x0020100800020000ULL, 0x0020100804000000ULL, 0x0020100804020000ULL,
		0x0000400000000000ULL, 0x0000400000020000ULL, 0x0000400004000000ULL, 0x0000400004020000ULL, 0x0000400800000000ULL, 0x0000400800020000ULL, 0x0000400804000000ULL, 0x0000400804020000ULL,
		0x0000500000000000ULL, 0x0000500000020000ULL, 0x0000500004000000ULL, 0x0000500004020000ULL, 0x0000500800000000ULL, 0x0000500800020000ULL, 0x0000500804000000ULL, 0x0000500804020000ULL,
		0x0020400000000000ULL, 0x0020400000020000ULL, 0x0020400004000000ULL, 0x0020400004020000ULL, 0x0020400800000000ULL, 0x0020400800020000ULL, 0x0020400804000000ULL, 0x0020400804020000ULL,
		0x0020500000000000ULL, 0x0020500000020000ULL, 0x0020500004000000ULL, 0x0020500004020000ULL, 0x0020500800000000ULL, 0x0020500800020000ULL, 0x0020500804000000ULL, 0x0020500804020000ULL,
};

/** conversion from an 8-bit line to the B7-C8-G4 line */
unsigned long long B7C8G4[64] = {
		0x0000000000000000ULL, 0x0002000000000000ULL, 0x0400000000000000ULL, 0x0402000000000000ULL, 0x0008000000000000ULL, 0x000a000000000000ULL, 0x0408000000000000ULL, 0x040a000000000000ULL,
		0x0000100000000000ULL, 0x0002100000000000ULL, 0x0400100000000000ULL, 0x0402100000000000ULL, 0x0008100000000000ULL, 0x000a100000000000ULL, 0x0408100000000000ULL, 0x040a100000000000ULL,
		0x0000002000000000ULL, 0x0002002000000000ULL, 0x0400002000000000ULL, 0x0402002000000000ULL, 0x0008002000000000ULL, 0x000a002000000000ULL, 0x0408002000000000ULL, 0x040a002000000000ULL,
		0x0000102000000000ULL, 0x0002102000000000ULL, 0x0400102000000000ULL, 0x0402102000000000ULL, 0x0008102000000000ULL, 0x000a102000000000ULL, 0x0408102000000000ULL, 0x040a102000000000ULL,
		0x0000000040000000ULL, 0x0002000040000000ULL, 0x0400000040000000ULL, 0x0402000040000000ULL, 0x0008000040000000ULL, 0x000a000040000000ULL, 0x0408000040000000ULL, 0x040a000040000000ULL,
		0x0000100040000000ULL, 0x0002100040000000ULL, 0x0400100040000000ULL, 0x0402100040000000ULL, 0x0008100040000000ULL, 0x000a100040000000ULL, 0x0408100040000000ULL, 0x040a100040000000ULL,
		0x0000002040000000ULL, 0x0002002040000000ULL, 0x0400002040000000ULL, 0x0402002040000000ULL, 0x0008002040000000ULL, 0x000a002040000000ULL, 0x0408002040000000ULL, 0x040a002040000000ULL,
		0x0000102040000000ULL, 0x0002102040000000ULL, 0x0400102040000000ULL, 0x0402102040000000ULL, 0x0008102040000000ULL, 0x000a102040000000ULL, 0x0408102040000000ULL, 0x040a102040000000ULL,
};

/** conversion from an 8-bit line to the B6-D8-G5 line */
unsigned long long B6D8G5[64] = {
		0x0000000000000000ULL, 0x0000020000000000ULL, 0x0004000000000000ULL, 0x0004020000000000ULL, 0x0800000000000000ULL, 0x0800020000000000ULL, 0x0804000000000000ULL, 0x0804020000000000ULL,
		0x0010000000000000ULL, 0x0010020000000000ULL, 0x0014000000000000ULL, 0x0014020000000000ULL, 0x0810000000000000ULL, 0x0810020000000000ULL, 0x0814000000000000ULL, 0x0814020000000000ULL,
		0x0000200000000000ULL, 0x0000220000000000ULL, 0x0004200000000000ULL, 0x0004220000000000ULL, 0x0800200000000000ULL, 0x0800220000000000ULL, 0x0804200000000000ULL, 0x0804220000000000ULL,
		0x0010200000000000ULL, 0x0010220000000000ULL, 0x0014200000000000ULL, 0x0014220000000000ULL, 0x0810200000000000ULL, 0x0810220000000000ULL, 0x0814200000000000ULL, 0x0814220000000000ULL,
		0x0000004000000000ULL, 0x0000024000000000ULL, 0x0004004000000000ULL, 0x0004024000000000ULL, 0x0800004000000000ULL, 0x0800024000000000ULL, 0x0804004000000000ULL, 0x0804024000000000ULL,
		0x0010004000000000ULL, 0x0010024000000000ULL, 0x0014004000000000ULL, 0x0014024000000000ULL, 0x0810004000000000ULL, 0x0810024000000000ULL, 0x0814004000000000ULL, 0x0814024000000000ULL,
		0x0000204000000000ULL, 0x0000224000000000ULL, 0x0004204000000000ULL, 0x0004224000000000ULL, 0x0800204000000000ULL, 0x0800224000000000ULL, 0x0804204000000000ULL, 0x0804224000000000ULL,
		0x0010204000000000ULL, 0x0010224000000000ULL, 0x0014204000000000ULL, 0x0014224000000000ULL, 0x0810204000000000ULL, 0x0810224000000000ULL, 0x0814204000000000ULL, 0x0814224000000000ULL,
};

/** conversion from an 8-bit line to the B5-E8-G6 line */
unsigned long long B5E8G6[64] = {
		0x0000000000000000ULL, 0x0000000200000000ULL, 0x0000040000000000ULL, 0x0000040200000000ULL, 0x0008000000000000ULL, 0x0008000200000000ULL, 0x0008040000000000ULL, 0x0008040200000000ULL,
		0x1000000000000000ULL, 0x1000000200000000ULL, 0x1000040000000000ULL, 0x1000040200000000ULL, 0x1008000000000000ULL, 0x1008000200000000ULL, 0x1008040000000000ULL, 0x1008040200000000ULL,
		0x0020000000000000ULL, 0x0020000200000000ULL, 0x0020040000000000ULL, 0x0020040200000000ULL, 0x0028000000000000ULL, 0x0028000200000000ULL, 0x0028040000000000ULL, 0x0028040200000000ULL,
		0x1020000000000000ULL, 0x1020000200000000ULL, 0x1020040000000000ULL, 0x1020040200000000ULL, 0x1028000000000000ULL, 0x1028000200000000ULL, 0x1028040000000000ULL, 0x1028040200000000ULL,
		0x0000400000000000ULL, 0x0000400200000000ULL, 0x0000440000000000ULL, 0x0000440200000000ULL, 0x0008400000000000ULL, 0x0008400200000000ULL, 0x0008440000000000ULL, 0x0008440200000000ULL,
		0x1000400000000000ULL, 0x1000400200000000ULL, 0x1000440000000000ULL, 0x1000440200000000ULL, 0x1008400000000000ULL, 0x1008400200000000ULL, 0x1008440000000000ULL, 0x1008440200000000ULL,
		0x0020400000000000ULL, 0x0020400200000000ULL, 0x0020440000000000ULL, 0x0020440200000000ULL, 0x0028400000000000ULL, 0x0028400200000000ULL, 0x0028440000000000ULL, 0x0028440200000000ULL,
		0x1020400000000000ULL, 0x1020400200000000ULL, 0x1020440000000000ULL, 0x1020440200000000ULL, 0x1028400000000000ULL, 0x1028400200000000ULL, 0x1028440000000000ULL, 0x1028440200000000ULL,
};

/** conversion from an 8-bit line to the B4-F8-G7 line */
unsigned long long B4F8G7[64] = {
		0x0000000000000000ULL, 0x0000000002000000ULL, 0x0000000400000000ULL, 0x0000000402000000ULL, 0x0000080000000000ULL, 0x0000080002000000ULL, 0x0000080400000000ULL, 0x0000080402000000ULL,
		0x0010000000000000ULL, 0x0010000002000000ULL, 0x0010000400000000ULL, 0x0010000402000000ULL, 0x0010080000000000ULL, 0x0010080002000000ULL, 0x0010080400000000ULL, 0x0010080402000000ULL,
		0x2000000000000000ULL, 0x2000000002000000ULL, 0x2000000400000000ULL, 0x2000000402000000ULL, 0x2000080000000000ULL, 0x2000080002000000ULL, 0x2000080400000000ULL, 0x2000080402000000ULL,
		0x2010000000000000ULL, 0x2010000002000000ULL, 0x2010000400000000ULL, 0x2010000402000000ULL, 0x2010080000000000ULL, 0x2010080002000000ULL, 0x2010080400000000ULL, 0x2010080402000000ULL,
		0x0040000000000000ULL, 0x0040000002000000ULL, 0x0040000400000000ULL, 0x0040000402000000ULL, 0x0040080000000000ULL, 0x0040080002000000ULL, 0x0040080400000000ULL, 0x0040080402000000ULL,
		0x0050000000000000ULL, 0x0050000002000000ULL, 0x0050000400000000ULL, 0x0050000402000000ULL, 0x0050080000000000ULL, 0x0050080002000000ULL, 0x0050080400000000ULL, 0x0050080402000000ULL,
		0x2040000000000000ULL, 0x2040000002000000ULL, 0x2040000400000000ULL, 0x2040000402000000ULL, 0x2040080000000000ULL, 0x2040080002000000ULL, 0x2040080400000000ULL, 0x2040080402000000ULL,
		0x2050000000000000ULL, 0x2050000002000000ULL, 0x2050000400000000ULL, 0x2050000402000000ULL, 0x2050080000000000ULL, 0x2050080002000000ULL, 0x2050080400000000ULL, 0x2050080402000000ULL,
};

/** conversion from an 8-bit line to the G2-H3-D7 line */
unsigned long long G2H3D7[64] = {
		0x0000000000000000ULL, 0x0000000000004000ULL, 0x0000000000800000ULL, 0x0000000000804000ULL, 0x0000000040000000ULL, 0x0000000040004000ULL, 0x0000000040800000ULL, 0x0000000040804000ULL,
		0x0000002000000000ULL, 0x0000002000004000ULL, 0x0000002000800000ULL, 0x0000002000804000ULL, 0x0000002040000000ULL, 0x0000002040004000ULL, 0x0000002040800000ULL, 0x0000002040804000ULL,
		0x0000100000000000ULL, 0x0000100000004000ULL, 0x0000100000800000ULL, 0x0000100000804000ULL, 0x0000100040000000ULL, 0x0000100040004000ULL, 0x0000100040800000ULL, 0x0000100040804000ULL,
		0x0000102000000000ULL, 0x0000102000004000ULL, 0x0000102000800000ULL, 0x0000102000804000ULL, 0x0000102040000000ULL, 0x0000102040004000ULL, 0x0000102040800000ULL, 0x0000102040804000ULL,
		0x0008000000000000ULL, 0x0008000000004000ULL, 0x0008000000800000ULL, 0x0008000000804000ULL, 0x0008000040000000ULL, 0x0008000040004000ULL, 0x0008000040800000ULL, 0x0008000040804000ULL,
		0x0008002000000000ULL, 0x0008002000004000ULL, 0x0008002000800000ULL, 0x0008002000804000ULL, 0x0008002040000000ULL, 0x0008002040004000ULL, 0x0008002040800000ULL, 0x0008002040804000ULL,
		0x0008100000000000ULL, 0x0008100000004000ULL, 0x0008100000800000ULL, 0x0008100000804000ULL, 0x0008100040000000ULL, 0x0008100040004000ULL, 0x0008100040800000ULL, 0x0008100040804000ULL,
		0x0008102000000000ULL, 0x0008102000004000ULL, 0x0008102000800000ULL, 0x0008102000804000ULL, 0x0008102040000000ULL, 0x0008102040004000ULL, 0x0008102040800000ULL, 0x0008102040804000ULL,
};

/** conversion from an 8-bit line to the F2-H4-E7 line */
unsigned long long F2H4E7[64] = {
		0x0000000000000000ULL, 0x0000000000002000ULL, 0x0000000000400000ULL, 0x0000000000402000ULL, 0x0000000080000000ULL, 0x0000000080002000ULL, 0x0000000080400000ULL, 0x0000000080402000ULL,
		0x0000004000000000ULL, 0x0000004000002000ULL, 0x0000004000400000ULL, 0x0000004000402000ULL, 0x0000004080000000ULL, 0x0000004080002000ULL, 0x0000004080400000ULL, 0x0000004080402000ULL,
		0x0000200000000000ULL, 0x0000200000002000ULL, 0x0000200000400000ULL, 0x0000200000402000ULL, 0x0000200080000000ULL, 0x0000200080002000ULL, 0x0000200080400000ULL, 0x0000200080402000ULL,
		0x0000204000000000ULL, 0x0000204000002000ULL, 0x0000204000400000ULL, 0x0000204000402000ULL, 0x0000204080000000ULL, 0x0000204080002000ULL, 0x0000204080400000ULL, 0x0000204080402000ULL,
		0x0010000000000000ULL, 0x0010000000002000ULL, 0x0010000000400000ULL, 0x0010000000402000ULL, 0x0010000080000000ULL, 0x0010000080002000ULL, 0x0010000080400000ULL, 0x0010000080402000ULL,
		0x0010004000000000ULL, 0x0010004000002000ULL, 0x0010004000400000ULL, 0x0010004000402000ULL, 0x0010004080000000ULL, 0x0010004080002000ULL, 0x0010004080400000ULL, 0x0010004080402000ULL,
		0x0010200000000000ULL, 0x0010200000002000ULL, 0x0010200000400000ULL, 0x0010200000402000ULL, 0x0010200080000000ULL, 0x0010200080002000ULL, 0x0010200080400000ULL, 0x0010200080402000ULL,
		0x0010204000000000ULL, 0x0010204000002000ULL, 0x0010204000400000ULL, 0x0010204000402000ULL, 0x0010204080000000ULL, 0x0010204080002000ULL, 0x0010204080400000ULL, 0x0010204080402000ULL,
};

/** conversion from an 8-bit line to the E2-H5-F7 line */
unsigned long long E2H5F7[64] = {
		0x0000000000000000ULL, 0x0000000000001000ULL, 0x0000000000200000ULL, 0x0000000000201000ULL, 0x0000000040000000ULL, 0x0000000040001000ULL, 0x0000000040200000ULL, 0x0000000040201000ULL,
		0x0000008000000000ULL, 0x0000008000001000ULL, 0x0000008000200000ULL, 0x0000008000201000ULL, 0x0000008040000000ULL, 0x0000008040001000ULL, 0x0000008040200000ULL, 0x0000008040201000ULL,
		0x0000400000000000ULL, 0x0000400000001000ULL, 0x0000400000200000ULL, 0x0000400000201000ULL, 0x0000400040000000ULL, 0x0000400040001000ULL, 0x0000400040200000ULL, 0x0000400040201000ULL,
		0x0000408000000000ULL, 0x0000408000001000ULL, 0x0000408000200000ULL, 0x0000408000201000ULL, 0x0000408040000000ULL, 0x0000408040001000ULL, 0x0000408040200000ULL, 0x0000408040201000ULL,
		0x0020000000000000ULL, 0x0020000000001000ULL, 0x0020000000200000ULL, 0x0020000000201000ULL, 0x0020000040000000ULL, 0x0020000040001000ULL, 0x0020000040200000ULL, 0x0020000040201000ULL,
		0x0020008000000000ULL, 0x0020008000001000ULL, 0x0020008000200000ULL, 0x0020008000201000ULL, 0x0020008040000000ULL, 0x0020008040001000ULL, 0x0020008040200000ULL, 0x0020008040201000ULL,
		0x0020400000000000ULL, 0x0020400000001000ULL, 0x0020400000200000ULL, 0x0020400000201000ULL, 0x0020400040000000ULL, 0x0020400040001000ULL, 0x0020400040200000ULL, 0x0020400040201000ULL,
		0x0020408000000000ULL, 0x0020408000001000ULL, 0x0020408000200000ULL, 0x0020408000201000ULL, 0x0020408040000000ULL, 0x0020408040001000ULL, 0x0020408040200000ULL, 0x0020408040201000ULL,
};

/** conversion from an 8-bit line to the D2-H6-G7 line */
unsigned long long D2H6G7[64] = {
		0x0000000000000000ULL, 0x0000000000000800ULL, 0x0000000000100000ULL, 0x0000000000100800ULL, 0x0000000020000000ULL, 0x0000000020000800ULL, 0x0000000020100000ULL, 0x0000000020100800ULL,
		0x0000004000000000ULL, 0x0000004000000800ULL, 0x0000004000100000ULL, 0x0000004000100800ULL, 0x0000004020000000ULL, 0x0000004020000800ULL, 0x0000004020100000ULL, 0x0000004020100800ULL,
		0x0000800000000000ULL, 0x0000800000000800ULL, 0x0000800000100000ULL, 0x0000800000100800ULL, 0x0000800020000000ULL, 0x0000800020000800ULL, 0x0000800020100000ULL, 0x0000800020100800ULL,
		0x0000804000000000ULL, 0x0000804000000800ULL, 0x0000804000100000ULL, 0x0000804000100800ULL, 0x0000804020000000ULL, 0x0000804020000800ULL, 0x0000804020100000ULL, 0x0000804020100800ULL,
		0x0040000000000000ULL, 0x0040000000000800ULL, 0x0040000000100000ULL, 0x0040000000100800ULL, 0x0040000020000000ULL, 0x0040000020000800ULL, 0x0040000020100000ULL, 0x0040000020100800ULL,
		0x0040004000000000ULL, 0x0040004000000800ULL, 0x0040004000100000ULL, 0x0040004000100800ULL, 0x0040004020000000ULL, 0x0040004020000800ULL, 0x0040004020100000ULL, 0x0040004020100800ULL,
		0x0040800000000000ULL, 0x0040800000000800ULL, 0x0040800000100000ULL, 0x0040800000100800ULL, 0x0040800020000000ULL, 0x0040800020000800ULL, 0x0040800020100000ULL, 0x0040800020100800ULL,
		0x0040804000000000ULL, 0x0040804000000800ULL, 0x0040804000100000ULL, 0x0040804000100800ULL, 0x0040804020000000ULL, 0x0040804020000800ULL, 0x0040804020100000ULL, 0x0040804020100800ULL,
};

/** conversion from an 8-bit line to the F2-G3-C7 line */
unsigned long long F2G3C7[64] = {
		0x0000000000000000ULL, 0x0000000000002000ULL, 0x0000000000400000ULL, 0x0000000000402000ULL, 0x0000000020000000ULL, 0x0000000020002000ULL, 0x0000000020400000ULL, 0x0000000020402000ULL,
		0x0000001000000000ULL, 0x0000001000002000ULL, 0x0000001000400000ULL, 0x0000001000402000ULL, 0x0000001020000000ULL, 0x0000001020002000ULL, 0x0000001020400000ULL, 0x0000001020402000ULL,
		0x0000080000000000ULL, 0x0000080000002000ULL, 0x0000080000400000ULL, 0x0000080000402000ULL, 0x0000080020000000ULL, 0x0000080020002000ULL, 0x0000080020400000ULL, 0x0000080020402000ULL,
		0x0000081000000000ULL, 0x0000081000002000ULL, 0x0000081000400000ULL, 0x0000081000402000ULL, 0x0000081020000000ULL, 0x0000081020002000ULL, 0x0000081020400000ULL, 0x0000081020402000ULL,
		0x0004000000000000ULL, 0x0004000000002000ULL, 0x0004000000400000ULL, 0x0004000000402000ULL, 0x0004000020000000ULL, 0x0004000020002000ULL, 0x0004000020400000ULL, 0x0004000020402000ULL,
		0x0004001000000000ULL, 0x0004001000002000ULL, 0x0004001000400000ULL, 0x0004001000402000ULL, 0x0004001020000000ULL, 0x0004001020002000ULL, 0x0004001020400000ULL, 0x0004001020402000ULL,
		0x0004080000000000ULL, 0x0004080000002000ULL, 0x0004080000400000ULL, 0x0004080000402000ULL, 0x0004080020000000ULL, 0x0004080020002000ULL, 0x0004080020400000ULL, 0x0004080020402000ULL,
		0x0004081000000000ULL, 0x0004081000002000ULL, 0x0004081000400000ULL, 0x0004081000402000ULL, 0x0004081020000000ULL, 0x0004081020002000ULL, 0x0004081020400000ULL, 0x0004081020402000ULL,
};

/** conversion from an 8-bit line to the E2-G4-D7 line */
unsigned long long E2G4D7[64] = {
		0x0000000000000000ULL, 0x0000000000001000ULL, 0x0000000000200000ULL, 0x0000000000201000ULL, 0x0000000040000000ULL, 0x0000000040001000ULL, 0x0000000040200000ULL, 0x0000000040201000ULL,
		0x0000002000000000ULL, 0x0000002000001000ULL, 0x0000002000200000ULL, 0x0000002000201000ULL, 0x0000002040000000ULL, 0x0000002040001000ULL, 0x0000002040200000ULL, 0x0000002040201000ULL,
		0x0000100000000000ULL, 0x0000100000001000ULL, 0x0000100000200000ULL, 0x0000100000201000ULL, 0x0000100040000000ULL, 0x0000100040001000ULL, 0x0000100040200000ULL, 0x0000100040201000ULL,
		0x0000102000000000ULL, 0x0000102000001000ULL, 0x0000102000200000ULL, 0x0000102000201000ULL, 0x0000102040000000ULL, 0x0000102040001000ULL, 0x0000102040200000ULL, 0x0000102040201000ULL,
		0x0008000000000000ULL, 0x0008000000001000ULL, 0x0008000000200000ULL, 0x0008000000201000ULL, 0x0008000040000000ULL, 0x0008000040001000ULL, 0x0008000040200000ULL, 0x0008000040201000ULL,
		0x0008002000000000ULL, 0x0008002000001000ULL, 0x0008002000200000ULL, 0x0008002000201000ULL, 0x0008002040000000ULL, 0x0008002040001000ULL, 0x0008002040200000ULL, 0x0008002040201000ULL,
		0x0008100000000000ULL, 0x0008100000001000ULL, 0x0008100000200000ULL, 0x0008100000201000ULL, 0x0008100040000000ULL, 0x0008100040001000ULL, 0x0008100040200000ULL, 0x0008100040201000ULL,
		0x0008102000000000ULL, 0x0008102000001000ULL, 0x0008102000200000ULL, 0x0008102000201000ULL, 0x0008102040000000ULL, 0x0008102040001000ULL, 0x0008102040200000ULL, 0x0008102040201000ULL,
};

/** conversion from an 8-bit line to the D2-G5-E7 line */
unsigned long long D2G5E7[64] = {
		0x0000000000000000ULL, 0x0000000000000800ULL, 0x0000000000100000ULL, 0x0000000000100800ULL, 0x0000000020000000ULL, 0x0000000020000800ULL, 0x0000000020100000ULL, 0x0000000020100800ULL,
		0x0000004000000000ULL, 0x0000004000000800ULL, 0x0000004000100000ULL, 0x0000004000100800ULL, 0x0000004020000000ULL, 0x0000004020000800ULL, 0x0000004020100000ULL, 0x0000004020100800ULL,
		0x0000200000000000ULL, 0x0000200000000800ULL, 0x0000200000100000ULL, 0x0000200000100800ULL, 0x0000200020000000ULL, 0x0000200020000800ULL, 0x0000200020100000ULL, 0x0000200020100800ULL,
		0x0000204000000000ULL, 0x0000204000000800ULL, 0x0000204000100000ULL, 0x0000204000100800ULL, 0x0000204020000000ULL, 0x0000204020000800ULL, 0x0000204020100000ULL, 0x0000204020100800ULL,
		0x0010000000000000ULL, 0x0010000000000800ULL, 0x0010000000100000ULL, 0x0010000000100800ULL, 0x0010000020000000ULL, 0x0010000020000800ULL, 0x0010000020100000ULL, 0x0010000020100800ULL,
		0x0010004000000000ULL, 0x0010004000000800ULL, 0x0010004000100000ULL, 0x0010004000100800ULL, 0x0010004020000000ULL, 0x0010004020000800ULL, 0x0010004020100000ULL, 0x0010004020100800ULL,
		0x0010200000000000ULL, 0x0010200000000800ULL, 0x0010200000100000ULL, 0x0010200000100800ULL, 0x0010200020000000ULL, 0x0010200020000800ULL, 0x0010200020100000ULL, 0x0010200020100800ULL,
		0x0010204000000000ULL, 0x0010204000000800ULL, 0x0010204000100000ULL, 0x0010204000100800ULL, 0x0010204020000000ULL, 0x0010204020000800ULL, 0x0010204020100000ULL, 0x0010204020100800ULL,
};

/** conversion from an 8-bit line to the C2-G6-F7 line */
unsigned long long C2G6F7[64] = {
		0x0000000000000000ULL, 0x0000000000000400ULL, 0x0000000000080000ULL, 0x0000000000080400ULL, 0x0000000010000000ULL, 0x0000000010000400ULL, 0x0000000010080000ULL, 0x0000000010080400ULL,
		0x0000002000000000ULL, 0x0000002000000400ULL, 0x0000002000080000ULL, 0x0000002000080400ULL, 0x0000002010000000ULL, 0x0000002010000400ULL, 0x0000002010080000ULL, 0x0000002010080400ULL,
		0x0000400000000000ULL, 0x0000400000000400ULL, 0x0000400000080000ULL, 0x0000400000080400ULL, 0x0000400010000000ULL, 0x0000400010000400ULL, 0x0000400010080000ULL, 0x0000400010080400ULL,
		0x0000402000000000ULL, 0x0000402000000400ULL, 0x0000402000080000ULL, 0x0000402000080400ULL, 0x0000402010000000ULL, 0x0000402010000400ULL, 0x0000402010080000ULL, 0x0000402010080400ULL,
		0x0020000000000000ULL, 0x0020000000000400ULL, 0x0020000000080000ULL, 0x0020000000080400ULL, 0x0020000010000000ULL, 0x0020000010000400ULL, 0x0020000010080000ULL, 0x0020000010080400ULL,
		0x0020002000000000ULL, 0x0020002000000400ULL, 0x0020002000080000ULL, 0x0020002000080400ULL, 0x0020002010000000ULL, 0x0020002010000400ULL, 0x0020002010080000ULL, 0x0020002010080400ULL,
		0x0020400000000000ULL, 0x0020400000000400ULL, 0x0020400000080000ULL, 0x0020400000080400ULL, 0x0020400010000000ULL, 0x0020400010000400ULL, 0x0020400010080000ULL, 0x0020400010080400ULL,
		0x0020402000000000ULL, 0x0020402000000400ULL, 0x0020402000080000ULL, 0x0020402000080400ULL, 0x0020402010000000ULL, 0x0020402010000400ULL, 0x0020402010080000ULL, 0x0020402010080400ULL,
};

/** conversion from an 8-bit line to the B2-A3-E7 line */
unsigned long long B2A3E7[64] = {
		0x0000000000000000ULL, 0x0000000000000200ULL, 0x0000000000010000ULL, 0x0000000000010200ULL, 0x0000000002000000ULL, 0x0000000002000200ULL, 0x0000000002010000ULL, 0x0000000002010200ULL,
		0x0000000400000000ULL, 0x0000000400000200ULL, 0x0000000400010000ULL, 0x0000000400010200ULL, 0x0000000402000000ULL, 0x0000000402000200ULL, 0x0000000402010000ULL, 0x0000000402010200ULL,
		0x0000080000000000ULL, 0x0000080000000200ULL, 0x0000080000010000ULL, 0x0000080000010200ULL, 0x0000080002000000ULL, 0x0000080002000200ULL, 0x0000080002010000ULL, 0x0000080002010200ULL,
		0x0000080400000000ULL, 0x0000080400000200ULL, 0x0000080400010000ULL, 0x0000080400010200ULL, 0x0000080402000000ULL, 0x0000080402000200ULL, 0x0000080402010000ULL, 0x0000080402010200ULL,
		0x0010000000000000ULL, 0x0010000000000200ULL, 0x0010000000010000ULL, 0x0010000000010200ULL, 0x0010000002000000ULL, 0x0010000002000200ULL, 0x0010000002010000ULL, 0x0010000002010200ULL,
		0x0010000400000000ULL, 0x0010000400000200ULL, 0x0010000400010000ULL, 0x0010000400010200ULL, 0x0010000402000000ULL, 0x0010000402000200ULL, 0x0010000402010000ULL, 0x0010000402010200ULL,
		0x0010080000000000ULL, 0x0010080000000200ULL, 0x0010080000010000ULL, 0x0010080000010200ULL, 0x0010080002000000ULL, 0x0010080002000200ULL, 0x0010080002010000ULL, 0x0010080002010200ULL,
		0x0010080400000000ULL, 0x0010080400000200ULL, 0x0010080400010000ULL, 0x0010080400010200ULL, 0x0010080402000000ULL, 0x0010080402000200ULL, 0x0010080402010000ULL, 0x0010080402010200ULL,
};

/** conversion from an 8-bit line to the C2-A4-D7 line */
unsigned long long C2A4D7[64] = {
		0x0000000000000000ULL, 0x0000000000000400ULL, 0x0000000000020000ULL, 0x0000000000020400ULL, 0x0000000001000000ULL, 0x0000000001000400ULL, 0x0000000001020000ULL, 0x0000000001020400ULL,
		0x0000000200000000ULL, 0x0000000200000400ULL, 0x0000000200020000ULL, 0x0000000200020400ULL, 0x0000000201000000ULL, 0x0000000201000400ULL, 0x0000000201020000ULL, 0x0000000201020400ULL,
		0x0000040000000000ULL, 0x0000040000000400ULL, 0x0000040000020000ULL, 0x0000040000020400ULL, 0x0000040001000000ULL, 0x0000040001000400ULL, 0x0000040001020000ULL, 0x0000040001020400ULL,
		0x0000040200000000ULL, 0x0000040200000400ULL, 0x0000040200020000ULL, 0x0000040200020400ULL, 0x0000040201000000ULL, 0x0000040201000400ULL, 0x0000040201020000ULL, 0x0000040201020400ULL,
		0x0008000000000000ULL, 0x0008000000000400ULL, 0x0008000000020000ULL, 0x0008000000020400ULL, 0x0008000001000000ULL, 0x0008000001000400ULL, 0x0008000001020000ULL, 0x0008000001020400ULL,
		0x0008000200000000ULL, 0x0008000200000400ULL, 0x0008000200020000ULL, 0x0008000200020400ULL, 0x0008000201000000ULL, 0x0008000201000400ULL, 0x0008000201020000ULL, 0x0008000201020400ULL,
		0x0008040000000000ULL, 0x0008040000000400ULL, 0x0008040000020000ULL, 0x0008040000020400ULL, 0x0008040001000000ULL, 0x0008040001000400ULL, 0x0008040001020000ULL, 0x0008040001020400ULL,
		0x0008040200000000ULL, 0x0008040200000400ULL, 0x0008040200020000ULL, 0x0008040200020400ULL, 0x0008040201000000ULL, 0x0008040201000400ULL, 0x0008040201020000ULL, 0x0008040201020400ULL,
};

/** conversion from an 8-bit line to the D2-A5-C7 line */
unsigned long long D2A5C7[64] = {
		0x0000000000000000ULL, 0x0000000000000800ULL, 0x0000000000040000ULL, 0x0000000000040800ULL, 0x0000000002000000ULL, 0x0000000002000800ULL, 0x0000000002040000ULL, 0x0000000002040800ULL,
		0x0000000100000000ULL, 0x0000000100000800ULL, 0x0000000100040000ULL, 0x0000000100040800ULL, 0x0000000102000000ULL, 0x0000000102000800ULL, 0x0000000102040000ULL, 0x0000000102040800ULL,
		0x0000020000000000ULL, 0x0000020000000800ULL, 0x0000020000040000ULL, 0x0000020000040800ULL, 0x0000020002000000ULL, 0x0000020002000800ULL, 0x0000020002040000ULL, 0x0000020002040800ULL,
		0x0000020100000000ULL, 0x0000020100000800ULL, 0x0000020100040000ULL, 0x0000020100040800ULL, 0x0000020102000000ULL, 0x0000020102000800ULL, 0x0000020102040000ULL, 0x0000020102040800ULL,
		0x0004000000000000ULL, 0x0004000000000800ULL, 0x0004000000040000ULL, 0x0004000000040800ULL, 0x0004000002000000ULL, 0x0004000002000800ULL, 0x0004000002040000ULL, 0x0004000002040800ULL,
		0x0004000100000000ULL, 0x0004000100000800ULL, 0x0004000100040000ULL, 0x0004000100040800ULL, 0x0004000102000000ULL, 0x0004000102000800ULL, 0x0004000102040000ULL, 0x0004000102040800ULL,
		0x0004020000000000ULL, 0x0004020000000800ULL, 0x0004020000040000ULL, 0x0004020000040800ULL, 0x0004020002000000ULL, 0x0004020002000800ULL, 0x0004020002040000ULL, 0x0004020002040800ULL,
		0x0004020100000000ULL, 0x0004020100000800ULL, 0x0004020100040000ULL, 0x0004020100040800ULL, 0x0004020102000000ULL, 0x0004020102000800ULL, 0x0004020102040000ULL, 0x0004020102040800ULL,
};

/** conversion from an 8-bit line to the E2-A6-B7 line */
unsigned long long E2A6B7[64] = {
		0x0000000000000000ULL, 0x0000000000001000ULL, 0x0000000000080000ULL, 0x0000000000081000ULL, 0x0000000004000000ULL, 0x0000000004001000ULL, 0x0000000004080000ULL, 0x0000000004081000ULL,
		0x0000000200000000ULL, 0x0000000200001000ULL, 0x0000000200080000ULL, 0x0000000200081000ULL, 0x0000000204000000ULL, 0x0000000204001000ULL, 0x0000000204080000ULL, 0x0000000204081000ULL,
		0x0000010000000000ULL, 0x0000010000001000ULL, 0x0000010000080000ULL, 0x0000010000081000ULL, 0x0000010004000000ULL, 0x0000010004001000ULL, 0x0000010004080000ULL, 0x0000010004081000ULL,
		0x0000010200000000ULL, 0x0000010200001000ULL, 0x0000010200080000ULL, 0x0000010200081000ULL, 0x0000010204000000ULL, 0x0000010204001000ULL, 0x0000010204080000ULL, 0x0000010204081000ULL,
		0x0002000000000000ULL, 0x0002000000001000ULL, 0x0002000000080000ULL, 0x0002000000081000ULL, 0x0002000004000000ULL, 0x0002000004001000ULL, 0x0002000004080000ULL, 0x0002000004081000ULL,
		0x0002000200000000ULL, 0x0002000200001000ULL, 0x0002000200080000ULL, 0x0002000200081000ULL, 0x0002000204000000ULL, 0x0002000204001000ULL, 0x0002000204080000ULL, 0x0002000204081000ULL,
		0x0002010000000000ULL, 0x0002010000001000ULL, 0x0002010000080000ULL, 0x0002010000081000ULL, 0x0002010004000000ULL, 0x0002010004001000ULL, 0x0002010004080000ULL, 0x0002010004081000ULL,
		0x0002010200000000ULL, 0x0002010200001000ULL, 0x0002010200080000ULL, 0x0002010200081000ULL, 0x0002010204000000ULL, 0x0002010204001000ULL, 0x0002010204080000ULL, 0x0002010204081000ULL,
};

/** conversion from an 8-bit line to the C2-B3-F7 line */
unsigned long long C2B3F7[64] = {
		0x0000000000000000ULL, 0x0000000000000400ULL, 0x0000000000020000ULL, 0x0000000000020400ULL, 0x0000000004000000ULL, 0x0000000004000400ULL, 0x0000000004020000ULL, 0x0000000004020400ULL,
		0x0000000800000000ULL, 0x0000000800000400ULL, 0x0000000800020000ULL, 0x0000000800020400ULL, 0x0000000804000000ULL, 0x0000000804000400ULL, 0x0000000804020000ULL, 0x0000000804020400ULL,
		0x0000100000000000ULL, 0x0000100000000400ULL, 0x0000100000020000ULL, 0x0000100000020400ULL, 0x0000100004000000ULL, 0x0000100004000400ULL, 0x0000100004020000ULL, 0x0000100004020400ULL,
		0x0000100800000000ULL, 0x0000100800000400ULL, 0x0000100800020000ULL, 0x0000100800020400ULL, 0x0000100804000000ULL, 0x0000100804000400ULL, 0x0000100804020000ULL, 0x0000100804020400ULL,
		0x0020000000000000ULL, 0x0020000000000400ULL, 0x0020000000020000ULL, 0x0020000000020400ULL, 0x0020000004000000ULL, 0x0020000004000400ULL, 0x0020000004020000ULL, 0x0020000004020400ULL,
		0x0020000800000000ULL, 0x0020000800000400ULL, 0x0020000800020000ULL, 0x0020000800020400ULL, 0x0020000804000000ULL, 0x0020000804000400ULL, 0x0020000804020000ULL, 0x0020000804020400ULL,
		0x0020100000000000ULL, 0x0020100000000400ULL, 0x0020100000020000ULL, 0x0020100000020400ULL, 0x0020100004000000ULL, 0x0020100004000400ULL, 0x0020100004020000ULL, 0x0020100004020400ULL,
		0x0020100800000000ULL, 0x0020100800000400ULL, 0x0020100800020000ULL, 0x0020100800020400ULL, 0x0020100804000000ULL, 0x0020100804000400ULL, 0x0020100804020000ULL, 0x0020100804020400ULL,
};

/** conversion from an 8-bit line to the D2-B4-E7 line */
unsigned long long D2B4E7[64] = {
		0x0000000000000000ULL, 0x0000000000000800ULL, 0x0000000000040000ULL, 0x0000000000040800ULL, 0x0000000002000000ULL, 0x0000000002000800ULL, 0x0000000002040000ULL, 0x0000000002040800ULL,
		0x0000000400000000ULL, 0x0000000400000800ULL, 0x0000000400040000ULL, 0x0000000400040800ULL, 0x0000000402000000ULL, 0x0000000402000800ULL, 0x0000000402040000ULL, 0x0000000402040800ULL,
		0x0000080000000000ULL, 0x0000080000000800ULL, 0x0000080000040000ULL, 0x0000080000040800ULL, 0x0000080002000000ULL, 0x0000080002000800ULL, 0x0000080002040000ULL, 0x0000080002040800ULL,
		0x0000080400000000ULL, 0x0000080400000800ULL, 0x0000080400040000ULL, 0x0000080400040800ULL, 0x0000080402000000ULL, 0x0000080402000800ULL, 0x0000080402040000ULL, 0x0000080402040800ULL,
		0x0010000000000000ULL, 0x0010000000000800ULL, 0x0010000000040000ULL, 0x0010000000040800ULL, 0x0010000002000000ULL, 0x0010000002000800ULL, 0x0010000002040000ULL, 0x0010000002040800ULL,
		0x0010000400000000ULL, 0x0010000400000800ULL, 0x0010000400040000ULL, 0x0010000400040800ULL, 0x0010000402000000ULL, 0x0010000402000800ULL, 0x0010000402040000ULL, 0x0010000402040800ULL,
		0x0010080000000000ULL, 0x0010080000000800ULL, 0x0010080000040000ULL, 0x0010080000040800ULL, 0x0010080002000000ULL, 0x0010080002000800ULL, 0x0010080002040000ULL, 0x0010080002040800ULL,
		0x0010080400000000ULL, 0x0010080400000800ULL, 0x0010080400040000ULL, 0x0010080400040800ULL, 0x0010080402000000ULL, 0x0010080402000800ULL, 0x0010080402040000ULL, 0x0010080402040800ULL,
};

/** conversion from an 8-bit line to the E2-B5-D7 line */
unsigned long long E2B5D7[64] = {
		0x0000000000000000ULL, 0x0000000000001000ULL, 0x0000000000080000ULL, 0x0000000000081000ULL, 0x0000000004000000ULL, 0x0000000004001000ULL, 0x0000000004080000ULL, 0x0000000004081000ULL,
		0x0000000200000000ULL, 0x0000000200001000ULL, 0x0000000200080000ULL, 0x0000000200081000ULL, 0x0000000204000000ULL, 0x0000000204001000ULL, 0x0000000204080000ULL, 0x0000000204081000ULL,
		0x0000040000000000ULL, 0x0000040000001000ULL, 0x0000040000080000ULL, 0x0000040000081000ULL, 0x0000040004000000ULL, 0x0000040004001000ULL, 0x0000040004080000ULL, 0x0000040004081000ULL,
		0x0000040200000000ULL, 0x0000040200001000ULL, 0x0000040200080000ULL, 0x0000040200081000ULL, 0x0000040204000000ULL, 0x0000040204001000ULL, 0x0000040204080000ULL, 0x0000040204081000ULL,
		0x0008000000000000ULL, 0x0008000000001000ULL, 0x0008000000080000ULL, 0x0008000000081000ULL, 0x0008000004000000ULL, 0x0008000004001000ULL, 0x0008000004080000ULL, 0x0008000004081000ULL,
		0x0008000200000000ULL, 0x0008000200001000ULL, 0x0008000200080000ULL, 0x0008000200081000ULL, 0x0008000204000000ULL, 0x0008000204001000ULL, 0x0008000204080000ULL, 0x0008000204081000ULL,
		0x0008040000000000ULL, 0x0008040000001000ULL, 0x0008040000080000ULL, 0x0008040000081000ULL, 0x0008040004000000ULL, 0x0008040004001000ULL, 0x0008040004080000ULL, 0x0008040004081000ULL,
		0x0008040200000000ULL, 0x0008040200001000ULL, 0x0008040200080000ULL, 0x0008040200081000ULL, 0x0008040204000000ULL, 0x0008040204001000ULL, 0x0008040204080000ULL, 0x0008040204081000ULL,
};

/** conversion from an 8-bit line to the F2-B6-C7 line */
unsigned long long F2B6C7[64] = {
		0x0000000000000000ULL, 0x0000000000002000ULL, 0x0000000000100000ULL, 0x0000000000102000ULL, 0x0000000008000000ULL, 0x0000000008002000ULL, 0x0000000008100000ULL, 0x0000000008102000ULL,
		0x0000000400000000ULL, 0x0000000400002000ULL, 0x0000000400100000ULL, 0x0000000400102000ULL, 0x0000000408000000ULL, 0x0000000408002000ULL, 0x0000000408100000ULL, 0x0000000408102000ULL,
		0x0000020000000000ULL, 0x0000020000002000ULL, 0x0000020000100000ULL, 0x0000020000102000ULL, 0x0000020008000000ULL, 0x0000020008002000ULL, 0x0000020008100000ULL, 0x0000020008102000ULL,
		0x0000020400000000ULL, 0x0000020400002000ULL, 0x0000020400100000ULL, 0x0000020400102000ULL, 0x0000020408000000ULL, 0x0000020408002000ULL, 0x0000020408100000ULL, 0x0000020408102000ULL,
		0x0004000000000000ULL, 0x0004000000002000ULL, 0x0004000000100000ULL, 0x0004000000102000ULL, 0x0004000008000000ULL, 0x0004000008002000ULL, 0x0004000008100000ULL, 0x0004000008102000ULL,
		0x0004000400000000ULL, 0x0004000400002000ULL, 0x0004000400100000ULL, 0x0004000400102000ULL, 0x0004000408000000ULL, 0x0004000408002000ULL, 0x0004000408100000ULL, 0x0004000408102000ULL,
		0x0004020000000000ULL, 0x0004020000002000ULL, 0x0004020000100000ULL, 0x0004020000102000ULL, 0x0004020008000000ULL, 0x0004020008002000ULL, 0x0004020008100000ULL, 0x0004020008102000ULL,
		0x0004020400000000ULL, 0x0004020400002000ULL, 0x0004020400100000ULL, 0x0004020400102000ULL, 0x0004020408000000ULL, 0x0004020408002000ULL, 0x0004020408100000ULL, 0x0004020408102000ULL,
};


/**
 * Compute flipped discs when playing on square A1.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_A1(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[0][(O & 0x0001010101010100) * 0x0002040810204000 >> 57] & (P & 0x0101010101010101) * 0x0102040810204080 >> 56;
	index_v = FLIPPED[0][index_v];
	flipped = A2A7[index_v];

	index_h = OUTFLANK[0][(O >> 1) & 0x3f] & P;
	flipped |= ((unsigned long long) FLIPPED[0][index_h]) << 1;

	index_d9 = OUTFLANK[0][(O & 0x0040201008040200ULL)* 0x0101010101010101ULL >> 57] & (P & 0x8040201008040201ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[0][index_d9];
	flipped |= B2G7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square B1.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_B1(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[0][(O & 0x0002020202020200) * 0x0001020408102000 >> 57] & (P & 0x0202020202020202) * 0x0081020408102040 >> 56;
	index_v = FLIPPED[0][index_v];
	flipped = B2B7[index_v];

	index_h = OUTFLANK[1][(O >> 1) & 0x3f] & P;
	flipped |= ((unsigned long long) FLIPPED[1][index_h]) << 1;

	index_d9 = OUTFLANK[1][(O & 0x0000402010080400ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0080402010080402ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[1][index_d9];
	flipped |= C2G6[index_d9 >> 1];


	return flipped;
}

/**
 * Compute flipped discs when playing on square C1.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_C1(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[0][(O & 0x0004040404040400) * 0x0000810204081000 >> 57] & (P & 0x0404040404040404) * 0x0040810204081020 >> 56;
	index_v = FLIPPED[0][index_v];
	flipped = C2C7[index_v];

	index_h = OUTFLANK[2][(O >> 1) & 0x3f] & P;
	flipped |= ((unsigned long long) FLIPPED[2][index_h]) << 1;

	index_d = OUTFLANK[2][(O & 0x0000004020100a04ULL) * 0x0101010101010101ULL >> 57] & (P & 0x0000804020110a04ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[2][index_d];
	flipped |= B2C1G5[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square D1.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_D1(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[0][(O & 0x0008080808080800) * 0x0000408102040800 >> 57] & (P & 0x0808080808080808) * 0x0020408102040810 >> 56;
	index_v = FLIPPED[0][index_v];
	flipped = D2D7[index_v];

	index_h = OUTFLANK[3][(O >> 1) & 0x3f] & P;
	flipped |= ((unsigned long long) FLIPPED[3][index_h]) << 1;

	index_d = OUTFLANK[3][(O & 0x0000000040221408ULL) * 0x0101010101010101ULL >> 57] & (P & 0x0000008041221408ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[3][index_d];
	flipped |= B3D1G4[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square E1.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_E1(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[0][(O & 0x0010101010101000) * 0x0000204081020400 >> 57] & (P & 0x1010101010101010) * 0x0010204081020408 >> 56;
	index_v = FLIPPED[0][index_v];
	flipped = E2E7[index_v];

	index_h = OUTFLANK[4][(O >> 1) & 0x3f] & P;
	flipped |= ((unsigned long long) FLIPPED[4][index_h]) << 1;

	index_d = OUTFLANK[4][(O & 0x0000000002442810ULL) * 0x0101010101010101ULL >> 57] & (P & 0x0000000182442810ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[4][index_d];
	flipped |= B4E1G3[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square F1.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_F1(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[0][(O & 0x0020202020202000) * 0x0000102040810200 >> 57] & (P & 0x2020202020202020) * 0x0008102040810204 >> 56;
	index_v = FLIPPED[0][index_v];
	flipped = F2F7[index_v];

	index_h = OUTFLANK[5][(O >> 1) & 0x3f] & P;
	flipped |= ((unsigned long long) FLIPPED[5][index_h]) << 1;

	index_d = OUTFLANK[5][(O & 0x0000000204085020ULL) * 0x0101010101010101ULL >> 57] & (P & 0x0000010204885020ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[5][index_d];
	flipped |= B5F1G2[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square G1.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_G1(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7;
	register unsigned long long flipped;

	index_v = OUTFLANK[0][(O & 0x0040404040404000) * 0x0000081020408100 >> 57] & (P & 0x4040404040404040) * 0x0004081020408102 >> 56;
	index_v = FLIPPED[0][index_v];
	flipped = G2G7[index_v];

	index_h = OUTFLANK[6][(O >> 1) & 0x3f] & P;
	flipped |= ((unsigned long long) FLIPPED[6][index_h]) << 1;

	index_d7 = OUTFLANK[6][(O & 0x0000020408102040ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0001020408102040ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[6][index_d7];
	flipped |= F2B6[index_d7];


	return flipped;
}

/**
 * Compute flipped discs when playing on square H1.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_H1(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7;
	register unsigned long long flipped;

	index_v = OUTFLANK[0][(O & 0x0080808080808000) * 0x0000040810204080 >> 57] & (P & 0x8080808080808080) * 0x0002040810204081 >> 56;
	index_v = FLIPPED[0][index_v];
	flipped = H2H7[index_v];

	index_h = OUTFLANK[7][(O >> 1) & 0x3f] & P;
	flipped |= ((unsigned long long) FLIPPED[7][index_h]) << 1;

	index_d7 = OUTFLANK[7][(O & 0x0002040810204080ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0102040810204080ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[7][index_d7];
	flipped |= G2B7[index_d7];


	return flipped;
}

/**
 * Compute flipped discs when playing on square A2.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_A2(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[1][(O & 0x0001010101010100) * 0x0002040810204000 >> 57] & (P & 0x0101010101010101) * 0x0102040810204080 >> 56;
	index_v = FLIPPED[1][index_v];
	flipped = A2A7[index_v];

	index_h = OUTFLANK[0][(O >> 9) & 0x3f] & (P >> 8);
	flipped |= ((unsigned long long) FLIPPED[0][index_h]) << 9;

	index_d9 = OUTFLANK[0][(O & 0x0020100804020000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x4020100804020100ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[0][index_d9];
	flipped |= B3F7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square B2.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_B2(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[1][(O & 0x0002020202020200) * 0x0001020408102000 >> 57] & (P & 0x0202020202020202) * 0x0081020408102040 >> 56;
	index_v = FLIPPED[1][index_v];
	flipped = B2B7[index_v];

	index_h = OUTFLANK[1][(O >> 9) & 0x3f] & (P >> 8);
	flipped |= ((unsigned long long) FLIPPED[1][index_h]) << 9;

	index_d9 = OUTFLANK[1][(O & 0x0040201008040200ULL)* 0x0101010101010101ULL >> 57] & (P & 0x8040201008040201ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[1][index_d9];
	flipped |= B2G7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square C2.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_C2(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[1][(O & 0x0004040404040400) * 0x0000810204081000 >> 57] & (P & 0x0404040404040404) * 0x0040810204081020 >> 56;
	index_v = FLIPPED[1][index_v];
	flipped = C2C7[index_v];

	index_h = OUTFLANK[2][(O >> 9) & 0x3f] & (P >> 8);
	flipped |= ((unsigned long long) FLIPPED[2][index_h]) << 9;

	index_d = OUTFLANK[2][(O & 0x00004020100a0400ULL) * 0x0101010101010101ULL >> 57] & (P & 0x00804020110a0400ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[2][index_d];
	flipped |= B3C2G6[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square D2.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_D2(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[1][(O & 0x0008080808080800) * 0x0000408102040800 >> 57] & (P & 0x0808080808080808) * 0x0020408102040810 >> 56;
	index_v = FLIPPED[1][index_v];
	flipped = D2D7[index_v];

	index_h = OUTFLANK[3][(O >> 9) & 0x3f] & (P >> 8);
	flipped |= ((unsigned long long) FLIPPED[3][index_h]) << 9;

	index_d = OUTFLANK[3][(O & 0x0000004022140800ULL) * 0x0101010101010101ULL >> 57] & (P & 0x0000804122140800ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[3][index_d];
	flipped |= B4D2G5[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square E2.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_E2(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[1][(O & 0x0010101010101000) * 0x0000204081020400 >> 57] & (P & 0x1010101010101010) * 0x0010204081020408 >> 56;
	index_v = FLIPPED[1][index_v];
	flipped = E2E7[index_v];

	index_h = OUTFLANK[4][(O >> 9) & 0x3f] & (P >> 8);
	flipped |= ((unsigned long long) FLIPPED[4][index_h]) << 9;

	index_d = OUTFLANK[4][(O & 0x0000000244281000ULL) * 0x0101010101010101ULL >> 57] & (P & 0x0000018244281000ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[4][index_d];
	flipped |= B5E2G4[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square F2.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_F2(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[1][(O & 0x0020202020202000) * 0x0000102040810200 >> 57] & (P & 0x2020202020202020) * 0x0008102040810204 >> 56;
	index_v = FLIPPED[1][index_v];
	flipped = F2F7[index_v];

	index_h = OUTFLANK[5][(O >> 9) & 0x3f] & (P >> 8);
	flipped |= ((unsigned long long) FLIPPED[5][index_h]) << 9;

	index_d = OUTFLANK[5][(O & 0x0000020408502000ULL) * 0x0101010101010101ULL >> 57] & (P & 0x0001020488502000ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[5][index_d];
	flipped |= B6F2G3[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square G2.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_G2(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7;
	register unsigned long long flipped;

	index_v = OUTFLANK[1][(O & 0x0040404040404000) * 0x0000081020408100 >> 57] & (P & 0x4040404040404040) * 0x0004081020408102 >> 56;
	index_v = FLIPPED[1][index_v];
	flipped = G2G7[index_v];

	index_h = OUTFLANK[6][(O >> 9) & 0x3f] & (P >> 8);
	flipped |= ((unsigned long long) FLIPPED[6][index_h]) << 9;

	index_d7 = OUTFLANK[6][(O & 0x0002040810204000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0102040810204080ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[6][index_d7];
	flipped |= G2B7[index_d7];


	return flipped;
}

/**
 * Compute flipped discs when playing on square H2.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_H2(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7;
	register unsigned long long flipped;

	index_v = OUTFLANK[1][(O & 0x0080808080808000) * 0x0000040810204080 >> 57] & (P & 0x8080808080808080) * 0x0002040810204081 >> 56;
	index_v = FLIPPED[1][index_v];
	flipped = H2H7[index_v];

	index_h = OUTFLANK[7][(O >> 9) & 0x3f] & (P >> 8);
	flipped |= ((unsigned long long) FLIPPED[7][index_h]) << 9;

	index_d7 = OUTFLANK[7][(O & 0x0004081020408000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0204081020408000ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[7][index_d7];
	flipped |= G3C7[index_d7 >> 1];


	return flipped;
}

/**
 * Compute flipped discs when playing on square A3.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_A3(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[2][(O & 0x0001010101010100) * 0x0002040810204000 >> 57] & (P & 0x0101010101010101) * 0x0102040810204080 >> 56;
	index_v = FLIPPED[2][index_v];
	flipped = A2A7[index_v];

	index_h = OUTFLANK[0][(O >> 17) & 0x3f] & (P >> 16);
	flipped |= ((unsigned long long) FLIPPED[0][index_h]) << 17;

	index_d = OUTFLANK[2][(((O & 0x0010080402000200ULL) + 0x0070787c7e007e00ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x2010080402010204ULL) + 0x6070787c7e7f7e7cULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[2][index_d];
	flipped |= B2A3E7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square B3.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_B3(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[2][(O & 0x0002020202020200) * 0x0001020408102000 >> 57] & (P & 0x0202020202020202) * 0x0081020408102040 >> 56;
	index_v = FLIPPED[2][index_v];
	flipped = B2B7[index_v];

	index_h = OUTFLANK[1][(O >> 17) & 0x3f] & (P >> 16);
	flipped |= ((unsigned long long) FLIPPED[1][index_h]) << 17;

	index_d = OUTFLANK[2][(((O & 0x0020100804020400ULL) + 0x006070787c7e7c00ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x4020100804020408ULL) + 0x406070787c7e7c78ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[2][index_d];
	flipped |= C2B3F7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square C3.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_C3(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[2][(O & 0x0004040404040400) * 0x0000810204081000 >> 57] & (P & 0x0404040404040404) * 0x0040810204081020 >> 56;
	index_v = FLIPPED[2][index_v];
	flipped = C2C7[index_v];

	index_h = OUTFLANK[2][(O >> 17) & 0x3f] & (P >> 16);
	flipped |= ((unsigned long long) FLIPPED[2][index_h]) << 17;

	flipped |= ((P >> 7) & 0x0000000002000000ULL & O) | ((P << 7) & 0x0000000000000800ULL & O);

	index_d9 = OUTFLANK[2][(O & 0x0040201008040200ULL)* 0x0101010101010101ULL >> 57] & (P & 0x8040201008040201ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[2][index_d9];
	flipped |= B2G7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square D3.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_D3(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[2][(O & 0x0008080808080800) * 0x0000408102040800 >> 57] & (P & 0x0808080808080808) * 0x0020408102040810 >> 56;
	index_v = FLIPPED[2][index_v];
	flipped = D2D7[index_v];

	index_h = OUTFLANK[3][(O >> 17) & 0x3f] & (P >> 16);
	flipped |= ((unsigned long long) FLIPPED[3][index_h]) << 17;

	index_d7 = OUTFLANK[3][(O & 0x0000000204081000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0000010204081020ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[3][index_d7];
	flipped |= E2B5[index_d7];

	index_d9 = OUTFLANK[3][(O & 0x0000402010080400ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0080402010080402ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[3][index_d9];
	flipped |= C2G6[index_d9 >> 1];


	return flipped;
}

/**
 * Compute flipped discs when playing on square E3.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_E3(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[2][(O & 0x0010101010101000) * 0x0000204081020400 >> 57] & (P & 0x1010101010101010) * 0x0010204081020408 >> 56;
	index_v = FLIPPED[2][index_v];
	flipped = E2E7[index_v];

	index_h = OUTFLANK[4][(O >> 17) & 0x3f] & (P >> 16);
	flipped |= ((unsigned long long) FLIPPED[4][index_h]) << 17;

	index_d7 = OUTFLANK[4][(O & 0x0000020408102000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0001020408102040ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[4][index_d7];
	flipped |= F2B6[index_d7];

	index_d9 = OUTFLANK[4][(O & 0x0000004020100800ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0000804020100804ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[4][index_d9];
	flipped |= D2G5[index_d9 >> 2];


	return flipped;
}

/**
 * Compute flipped discs when playing on square F3.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_F3(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7;
	register unsigned long long flipped;

	index_v = OUTFLANK[2][(O & 0x0020202020202000) * 0x0000102040810200 >> 57] & (P & 0x2020202020202020) * 0x0008102040810204 >> 56;
	index_v = FLIPPED[2][index_v];
	flipped = F2F7[index_v];

	index_h = OUTFLANK[5][(O >> 17) & 0x3f] & (P >> 16);
	flipped |= ((unsigned long long) FLIPPED[5][index_h]) << 17;

	index_d7 = OUTFLANK[5][(O & 0x0002040810204000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0102040810204080ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[5][index_d7];
	flipped |= G2B7[index_d7];

	flipped |= ((P >> 9) & 0x0000000040000000ULL & O) | ((P << 9) & 0x0000000000001000ULL & O);


	return flipped;
}

/**
 * Compute flipped discs when playing on square G3.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_G3(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[2][(O & 0x0040404040404000) * 0x0000081020408100 >> 57] & (P & 0x4040404040404040) * 0x0004081020408102 >> 56;
	index_v = FLIPPED[2][index_v];
	flipped = G2G7[index_v];

	index_h = OUTFLANK[6][(O >> 17) & 0x3f] & (P >> 16);
	flipped |= ((unsigned long long) FLIPPED[6][index_h]) << 17;

	index_d = OUTFLANK[2][(((O & 0x0004081020402000ULL) + 0x007c787060406000ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x0204081020402010ULL) + 0x7e7c787060406070ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[2][index_d];
	flipped |= F2G3C7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square H3.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_H3(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[2][(O & 0x0080808080808000) * 0x0000040810204080 >> 57] & (P & 0x8080808080808080) * 0x0002040810204081 >> 56;
	index_v = FLIPPED[2][index_v];
	flipped = H2H7[index_v];

	index_h = OUTFLANK[7][(O >> 17) & 0x3f] & (P >> 16);
	flipped |= ((unsigned long long) FLIPPED[7][index_h]) << 17;

	index_d = OUTFLANK[2][(((O & 0x0008102040804000ULL) + 0x0078706040004000ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x0408102040804020ULL) + 0x7c78706040004060ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[2][index_d];
	flipped |= G2H3D7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square A4.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_A4(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[3][(O & 0x0001010101010100) * 0x0002040810204000 >> 57] & (P & 0x0101010101010101) * 0x0102040810204080 >> 56;
	index_v = FLIPPED[3][index_v];
	flipped = A2A7[index_v];

	index_h = OUTFLANK[0][(O >> 25) & 0x3f] & (P >> 24);
	flipped |= ((unsigned long long) FLIPPED[0][index_h]) << 25;

	index_d = OUTFLANK[3][(((O & 0x0008040200020400ULL) + 0x00787c7e007e7c00ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x1008040201020408ULL) + 0x70787c7e7f7e7c78ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[3][index_d];
	flipped |= C2A4D7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square B4.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_B4(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[3][(O & 0x0002020202020200) * 0x0001020408102000 >> 57] & (P & 0x0202020202020202) * 0x0081020408102040 >> 56;
	index_v = FLIPPED[3][index_v];
	flipped = B2B7[index_v];

	index_h = OUTFLANK[1][(O >> 25) & 0x3f] & (P >> 24);
	flipped |= ((unsigned long long) FLIPPED[1][index_h]) << 25;

	index_d = OUTFLANK[3][(((O & 0x0010080402040800ULL) + 0x0070787c7e7c7800ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x2010080402040810ULL) + 0x6070787c7e7c7870ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[3][index_d];
	flipped |= D2B4E7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square C4.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_C4(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[3][(O & 0x0004040404040400) * 0x0000810204081000 >> 57] & (P & 0x0404040404040404) * 0x0040810204081020 >> 56;
	index_v = FLIPPED[3][index_v];
	flipped = C2C7[index_v];

	index_h = OUTFLANK[2][(O >> 25) & 0x3f] & (P >> 24);
	flipped |= ((unsigned long long) FLIPPED[2][index_h]) << 25;

	index_d7 = OUTFLANK[2][(O & 0x0000000204081000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0000010204081020ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[2][index_d7];
	flipped |= E2B5[index_d7];

	index_d9 = OUTFLANK[2][(O & 0x0020100804020000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x4020100804020100ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[2][index_d9];
	flipped |= B3F7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square D4.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_D4(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[3][(O & 0x0008080808080800) * 0x0000408102040800 >> 57] & (P & 0x0808080808080808) * 0x0020408102040810 >> 56;
	index_v = FLIPPED[3][index_v];
	flipped = D2D7[index_v];

	index_h = OUTFLANK[3][(O >> 25) & 0x3f] & (P >> 24);
	flipped |= ((unsigned long long) FLIPPED[3][index_h]) << 25;

	index_d7 = OUTFLANK[3][(O & 0x0000020408102000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0001020408102040ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[3][index_d7];
	flipped |= F2B6[index_d7];

	index_d9 = OUTFLANK[3][(O & 0x0040201008040200ULL)* 0x0101010101010101ULL >> 57] & (P & 0x8040201008040201ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[3][index_d9];
	flipped |= B2G7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square E4.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_E4(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[3][(O & 0x0010101010101000) * 0x0000204081020400 >> 57] & (P & 0x1010101010101010) * 0x0010204081020408 >> 56;
	index_v = FLIPPED[3][index_v];
	flipped = E2E7[index_v];

	index_h = OUTFLANK[4][(O >> 25) & 0x3f] & (P >> 24);
	flipped |= ((unsigned long long) FLIPPED[4][index_h]) << 25;

	index_d7 = OUTFLANK[4][(O & 0x0002040810204000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0102040810204080ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[4][index_d7];
	flipped |= G2B7[index_d7];

	index_d9 = OUTFLANK[4][(O & 0x0000402010080400ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0080402010080402ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[4][index_d9];
	flipped |= C2G6[index_d9 >> 1];


	return flipped;
}

/**
 * Compute flipped discs when playing on square F4.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_F4(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[3][(O & 0x0020202020202000) * 0x0000102040810200 >> 57] & (P & 0x2020202020202020) * 0x0008102040810204 >> 56;
	index_v = FLIPPED[3][index_v];
	flipped = F2F7[index_v];

	index_h = OUTFLANK[5][(O >> 25) & 0x3f] & (P >> 24);
	flipped |= ((unsigned long long) FLIPPED[5][index_h]) << 25;

	index_d7 = OUTFLANK[5][(O & 0x0004081020400000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0204081020408000ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[5][index_d7];
	flipped |= G3C7[index_d7 >> 1];

	index_d9 = OUTFLANK[5][(O & 0x0000004020100800ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0000804020100804ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[5][index_d9];
	flipped |= D2G5[index_d9 >> 2];


	return flipped;
}

/**
 * Compute flipped discs when playing on square G4.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_G4(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[3][(O & 0x0040404040404000) * 0x0000081020408100 >> 57] & (P & 0x4040404040404040) * 0x0004081020408102 >> 56;
	index_v = FLIPPED[3][index_v];
	flipped = G2G7[index_v];

	index_h = OUTFLANK[6][(O >> 25) & 0x3f] & (P >> 24);
	flipped |= ((unsigned long long) FLIPPED[6][index_h]) << 25;

	index_d = OUTFLANK[3][(((O & 0x0008102040201000ULL) + 0x0078706040607000ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x0408102040201008ULL) + 0x7c78706040607078ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[3][index_d];
	flipped |= E2G4D7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square H4.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_H4(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[3][(O & 0x0080808080808000) * 0x0000040810204080 >> 57] & (P & 0x8080808080808080) * 0x0002040810204081 >> 56;
	index_v = FLIPPED[3][index_v];
	flipped = H2H7[index_v];

	index_h = OUTFLANK[7][(O >> 25) & 0x3f] & (P >> 24);
	flipped |= ((unsigned long long) FLIPPED[7][index_h]) << 25;

	index_d = OUTFLANK[3][(((O & 0x0010204080402000ULL) + 0x0070604000406000ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x0810204080402010ULL) + 0x7870604000406070ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[3][index_d];
	flipped |= F2H4E7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square A5.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_A5(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[4][(O & 0x0001010101010100) * 0x0002040810204000 >> 57] & (P & 0x0101010101010101) * 0x0102040810204080 >> 56;
	index_v = FLIPPED[4][index_v];
	flipped = A2A7[index_v];

	index_h = OUTFLANK[0][(O >> 33) & 0x3f] & (P >> 32);
	flipped |= ((unsigned long long) FLIPPED[0][index_h]) << 33;

	index_d = OUTFLANK[4][(((O & 0x0004020002040800ULL) + 0x007c7e007e7c7800ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x0804020102040810ULL) + 0x787c7e7f7e7c7870ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[4][index_d];
	flipped |= D2A5C7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square B5.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_B5(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[4][(O & 0x0002020202020200) * 0x0001020408102000 >> 57] & (P & 0x0202020202020202) * 0x0081020408102040 >> 56;
	index_v = FLIPPED[4][index_v];
	flipped = B2B7[index_v];

	index_h = OUTFLANK[1][(O >> 33) & 0x3f] & (P >> 32);
	flipped |= ((unsigned long long) FLIPPED[1][index_h]) << 33;

	index_d = OUTFLANK[4][(((O & 0x0008040204081000ULL) + 0x00787c7e7c787000ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x1008040204081020ULL) + 0x70787c7e7c787060ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[4][index_d];
	flipped |= E2B5D7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square C5.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_C5(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[4][(O & 0x0004040404040400) * 0x0000810204081000 >> 57] & (P & 0x0404040404040404) * 0x0040810204081020 >> 56;
	index_v = FLIPPED[4][index_v];
	flipped = C2C7[index_v];

	index_h = OUTFLANK[2][(O >> 33) & 0x3f] & (P >> 32);
	flipped |= ((unsigned long long) FLIPPED[2][index_h]) << 33;

	index_d7 = OUTFLANK[2][(O & 0x0000020408102000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0001020408102040ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[2][index_d7];
	flipped |= F2B6[index_d7];

	index_d9 = OUTFLANK[2][(O & 0x0010080402000000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x2010080402010000ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[2][index_d9];
	flipped |= B4E7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square D5.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_D5(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[4][(O & 0x0008080808080800) * 0x0000408102040800 >> 57] & (P & 0x0808080808080808) * 0x0020408102040810 >> 56;
	index_v = FLIPPED[4][index_v];
	flipped = D2D7[index_v];

	index_h = OUTFLANK[3][(O >> 33) & 0x3f] & (P >> 32);
	flipped |= ((unsigned long long) FLIPPED[3][index_h]) << 33;

	index_d7 = OUTFLANK[3][(O & 0x0002040810204000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0102040810204080ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[3][index_d7];
	flipped |= G2B7[index_d7];

	index_d9 = OUTFLANK[3][(O & 0x0020100804020000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x4020100804020100ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[3][index_d9];
	flipped |= B3F7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square E5.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_E5(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[4][(O & 0x0010101010101000) * 0x0000204081020400 >> 57] & (P & 0x1010101010101010) * 0x0010204081020408 >> 56;
	index_v = FLIPPED[4][index_v];
	flipped = E2E7[index_v];

	index_h = OUTFLANK[4][(O >> 33) & 0x3f] & (P >> 32);
	flipped |= ((unsigned long long) FLIPPED[4][index_h]) << 33;

	index_d7 = OUTFLANK[4][(O & 0x0004081020400000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0204081020408000ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[4][index_d7];
	flipped |= G3C7[index_d7 >> 1];

	index_d9 = OUTFLANK[4][(O & 0x0040201008040200ULL)* 0x0101010101010101ULL >> 57] & (P & 0x8040201008040201ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[4][index_d9];
	flipped |= B2G7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square F5.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_F5(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[4][(O & 0x0020202020202000) * 0x0000102040810200 >> 57] & (P & 0x2020202020202020) * 0x0008102040810204 >> 56;
	index_v = FLIPPED[4][index_v];
	flipped = F2F7[index_v];

	index_h = OUTFLANK[5][(O >> 33) & 0x3f] & (P >> 32);
	flipped |= ((unsigned long long) FLIPPED[5][index_h]) << 33;

	index_d7 = OUTFLANK[5][(O & 0x0008102040000000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0408102040800000ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[5][index_d7];
	flipped |= G4D7[index_d7 >> 2];

	index_d9 = OUTFLANK[5][(O & 0x0000402010080400ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0080402010080402ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[5][index_d9];
	flipped |= C2G6[index_d9 >> 1];


	return flipped;
}

/**
 * Compute flipped discs when playing on square G5.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_G5(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[4][(O & 0x0040404040404000) * 0x0000081020408100 >> 57] & (P & 0x4040404040404040) * 0x0004081020408102 >> 56;
	index_v = FLIPPED[4][index_v];
	flipped = G2G7[index_v];

	index_h = OUTFLANK[6][(O >> 33) & 0x3f] & (P >> 32);
	flipped |= ((unsigned long long) FLIPPED[6][index_h]) << 33;

	index_d = OUTFLANK[4][(((O & 0x0010204020100800ULL) + 0x0070604060707800ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x0810204020100804ULL) + 0x787060406070787cULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[4][index_d];
	flipped |= D2G5E7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square H5.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_H5(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[4][(O & 0x0080808080808000) * 0x0000040810204080 >> 57] & (P & 0x8080808080808080) * 0x0002040810204081 >> 56;
	index_v = FLIPPED[4][index_v];
	flipped = H2H7[index_v];

	index_h = OUTFLANK[7][(O >> 33) & 0x3f] & (P >> 32);
	flipped |= ((unsigned long long) FLIPPED[7][index_h]) << 33;

	index_d = OUTFLANK[4][(((O & 0x0020408040201000ULL) + 0x0060400040607000ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x1020408040201008ULL) + 0x7060400040607078ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[4][index_d];
	flipped |= E2H5F7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square A6.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_A6(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[5][(O & 0x0001010101010100) * 0x0002040810204000 >> 57] & (P & 0x0101010101010101) * 0x0102040810204080 >> 56;
	index_v = FLIPPED[5][index_v];
	flipped = A2A7[index_v];

	index_h = OUTFLANK[0][(O >> 41) & 0x3f] & (P >> 40);
	flipped |= ((unsigned long long) FLIPPED[0][index_h]) << 41;

	index_d = OUTFLANK[5][(((O & 0x0002000204081000ULL) + 0x007e007e7c787000ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x0402010204081020ULL) + 0x7c7e7f7e7c787060ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[5][index_d];
	flipped |= E2A6B7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square B6.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_B6(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[5][(O & 0x0002020202020200) * 0x0001020408102000 >> 57] & (P & 0x0202020202020202) * 0x0081020408102040 >> 56;
	index_v = FLIPPED[5][index_v];
	flipped = B2B7[index_v];

	index_h = OUTFLANK[1][(O >> 41) & 0x3f] & (P >> 40);
	flipped |= ((unsigned long long) FLIPPED[1][index_h]) << 41;

	index_d = OUTFLANK[5][(((O & 0x0004020408102000ULL) + 0x007c7e7c78706000ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x0804020408102040ULL) + 0x787c7e7c78706040ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[5][index_d];
	flipped |= F2B6C7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square C6.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_C6(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7;
	register unsigned long long flipped;

	index_v = OUTFLANK[5][(O & 0x0004040404040400) * 0x0000810204081000 >> 57] & (P & 0x0404040404040404) * 0x0040810204081020 >> 56;
	index_v = FLIPPED[5][index_v];
	flipped = C2C7[index_v];

	index_h = OUTFLANK[2][(O >> 41) & 0x3f] & (P >> 40);
	flipped |= ((unsigned long long) FLIPPED[2][index_h]) << 41;

	index_d7 = OUTFLANK[2][(O & 0x0002040810204000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0102040810204080ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[2][index_d7];
	flipped |= G2B7[index_d7];

	flipped |= ((P >> 9) & 0x0008000000000000ULL & O) | ((P << 9) & 0x0000000200000000ULL & O);


	return flipped;
}

/**
 * Compute flipped discs when playing on square D6.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_D6(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[5][(O & 0x0008080808080800) * 0x0000408102040800 >> 57] & (P & 0x0808080808080808) * 0x0020408102040810 >> 56;
	index_v = FLIPPED[5][index_v];
	flipped = D2D7[index_v];

	index_h = OUTFLANK[3][(O >> 41) & 0x3f] & (P >> 40);
	flipped |= ((unsigned long long) FLIPPED[3][index_h]) << 41;

	index_d7 = OUTFLANK[3][(O & 0x0004081020400000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0204081020408000ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[3][index_d7];
	flipped |= G3C7[index_d7 >> 1];

	index_d9 = OUTFLANK[3][(O & 0x0010080402000000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x2010080402010000ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[3][index_d9];
	flipped |= B4E7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square E6.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_E6(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[5][(O & 0x0010101010101000) * 0x0000204081020400 >> 57] & (P & 0x1010101010101010) * 0x0010204081020408 >> 56;
	index_v = FLIPPED[5][index_v];
	flipped = E2E7[index_v];

	index_h = OUTFLANK[4][(O >> 41) & 0x3f] & (P >> 40);
	flipped |= ((unsigned long long) FLIPPED[4][index_h]) << 41;

	index_d7 = OUTFLANK[4][(O & 0x0008102040000000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0408102040800000ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[4][index_d7];
	flipped |= G4D7[index_d7 >> 2];

	index_d9 = OUTFLANK[4][(O & 0x0020100804020000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x4020100804020100ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[4][index_d9];
	flipped |= B3F7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square F6.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_F6(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[5][(O & 0x0020202020202000) * 0x0000102040810200 >> 57] & (P & 0x2020202020202020) * 0x0008102040810204 >> 56;
	index_v = FLIPPED[5][index_v];
	flipped = F2F7[index_v];

	index_h = OUTFLANK[5][(O >> 41) & 0x3f] & (P >> 40);
	flipped |= ((unsigned long long) FLIPPED[5][index_h]) << 41;

	flipped |= ((P >> 7) & 0x0010000000000000ULL & O) | ((P << 7) & 0x0000004000000000ULL & O);

	index_d9 = OUTFLANK[5][(O & 0x0040201008040200ULL)* 0x0101010101010101ULL >> 57] & (P & 0x8040201008040201ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[5][index_d9];
	flipped |= B2G7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square G6.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_G6(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[5][(O & 0x0040404040404000) * 0x0000081020408100 >> 57] & (P & 0x4040404040404040) * 0x0004081020408102 >> 56;
	index_v = FLIPPED[5][index_v];
	flipped = G2G7[index_v];

	index_h = OUTFLANK[6][(O >> 41) & 0x3f] & (P >> 40);
	flipped |= ((unsigned long long) FLIPPED[6][index_h]) << 41;

	index_d = OUTFLANK[5][(((O & 0x0020402010080400ULL) + 0x0060406070787c00ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x1020402010080402ULL) + 0x7060406070787c7eULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[5][index_d];
	flipped |= C2G6F7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square H6.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_H6(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[5][(O & 0x0080808080808000) * 0x0000040810204080 >> 57] & (P & 0x8080808080808080) * 0x0002040810204081 >> 56;
	index_v = FLIPPED[5][index_v];
	flipped = H2H7[index_v];

	index_h = OUTFLANK[7][(O >> 41) & 0x3f] & (P >> 40);
	flipped |= ((unsigned long long) FLIPPED[7][index_h]) << 41;

	index_d = OUTFLANK[5][(((O & 0x0040804020100800ULL) + 0x0040004060707800ULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 57] & (((P & 0x2040804020100804ULL) + 0x604000406070787cULL) & 0x8080808080808080ULL) * 0x0002040810204081ULL >> 56;
	index_d = FLIPPED[5][index_d];
	flipped |= D2H6G7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square A7.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_A7(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7;
	register unsigned long long flipped;

	index_v = OUTFLANK[6][(O & 0x0001010101010100) * 0x0002040810204000 >> 57] & (P & 0x0101010101010101) * 0x0102040810204080 >> 56;
	index_v = FLIPPED[6][index_v];
	flipped = A2A7[index_v];

	index_h = OUTFLANK[0][(O >> 49) & 0x3f] & (P >> 48);
	flipped |= ((unsigned long long) FLIPPED[0][index_h]) << 49;

	index_d7 = OUTFLANK[0][(O & 0x0000020408102000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0001020408102040ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[0][index_d7];
	flipped |= F2B6[index_d7];


	return flipped;
}

/**
 * Compute flipped discs when playing on square B7.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_B7(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7;
	register unsigned long long flipped;

	index_v = OUTFLANK[6][(O & 0x0002020202020200) * 0x0001020408102000 >> 57] & (P & 0x0202020202020202) * 0x0081020408102040 >> 56;
	index_v = FLIPPED[6][index_v];
	flipped = B2B7[index_v];

	index_h = OUTFLANK[1][(O >> 49) & 0x3f] & (P >> 48);
	flipped |= ((unsigned long long) FLIPPED[1][index_h]) << 49;

	index_d7 = OUTFLANK[1][(O & 0x0002040810204000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0102040810204080ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[1][index_d7];
	flipped |= G2B7[index_d7];


	return flipped;
}

/**
 * Compute flipped discs when playing on square C7.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_C7(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[6][(O & 0x0004040404040400) * 0x0000810204081000 >> 57] & (P & 0x0404040404040404) * 0x0040810204081020 >> 56;
	index_v = FLIPPED[6][index_v];
	flipped = C2C7[index_v];

	index_h = OUTFLANK[2][(O >> 49) & 0x3f] & (P >> 48);
	flipped |= ((unsigned long long) FLIPPED[2][index_h]) << 49;

	index_d = OUTFLANK[2][(O & 0x00040a1020400000ULL) * 0x0101010101010101ULL >> 57] & (P & 0x00040a1120408000ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[2][index_d];
	flipped |= B6C7G3[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square D7.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_D7(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[6][(O & 0x0008080808080800) * 0x0000408102040800 >> 57] & (P & 0x0808080808080808) * 0x0020408102040810 >> 56;
	index_v = FLIPPED[6][index_v];
	flipped = D2D7[index_v];

	index_h = OUTFLANK[3][(O >> 49) & 0x3f] & (P >> 48);
	flipped |= ((unsigned long long) FLIPPED[3][index_h]) << 49;

	index_d = OUTFLANK[3][(O & 0x0008142240000000ULL) * 0x0101010101010101ULL >> 57] & (P & 0x0008142241800000ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[3][index_d];
	flipped |= B5D7G4[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square E7.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_E7(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[6][(O & 0x0010101010101000) * 0x0000204081020400 >> 57] & (P & 0x1010101010101010) * 0x0010204081020408 >> 56;
	index_v = FLIPPED[6][index_v];
	flipped = E2E7[index_v];

	index_h = OUTFLANK[4][(O >> 49) & 0x3f] & (P >> 48);
	flipped |= ((unsigned long long) FLIPPED[4][index_h]) << 49;

	index_d = OUTFLANK[4][(O & 0x0010284402000000ULL) * 0x0101010101010101ULL >> 57] & (P & 0x0010284482010000ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[4][index_d];
	flipped |= B4E7G5[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square F7.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_F7(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[6][(O & 0x0020202020202000) * 0x0000102040810200 >> 57] & (P & 0x2020202020202020) * 0x0008102040810204 >> 56;
	index_v = FLIPPED[6][index_v];
	flipped = F2F7[index_v];

	index_h = OUTFLANK[5][(O >> 49) & 0x3f] & (P >> 48);
	flipped |= ((unsigned long long) FLIPPED[5][index_h]) << 49;

	index_d = OUTFLANK[5][(O & 0x0020500804020000ULL) * 0x0101010101010101ULL >> 57] & (P & 0x0020508804020100ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[5][index_d];
	flipped |= B3F7G6[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square G7.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_G7(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[6][(O & 0x0040404040404000) * 0x0000081020408100 >> 57] & (P & 0x4040404040404040) * 0x0004081020408102 >> 56;
	index_v = FLIPPED[6][index_v];
	flipped = G2G7[index_v];

	index_h = OUTFLANK[6][(O >> 49) & 0x3f] & (P >> 48);
	flipped |= ((unsigned long long) FLIPPED[6][index_h]) << 49;

	index_d9 = OUTFLANK[6][(O & 0x0040201008040200ULL)* 0x0101010101010101ULL >> 57] & (P & 0x8040201008040201ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[6][index_d9];
	flipped |= B2G7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square H7.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_H7(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[6][(O & 0x0080808080808000) * 0x0000040810204080 >> 57] & (P & 0x8080808080808080) * 0x0002040810204081 >> 56;
	index_v = FLIPPED[6][index_v];
	flipped = H2H7[index_v];

	index_h = OUTFLANK[7][(O >> 49) & 0x3f] & (P >> 48);
	flipped |= ((unsigned long long) FLIPPED[7][index_h]) << 49;

	index_d9 = OUTFLANK[7][(O & 0x0080402010080400ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0080402010080402ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[7][index_d9];
	flipped |= C2G6[index_d9 >> 1];


	return flipped;
}

/**
 * Compute flipped discs when playing on square A8.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_A8(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7;
	register unsigned long long flipped;

	index_v = OUTFLANK[7][(O & 0x0001010101010100) * 0x0002040810204000 >> 57] & (P & 0x0101010101010101) * 0x0102040810204080 >> 56;
	index_v = FLIPPED[7][index_v];
	flipped = A2A7[index_v];

	index_h = OUTFLANK[0][(O >> 57) & 0x3f] & (P >> 56);
	flipped |= ((unsigned long long) FLIPPED[0][index_h]) << 57;

	index_d7 = OUTFLANK[0][(O & 0x0002040810204000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0102040810204080ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[0][index_d7];
	flipped |= G2B7[index_d7];


	return flipped;
}

/**
 * Compute flipped discs when playing on square B8.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_B8(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d7;
	register unsigned long long flipped;

	index_v = OUTFLANK[7][(O & 0x0002020202020200) * 0x0001020408102000 >> 57] & (P & 0x0202020202020202) * 0x0081020408102040 >> 56;
	index_v = FLIPPED[7][index_v];
	flipped = B2B7[index_v];

	index_h = OUTFLANK[1][(O >> 57) & 0x3f] & (P >> 56);
	flipped |= ((unsigned long long) FLIPPED[1][index_h]) << 57;

	index_d7 = OUTFLANK[1][(O & 0x0004081020400000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x0204081020408000ULL) * 0x0101010101010101ULL >> 56;
	index_d7 = FLIPPED[1][index_d7];
	flipped |= G3C7[index_d7 >> 1];


	return flipped;
}

/**
 * Compute flipped discs when playing on square C8.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_C8(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[7][(O & 0x0004040404040400) * 0x0000810204081000 >> 57] & (P & 0x0404040404040404) * 0x0040810204081020 >> 56;
	index_v = FLIPPED[7][index_v];
	flipped = C2C7[index_v];

	index_h = OUTFLANK[2][(O >> 57) & 0x3f] & (P >> 56);
	flipped |= ((unsigned long long) FLIPPED[2][index_h]) << 57;

	index_d = OUTFLANK[2][(O & 0x040a102040000000ULL) * 0x0101010101010101ULL >> 57] & (P & 0x040a112040800000ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[2][index_d];
	flipped |= B7C8G4[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square D8.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_D8(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[7][(O & 0x0008080808080800) * 0x0000408102040800 >> 57] & (P & 0x0808080808080808) * 0x0020408102040810 >> 56;
	index_v = FLIPPED[7][index_v];
	flipped = D2D7[index_v];

	index_h = OUTFLANK[3][(O >> 57) & 0x3f] & (P >> 56);
	flipped |= ((unsigned long long) FLIPPED[3][index_h]) << 57;

	index_d = OUTFLANK[3][(O & 0x0814224000000000ULL) * 0x0101010101010101ULL >> 57] & (P & 0x0814224180000000ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[3][index_d];
	flipped |= B6D8G5[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square E8.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_E8(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[7][(O & 0x0010101010101000) * 0x0000204081020400 >> 57] & (P & 0x1010101010101010) * 0x0010204081020408 >> 56;
	index_v = FLIPPED[7][index_v];
	flipped = E2E7[index_v];

	index_h = OUTFLANK[4][(O >> 57) & 0x3f] & (P >> 56);
	flipped |= ((unsigned long long) FLIPPED[4][index_h]) << 57;

	index_d = OUTFLANK[4][(O & 0x1028440200000000ULL) * 0x0101010101010101ULL >> 57] & (P & 0x1028448201000000ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[4][index_d];
	flipped |= B5E8G6[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square F8.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_F8(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d;
	register unsigned long long flipped;

	index_v = OUTFLANK[7][(O & 0x0020202020202000) * 0x0000102040810200 >> 57] & (P & 0x2020202020202020) * 0x0008102040810204 >> 56;
	index_v = FLIPPED[7][index_v];
	flipped = F2F7[index_v];

	index_h = OUTFLANK[5][(O >> 57) & 0x3f] & (P >> 56);
	flipped |= ((unsigned long long) FLIPPED[5][index_h]) << 57;

	index_d = OUTFLANK[5][(O & 0x2050080402000000ULL) * 0x0101010101010101ULL >> 57] & (P & 0x2050880402010000ULL) * 0x0101010101010101ULL >> 56;
	index_d = FLIPPED[5][index_d];
	flipped |= B4F8G7[index_d];


	return flipped;
}

/**
 * Compute flipped discs when playing on square G8.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_G8(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[7][(O & 0x0040404040404000) * 0x0000081020408100 >> 57] & (P & 0x4040404040404040) * 0x0004081020408102 >> 56;
	index_v = FLIPPED[7][index_v];
	flipped = G2G7[index_v];

	index_h = OUTFLANK[6][(O >> 57) & 0x3f] & (P >> 56);
	flipped |= ((unsigned long long) FLIPPED[6][index_h]) << 57;

	index_d9 = OUTFLANK[6][(O & 0x4020100804020000ULL)* 0x0101010101010101ULL >> 57] & (P & 0x4020100804020100ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[6][index_d9];
	flipped |= B3F7[index_d9];


	return flipped;
}

/**
 * Compute flipped discs when playing on square H8.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_H8(const unsigned long long P, const unsigned long long O)
{
	register int index_h, index_v, index_d9;
	register unsigned long long flipped;

	index_v = OUTFLANK[7][(O & 0x0080808080808000) * 0x0000040810204080 >> 57] & (P & 0x8080808080808080) * 0x0002040810204081 >> 56;
	index_v = FLIPPED[7][index_v];
	flipped = H2H7[index_v];

	index_h = OUTFLANK[7][(O >> 57) & 0x3f] & (P >> 56);
	flipped |= ((unsigned long long) FLIPPED[7][index_h]) << 57;

	index_d9 = OUTFLANK[7][(O & 0x8040201008040200ULL)* 0x0101010101010101ULL >> 57] & (P & 0x8040201008040201ULL) * 0x0101010101010101ULL >> 56;
	index_d9 = FLIPPED[7][index_d9];
	flipped |= B2G7[index_d9];


	return flipped;
}

/**
 * Compute (zero-) flipped discs when plassing.
 *
 * @param P player's disc pattern.
 * @param O opponent's disc pattern.
 * @return flipped disc pattern.
 */
unsigned long long flip_pass(const unsigned long long P, const unsigned long long O)
{
	(void) P; // useless code to shut-up compiler warning
	(void) O;
	return 0;
}


unsigned long long (*flip[])(const unsigned long long, const unsigned long long) = {
	flip_A1, flip_B1, flip_C1, flip_D1,
	flip_E1, flip_F1, flip_G1, flip_H1,
	flip_A2, flip_B2, flip_C2, flip_D2,
	flip_E2, flip_F2, flip_G2, flip_H2,
	flip_A3, flip_B3, flip_C3, flip_D3,
	flip_E3, flip_F3, flip_G3, flip_H3,
	flip_A4, flip_B4, flip_C4, flip_D4,
	flip_E4, flip_F4, flip_G4, flip_H4,
	flip_A5, flip_B5, flip_C5, flip_D5,
	flip_E5, flip_F5, flip_G5, flip_H5,
	flip_A6, flip_B6, flip_C6, flip_D6,
	flip_E6, flip_F6, flip_G6, flip_H6,
	flip_A7, flip_B7, flip_C7, flip_D7,
	flip_E7, flip_F7, flip_G7, flip_H7,
	flip_A8, flip_B8, flip_C8, flip_D8,
	flip_E8, flip_F8, flip_G8, flip_H8,
	flip_pass, flip_pass
};
/*
 * $File: hash.cc
 * $Date: Wed Jan 30 05:42:29 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/




HashData HashTable::HASH_DATA_INIT;
HashItem::HashItem()
{
	code = HASH_CODE_EMPTY;
}

void HashTable::setup()
{
	HASH_DATA_INIT.score = -SCORE_INF;
	HASH_DATA_INIT.date = 0;
	HASH_DATA_INIT.move[0] = HASH_DATA_INIT.move[1] = NOMOVE;
}

void HashTable::set_size(int size)
{
	this->size = 1;
	while (this->size < size)
		this->size <<= 1;
	if (this->size != size)
		this->size <<= 1;

	if (item)
		delete [] item;
	item = new HashItem[this->size + n_way + 2];
	hash_mask = this->size - 1;
}

void HashTable::set_n_way(int n_way)
{
	this->n_way = n_way;
}

HashTable::HashTable()
{
	item = NULL;
	size = 0;
	n_way = HASH_N_WAY_DEFAULT;

	inited = false;
}

HashData *HashTable::get(ull code)
{
	HashItem *hash = item + (code & hash_mask);
	for (int i = 0; i < n_way; i ++)
	{
		if (hash->code == code)
			return &hash->data;
		hash ++;
	}
	return NULL;
}

HashData *HashTable::get_newest(ull code)
{
	HashItem *hash = item + (code & hash_mask);
	for (int i = 0; i < n_way; i ++)
	{
		if (hash->code == code && hash->data.date == date)
			return &hash->data;
		hash ++;
	}
	return NULL;
}

bool HashTable::is_newest(const HashData *hash_data) const
{
	return hash_data->date == date;
}

void HashTable::store(ull code, HashData *hash_data)
{
	HashItem *hash = item + (code & hash_mask);
	for (int i = 0; i < n_way; i ++)
	{
		if (hash->update(code, hash_data))
			return;
		hash ++;
	}
}

void HashTable::force_store(ull code, HashData *hash_data)
{
	// TODO: find the most unimportant one
	HashItem *hash = item + (code & hash_mask);
	HashItem *worst = hash;
	for (int i = 0; i < n_way; i ++)
	{
		if (hash->upgrade(code, hash_data))
			return;
		if (worst->data.writeable_level() > hash->data.writeable_level())
			worst = hash;
		hash ++;
	}
	// may be a new board configuration
	// substitute the most unimportant one
	worst->replace(code, hash_data);

}

void HashTable::clear()
{
	if (date == 127)
		cleanup();
	date ++;
}

void HashTable::init()
{
	cleanup();
	inited = true;
}

void HashTable::cleanup()
{
	for (register int i = 0; i < size + n_way; i ++)
	{
		item[i].code = HASH_CODE_EMPTY;
		item[i].data = HASH_DATA_INIT;
	}
	date = 0;
}

unsigned int HashData::writeable_level() const
{
	return date;
}

bool HashItem::update(ull code, HashData *hash_data)
{
	if (this->code != 0 && code != this->code)
		return false;
	if (this->code == HASH_CODE_EMPTY)
		this->code = code;
	if (data.date == hash_data->date) // update
		data.update(hash_data);
	else data.upgrade(hash_data);
	return true;
}

bool HashItem::upgrade(ull code, HashData *hash_data)
{
	if (this->code != 0 && code != this->code)
		return false;
	if (this->code == HASH_CODE_EMPTY)
		this->code = code;
	data.upgrade(hash_data);
	return true;
}

void HashItem::replace(ull code, HashData *hash_data)
{
	this->code = code;
	*hash_data = data;
}


void HashData::update(HashData *hash_data)
{
	if (hash_data->score > score && move[0] != hash_data->move[0])
	{
		move[1] = move[0];
		move[0] = (unsigned char)hash_data->move[0];
		score = hash_data->score;
	}
}

void HashData::upgrade(HashData *hash_data)
{
	if (hash_data->score > score && move[0] != hash_data->move[0])
	{
		move[1] = move[0];
		move[0] = (unsigned char)hash_data->move[0];
	}
	date = hash_data->date;
	score = hash_data->score;
}


/*
 * $File: main.cc
 * $Date: Fri Feb 01 10:44:14 2013 +0800
 * $Author: Xinyu Zhou <zxytim[at]gmail[dot]com>
 */

#include <cstdio>





#include <cstring>

void test()
{
	Search search[1];
	search->setup();

	Board board[1];
	board->from_string("-------------O-------O-X---OOOX---XXXX-----O-------------------- X");

	bitboard_write(board->player, stderr);
	bitboard_write(board->opponent, stderr);
	board->print(BLACK);

	int p = search->get_best_move(*board);
	int x = p / 8,
		y = p % 8;
	printf("%d %d\n", x, y);
}

void wai()
{
	Search search[1];
	search->setup();
	Board board;
	board.init();

	char buf[16];
	scanf("%s", buf);
	int turn = (strcmp(buf, "first") != 0);

	int x, y;
	if (turn)
	{
		scanf("%d%d", &x, &y);
		board.update(x * 8 + y);
	}

	while (true)
	{
		int p = search->get_best_move(board);
		if (p == NOMOVE)
			x = -1, y = -1;
		else 
		{
			x = p / 8, y = p % 8;
			board.update(p);
		}
		board.do_print(turn, COORD_NUMBER, stderr);
		printf("%d %d\n", x, y);
		fflush(stdout);

		scanf("%d%d", &x, &y);
		if (x != -1)
		{
			p = x * 8 + y;
			ull moves = board.get_moves();
			if (!(x_to_bit(p) & moves))
				break;
			board.update(p);
		}
	}
}

int main()
{
	test();
	//wai();
	return 0;
}

/**
 * vim: syntax=cpp11 foldmethod=marker
 */

/*
 * $File: move.cc
 * $Date: Wed Jan 30 06:04:02 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/








const int SQUARE_VALUE[] = {
	// JCW's score:
	18,  4,  16, 12, 12, 16,  4, 18,
	 4,  2,   6,  8,  8,  6,  2,  4,
	16,  6,  14, 10, 10, 14,  6, 16,
	12,  8,  10,  0,  0, 10,  8, 12,
	12,  8,  10,  0,  0, 10,  8, 12,
	16,  6,  14, 10, 10, 14,  6, 16,
	 4,  2,   6,  8,  8,  6,  2,  4,
	18,  4,  16, 12, 12, 16,  4, 18
};

static int eval_by_presorted_x[64];

void Move::setup()
{
	for (int i = 0; i < 64; i ++)
		for (int j = 0; j < 64; j ++)
			if (i == presorted_x[j])
				eval_by_presorted_x[i] = 64 - j;
}

bool MoveList::is_empty() const
{
	return n_moves == 0;
}

Move *MoveList::first_move()
{
	return move->next;
}

Move *MoveList::best_move()
{
	return move->next_best();
}

void MoveList::evaluate(Search *search, HashData *hash_data, const int alpha, const int depth)
{
	int sort_depth,
		sort_alpha;
	int min_depth = 9; // minimum depth to enable search evaluate

	if (search->n_empties <= 27)
		min_depth += (30 - search->n_empties) / 3;

	if (depth >= min_depth)
	{
		sort_depth = (depth - 15) / 3;
		//if (hash_data && hash_data->upper < alpha); // may cut off quickly ?
		if (search->n_empties >= 27) // more precise in end game
			sort_depth ++;
		if (sort_depth < 0)
			sort_depth = 0;
		else if (sort_depth > 6)
			sort_depth = 0;
	}
	else
		sort_depth = -1;

	sort_alpha = MAX(SCORE_MIN, alpha - 8); // slightly decrease alpha
	foreach_move(move, this)
		move->evaluate(search, hash_data, sort_alpha, sort_depth);
}

void MoveList::sort()
{
	foreach_best_move(move, this);
}

void Move::evaluate(Search *search, HashData *hash_data, const int sort_alpha, const int sort_depth)
{
	const int w_wipeout = 1 << 30,
		  w_first_move = 1 << 29,
		  w_second_move = 1 << 28,
		  w_hash = 1 << 15,
		  w_eval = 1 << 15,
		  w_mobility = 1 << 15,
		  w_corner_stability = 1 << 11,
		  w_edge_stability = 1 << 11,
		  w_potential_mobility = 1 << 5,
		  w_presorted_x = 0;

	Board *board = search->board;
	if (wipeout(board))
		score = w_wipeout;
	else if (x == hash_data->move[0])
		score = w_first_move;
	else if (x == hash_data->move[1])
		score = w_second_move;
	else
	{
		score = SQUARE_VALUE[x];
		
		if (sort_depth < 0) // not far from opening
		{
			score += board->corner_stability() * w_corner_stability;
			board->update(this); // after this move
			{
				score += (36 - board->potential_mobility()) * w_potential_mobility; // the lesser the better for opponent
				score += (36 - board->weighted_mobility()) * w_mobility; // the lesser the better  for opponent
			}
			board->restore(this);
		}
		else
		{
			search->update(this); // after this move
			score += board->edge_stability() * w_edge_stability;
			{
				score += (36 - board->potential_mobility()) * w_potential_mobility; // the lesser the better for opponent
				score += (36 - board->weighted_mobility()) * w_mobility; // the lesser the better  for opponent
			}

			switch (sort_depth)
			{
				case 0:
					score += ((SCORE_MAX - search->eval_0()) >> 2) * w_eval;
					break;
				case 1:
				case 2:
				default:
					if (search->hash_table->get(board->get_hash_code()))
						score += w_hash; // hash bonus
					//score += (SCORE_MAX - search->do_search(SCORE_MIN, -sort_alpha, sort_depth)) * w_eval;
			}

			search->restore(this);
		}
	}
	//score += eval_by_presorted_x[x] * w_presorted_x;
}

bool Move::wipeout(Board *board) const
{
	return board->opponent == flipped;
}

Move *Move::next_best()
{
	if (this->next)
	{
		Move *move,
			 *best = this; // the previous one of the best
		for (move = best->next; move->next; move = move->next)
			if (move->next->score > best->next->score)
				best = move;
		if (best != this)
		{
			move = best->next;
			best->next = move->next;
			move->next = this->next;
			this->next = move;
		}
	}
	return this->next;
}
/*
 * $File: random.cc
 * $Date: Tue Jan 29 00:15:35 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/



static const ull MASK48 = 0XFFFFFFFFFFFFull;

void Random::set_seed(ull seed)
{
	this->seed = seed & MASK48;
}

ull Random::get()
{
	const ull A = 0x5DEECE66Dull;
	const ull B = 0xBull;
	register ull r;
	seed = ((A * seed + B) & MASK48);
	r = seed >> 16;
	seed = ((A * seed + B) & MASK48);
	return (r << 32) | (seed >> 16);
}

Random::Random()
{
	seed = 0x5DEECE66Dull;
}

/*
 * $File: search.cc
 * $Date: Fri Feb 01 18:32:14 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/








#include <cmath>

void Search::setup()
{
	HashTable::setup();
	Board::setup();
	Move::setup();
	Eval::setup();
}

void Search::init(Board board)
{
	*this->board = board;
	enable_probcut = true;
	eval->set(this->board);
}

int Search::get_best_move(Board _board)
{
	init(_board);
	hash_table->set_size(HASH_SIZE);
	hash_table->init();
	n_empties = bit_count(~(board->player | board->opponent));

	stats.start();

	int search_depth = 12;
	if (USE_ITERATIVE_DEEPENING)
		iterative_deepening(SCORE_MIN, SCORE_MAX, search_depth);
	else
		do_search(SCORE_MIN, SCORE_MAX, search_depth, true);

	stats.stop();
	stats.print();

	ull hash_code = board->get_hash_code();
	HashData *hash_data = hash_table->get(hash_code);

	return hash_data->move[0];
}

int Search::do_nws_search(int alpha, int depth)
{
	SEARCH_STATS(stats.n_nws_node ++);
	SEARCH_STATS(stats.n_nws_node_at_depth[depth] ++);

	const int beta = alpha + 1;
	ull hash_code = board->get_hash_code();
	HashData *rst, // result
			 hash_init = HashTable::HASH_DATA_INIT;
	MoveList movelist[1];

	do
	{
		if (n_empties == 0)
			return solve_0();

		if (depth <= 0)
			return eval_0();

		rst = &hash_init;
		rst->score = SCORE_MIN;

		gen_movelist(movelist);

		/* special cases */
		if (movelist->is_empty())
		{
			if (board->can_opponent_move()) // pass
			{
				nws_pass_update();
				{
					rst->score = -do_nws_search(-beta, depth);
					rst->move[0] = PASS;
					rst->move[1] = NOMOVE;
				}
				nws_pass_restore();
			}
			else // game over
			{
				rst->score = solve_game_over();
				rst->move[0] = rst->move[1] = NOMOVE;
			}
			break;
		}

		if (!(rst = hash_table->get_newest(hash_code)))
			rst = &hash_init;
		else break;

		/* normal search */

		int score;
		
		// evaluate and sort moves for better ordering
		movelist->evaluate(this, rst, alpha, depth);
		movelist->sort();

		// multicut after edge sort
		if (multicut(alpha, depth, movelist, score))
			return score;
		// probcut
		if (probcut(alpha, beta, depth, score))
			return score;

		int nmove = 0;
		foreach_move(move, movelist)
		{
			nmove ++;
			nws_update(move);
			{
				move->score = -do_nws_search(-beta, depth - 1);
			}
			nws_restore(move);
			if (move->score > rst->score)
			{
				rst->score = move->score;
				rst->move[0] = move->x;
			}
			alpha = rst->score;
			if (alpha >= beta)
			{
				//SEARCH_STATS(stats.n_alpha_beta_cutoff ++);
				//SEARCH_STATS(stats.n_ab_cutoff_at_depth[depth] ++);
				break;
			}
		}

	} while (0);

	hash_table->store(hash_code, rst);

	return rst->score;
}

int Search::do_search(int alpha, int beta, int depth, bool force_store)
{
	SEARCH_STATS(stats.n_node ++);
	SEARCH_STATS(stats.n_node_at_depth[depth] ++);
	
	assert(n_empties == bit_count(~(board->player | board->opponent)));

	assert(alpha <= beta);
	assert(SCORE_MIN <= alpha && alpha <= SCORE_MAX);
	assert(SCORE_MIN <= beta && beta <= SCORE_MAX);


	ull hash_code = board->get_hash_code();
	HashData *rst, // result
			 hash_init = HashTable::HASH_DATA_INIT;
	MoveList movelist[1];

	do
	{
		if (n_empties == 0)
			return solve_0();

		if (depth <= 0)
			return eval_0();

		rst = &hash_init;
		rst->score = SCORE_MIN;

		gen_movelist(movelist);

		/* special cases */
		if (movelist->is_empty())
		{
			if (board->can_opponent_move()) // pass
			{
				pass_update();
				{
					rst->score = -do_search(-beta, -alpha, depth);
					rst->move[0] = PASS;
					rst->move[1] = NOMOVE;
				}
				pass_restore();
			}
			else // game over
			{
				rst->score = solve_game_over();
				rst->move[0] = rst->move[1] = NOMOVE;
			}
			break;
		}

		/* normal search */
		
#if 0
		int score;
		if (probcut(alpha, beta, depth, score))
			return score;
#endif

		if (!(rst = hash_table->get(hash_code)))
			rst = &hash_init;
		// evaluate and sort moves for better ordering
		movelist->evaluate(this, rst, alpha, depth);
		movelist->sort();

		int nmove = 0;
		foreach_move(move, movelist)
		{
			nmove ++;
			update(move);
			{
				if (nmove > 1 && USE_NWS)
				{
					SEARCH_STATS(stats.n_nws_tried ++);
					move->score = -do_nws_search(-beta, depth - 1);
					if (alpha < move->score && move->score < beta)
					{
						SEARCH_STATS(stats.n_nws_fail ++);
						move->score = -do_search(-beta, -alpha, depth - 1);
					}
				}
				else
					move->score = -do_search(-beta, -alpha, depth - 1);
			}
			restore(move);
			if (move->score > rst->score)
			{
				rst->score = move->score;
				rst->move[0] = move->x;
			}
			alpha = rst->score;
			if (alpha >= beta)
			{
				SEARCH_STATS(stats.n_alpha_beta_cutoff ++);
				SEARCH_STATS(stats.n_ab_cutoff_at_depth[depth] ++);
				break;
			}
		}

	} while (0);

	if (force_store)
	{
		hash_table->force_store(hash_code, rst);
		assert(movelist->first_move()->x == rst->move[0]);
		foreach_move(move, movelist)
		{
			fprintf(stderr, "(%d, %d) -> %d\n", move->x / 8, move->x % 8, move->score);
		}
	}
	else
	   	hash_table->store(hash_code, rst);
	return rst->score;
}


void Search::iterative_deepening(int alpha, int beta, int depth)
{
	for (int i = 1; i < depth; i ++)
	{
		hash_table->clear();
		//do_nws_search(alpha, i);
		do_search(alpha, beta, i);
	}
	hash_table->clear();
	//do_nws_search(alpha, depth);
	do_search(alpha, beta, depth, true);
}

void Search::gen_movelist(MoveList *movelist)
{
	Move *prev = movelist->move;
	Move *move = prev + 1;

	ull moves = board->get_moves();
	register int x;

	foreach_bit(x, moves)
	{
		board->gen_move(x, move);
		prev = prev->next = move;
		move ++;
	}
	prev->next = NULL;
	movelist->n_moves = move - movelist->move - 1;
	assert(movelist->n_moves == board->mobility());
}


void Search::update(Move *move)
{
	board->update(move);
	height ++;
	n_empties --;
}

void Search::restore(Move *move)
{
	board->restore(move);
	height --;
	n_empties ++;
}

void Search::nws_update(Move *move)
{
	board->update(move);
	height ++;
	n_empties --;
}

void Search::nws_restore(Move *move)
{
	board->restore(move);
	height --;
	n_empties ++;
}


void Search::pass_update()
{
	board->pass_update();
}

void Search::pass_restore()
{
	board->pass_restore();
}

void Search::nws_pass_update()
{
	board->pass_update();
}

void Search::nws_pass_restore()
{
	board->pass_restore();
}

int Search::solve_0()
{
	SEARCH_STATS(stats.n_solve_0 ++);
	return 2 * bit_count(board->player); // - SCORE_MAX;
}

int Search::solve_game_over()
{
	SEARCH_STATS(stats.n_solve_game_over ++);

	const int n_discs_p = bit_count(board->player);
	const int n_discs_o = 64 - n_empties - n_discs_p;

	register int score = n_discs_p - n_discs_o;

	if (score < 0)
		score -= n_empties;
	else if (score > 0)
		score += n_empties;

	return score;
}

int Search::eval_0()
{
	// XXX
	return solve_0();
}

bool Search::probcut(int alpha, int beta, int depth, int &score)
{
	if (!USE_PROBCUT)
		return false;
	if (!enable_probcut)
		return false;
	
	int probcut_depth = 2 * floor(depth * 0.25) + (depth & 1);
	if (probcut_depth == 0)
		probcut_depth = depth - 4;
	probcut_depth = depth - 4;
	
	int eval_score = eval_0();
	
	int eval_error = 0;
	int eval_alpha = alpha + eval_error,
		eval_beta = beta - eval_error;

	int probcut_error = 1.5;
	int probcut_alpha, probcut_beta;

	probcut_beta = beta + probcut_error;
	probcut_alpha = probcut_beta - 1;
	if (eval_score >= eval_beta && probcut_beta < SCORE_MAX)
	{
		this->enable_probcut = false;
		score = do_nws_search(probcut_alpha, probcut_depth);
		this->enable_probcut = true;
		if (score >= probcut_beta)
		{
			score = beta;
			SEARCH_STATS(stats.n_probcut_high_cutoff ++);
			return true;
		}
	}

	probcut_alpha = alpha - probcut_error;
	if (eval_score < eval_alpha && probcut_alpha > SCORE_MIN)
	{
		this->enable_probcut = false;
		score = do_nws_search(probcut_alpha, probcut_depth);
		this->enable_probcut = true;

		if (score <= probcut_alpha)
		{
			score = alpha;
			SEARCH_STATS(stats.n_probcut_low_cutoff ++);
			return true;
		}
	}
	return false;
}

bool Search::multicut(int alpha, int depth, MoveList *movelist, int &score)
{
	if (!USE_MULTICUT)
		return false;
	if (depth - 1 < MULTICUT_REDUCED_DEPTH)
		return false;

	// cut after edge sorted
	if (USE_MULTICUT && depth >= MULTICUT_REDUCED_DEPTH + 1)
	{
		SEARCH_STATS(stats.n_multicut_tried ++);
		int beta = alpha + 1;
		int nmove_remain = MULTICUT_MOVES_INSPECT;
		int ncut = MULTICUT_CUT_THRESHOLD;
		score = SCORE_MIN;
		int sc;
		foreach_move(move, movelist)
		{
			if ((nmove_remain --) == 0)
				break;
			nws_update(move);
			sc = -do_nws_search(-beta, depth - 1 - MULTICUT_REDUCED_DEPTH);
			nws_restore(move);
			if (sc > score)
			{
				score = sc;
				if (score >= beta)
				{
					ncut --; 
					if (ncut == 0)
					{
						SEARCH_STATS(stats.n_multicut_succeed ++);
						return true;
					}
				}
			}
		}
	}
	return false;
}

/*
 * $File: stats.cc
 * $Date: Fri Feb 01 11:40:23 2013 +0800
 * $Author: Zhou Xinyu <zxytim[at]gmail[dot]com>
 */
/*
   Copyright (C) <2013>  Zhou Xinyu <zxytim[at]gmail[dot]com>
   
   This file is part of Tothello

   Tothello is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Tothello is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Tothello.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <sys/time.h>
#include <cstdio>
#include <cstring>
#include <cmath>

static ull get_time()
{
	timeval tv;
	gettimeofday(&tv, 0);
	return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

void Statistics::start()
{
	time = get_time();
	memset(n_node_at_depth, 0, sizeof(n_node_at_depth));
	memset(n_nws_node_at_depth, 0, sizeof(n_nws_node_at_depth));

	memset(n_ab_cutoff_at_depth, 0, sizeof(n_ab_cutoff_at_depth));
	n_solve_0 = 0;
	n_solve_game_over = 0;
	n_node = 0;
	n_alpha_beta_cutoff = 0;

	n_transpositional_cutoff = 0;

	n_nws_node = 0;
	n_nws_fail = 0;
	n_nws_tried = 0;

	n_probcut_high_cutoff = 0;
	n_probcut_low_cutoff = 0;

	n_multicut_tried = 0;
	n_multicut_succeed = 0;
}

void Statistics::stop()
{
	time = get_time() - time;
}

void Statistics::print()
{
	int n_node_tot = n_node + n_nws_node;

#define INSPECT(key) \
	fprintf(stderr, #key ": %d\n", key);
	fprintf(stderr, "normal nodes : %d, %.2lf%%\n", n_node, n_node / (double)n_node_tot * 100);
	fprintf(stderr, "nws nodes: %d, %.2lf%%\n", n_nws_node, n_nws_node / (double)n_node_tot * 100);
	fprintf(stderr, "node total: %d\n", n_node_tot);
	fprintf(stderr, "time used: %llums\n", time);
	fprintf(stderr, "node per sec: %.2lf\n", n_node_tot / (time / 1000.0));
	int depth = 0; 
	for (int i = 0; n_node_at_depth[i]; i ++)
	{
		depth ++;
		//fprintf(stderr, "normal node at depth %d: %d, %.2lf%%\n", i, n_node_at_depth[i], n_node_at_depth[i] / (double)n_node * 100);
	}

#if 0
	for (int i = 0; i < depth; i ++)
	{
		fprintf(stderr, "ab-cut at depth %d: %d, %.2lf%%\n", i, n_ab_cutoff_at_depth[i], n_ab_cutoff_at_depth[i] / (double)n_alpha_beta_cutoff * 100);
	}
	
	for (int i = 0; i < depth; i ++)
	{
		fprintf(stderr, "nws node at depth %d: %d, %.2lf%%\n", i, n_nws_node_at_depth[i], n_nws_node_at_depth[i] / (double)n_nws_node * 100);
	}
#endif

	double bfactor = pow(n_node, 1.0 / depth);
	fprintf(stderr, "branching factor (normal node): %.2lf\n", bfactor);
	bfactor = pow(n_node_tot, 1.0 / depth);
	fprintf(stderr, "branching factor (tot): %.2lf\n", bfactor);
	fprintf(stderr, "solve 0: %d\n", n_solve_0);
	fprintf(stderr, "solve game end: %d\n", n_solve_game_over);
	fprintf(stderr, "alpha-beta cutoffs: %d, %.2lf%%\n", n_alpha_beta_cutoff, n_alpha_beta_cutoff / (double)n_node);
	fprintf(stderr, "transpositional cutoffs: %d\n", n_transpositional_cutoff);
	fprintf(stderr, "nws fail/tried: %d/%d, %.2lf%%\n", n_nws_fail, n_nws_tried, n_nws_fail / (double)n_nws_tried);

	fprintf(stderr, "probcut (high + low): %d + %d = %d\n", n_probcut_high_cutoff, n_probcut_low_cutoff, n_probcut_low_cutoff + n_probcut_high_cutoff);
	
	fprintf(stderr, "multicut: %d/%d, %.2lf%%\n", n_multicut_succeed, n_multicut_tried, n_multicut_succeed / (double)n_multicut_tried * 100);
}

