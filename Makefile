#
# $File: Makefile
# $Date: Fri Feb 01 18:36:50 2013 +0800
#
# A single output portable Makefile for
# simple c++ project

OBJ_DIR = obj
BIN_DIR = bin

TARGET = tothello

BIN_TARGET = $(BIN_DIR)/$(TARGET)

PRECOMP_DIR = precomp
PRECOMP_SRC = $(PRECOMP_DIR)/$(TARGET).cc
PRECOMP_TARGET = $(PRECOMP_DIR)/$(TARGET)


PKGCONFIG_LIBS = 
INCLUDE_DIR = 
DEFINES = 


CXXFLAGS += -g -O0 #-pg
CXXFLAGS += #$(DEFINES)
CXXFLAGS += -std=c++11
CXXFLAGS += -Wall -Wextra 
CXXFLAGS += $(INCLUDE_DIR) 

LDFLAGS = 

CXX = g++
CXXSOURCES = $(shell find src -name "*.cc")

OBJS = $(addprefix $(OBJ_DIR)/,$(CXXSOURCES:.cc=.o))
DEPFILES = $(OBJS:.o=.d)

.PHONY: all clean run rebuild gdb

all: $(BIN_TARGET)

$(OBJ_DIR)/%.o: %.cc
	@echo "[cc] $< ..."
	@$(CXX) -c $< $(CXXFLAGS) -o $@

$(OBJ_DIR)/%.d: %.cc
	@mkdir -pv $(dir $@)
	@echo "[dep] $< ..."
	@$(CXX) $(INCLUDE_DIR) $(CXXFLAGS) -MM -MT "$(OBJ_DIR)/$(<:.cc=.o) $(OBJ_DIR)/$(<:.cc=.d)" "$<" > "$@"

sinclude $(DEPFILES)

$(BIN_TARGET): $(OBJS)
	@echo "[link] $< ..."
	@mkdir -p $(BIN_DIR)
	@$(CXX) $(OBJS) -o $@ $(LDFLAGS) $(CXXFLAGS)

clean:
	rm -rf $(OBJ_DIR) $(BIN_DIR)

run: $(BIN_TARGET)
	./$(BIN_TARGET)

rebuild:
	+@make clean 
	+@make

gdb: $(BIN_TARGET)
	gdb ./$(BIN_TARGET)

genprecompsrc:
	rm -rf $(PRECOMP_DIR)
	mkdir -p $(PRECOMP_DIR)
	for i in src/type.hh src/stats.hh src/common.hh src/bit.hh src/board.hh src/feature.hh src/eval.hh src/flip_kindergarten.hh src/hash.hh src/move.hh src/random.hh src/search.hh src/settings.hh; do\
		cat $$i >> $(PRECOMP_SRC); \
	done
	for i in `ls src/*.cc`; do \
		cat $$i >> $(PRECOMP_SRC); \
	done
	sed -i $(PRECOMP_SRC) -e 's/^#include ".*$$//g'

speed: genprecompsrc
	g++ $(PRECOMP_SRC) -o $(PRECOMP_TARGET) $(DEFINES) -O2
	bash -c "time ./$(PRECOMP_TARGET)"

gprof: genprecompsrc
	g++ $(PRECOMP_SRC) -o $(PRECOMP_TARGET) $(DEFINES) -g -pg
	bash -c "time ./$(PRECOMP_TARGET)"
	gprof $(PRECOMP_TARGET) > tothello.gprof
	less tothello.gprof

push:
	make clean
	git add . -A
	git commit
	git push
